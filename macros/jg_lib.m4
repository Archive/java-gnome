AC_DEFUN([JG_LIB],[

if test -z "$PKG_CONFIG"; then
    AC_PATH_PROG(PKG_CONFIG, pkg-config, no)
fi

AC_MSG_CHECKING(for java-gnome jar file)
JGJAR=`$PKG_CONFIG --variable classpath jg-common`
AC_MSG_RESULT($JGJAR)
AC_SUBST(JGJAR)

AC_MSG_CHECKING(for java-gnome library)
JGJAVA_LIBS=`$PKG_CONFIG --libs jg-common`
AC_MSG_RESULT($JGJAVA_LIBS)
AC_SUBST(JGJAVA_LIBS)

AC_MSG_CHECKING(for java-gnome cflags)
JGJAVA_CFLAGS=`$PKG_CONFIG --cflags jg-common`
AC_MSG_RESULT($JGJAVA_CFLAGS)
AC_SUBST(JGJAVA_CFLAGS)

AC_MSG_CHECKING(for java-gnome jni library)
JGJNI_LIBS=`$PKG_CONFIG --variable jnilibs jg-common`
AC_MSG_RESULT($JGJNI_LIBS)
AC_SUBST(JGJNI_LIBS)

])