/* 
 * JNI utility functions, from the book "The Java Native Interface" by
 * Sheng Liang.
 */

#ifndef _JG_JNU_H_
#define _JG_JNU_H_
#include <jni.h>
#include <glib.h>
#include <glib-object.h>

#ifdef __cplusplus
extern "C" {
#endif


/** JNU_GetEnv: Returns a valid JNIEnv pointer.  
 *
 * This eliminates the need to store the JNIEnv pointer for 
 * later use in callbacks. 
 */
extern JNIEnv *JNU_GetEnv();

/** JNU_ThrowByName: throws a new Exception.
 *
 * @param env  the JNIEnv pointer
 * @param name the name of the Exception class, such as 
 * "java/lang/NullPointerException".
 * @param msg  the detail message for the exception.
 */
extern void JNU_ThrowByName(JNIEnv *env, const char *name, const char *msg);

extern gchar** getStringArray(JNIEnv *env, jobjectArray anArray);

extern void freeStringArray(JNIEnv *env, jobjectArray anArray, gchar** str);

extern jobjectArray getJavaStringArray(JNIEnv *env, const gchar* const * str);

    /**
     * Transforms a GSList into an java array of integers
     *
     * @param env the JNIEnv pointer
     * @param list the list to transform into an array
     */
extern jobjectArray getSList(JNIEnv *env, GSList* list);

    /**
     * Transforms a GList into an java array of integers
     *
     * @param env the JNIEnv pointer
     * @param list the list to transform into an array
     */
extern jobjectArray getList(JNIEnv *env, GList* list);

extern void* getPointerFromHandle(JNIEnv* env, jobject handle);

extern jobject getHandleFromPointer(JNIEnv* env, void* pointer);

extern void** getPointerArrayFromHandles(JNIEnv* env, jobjectArray handles);

extern void* getArrayFromHandles(JNIEnv* env, jobjectArray handles, int size_of_object, gboolean update_handles, gboolean delete_originals);

extern jobjectArray getHandleArrayFromPointers(JNIEnv* env, void** pointer, int numPtrs);

extern jobjectArray getHandleArrayFromGList(JNIEnv* env, GList* list);

extern jobjectArray getHandleArrayFromGSList(JNIEnv* env, GSList* list);

extern jobjectArray getHandleArray(JNIEnv* env, int length);

extern GSList* getGSListFromHandles(JNIEnv* env, jobjectArray handles);

extern GList* getGListFromHandles(JNIEnv* env, jobjectArray handles);

extern void updateHandle(JNIEnv* env, jobject handle, void* pointer);

#ifdef __cplusplus
} /* extern "C" */
#endif /* __cplusplus */

#endif /* !_JG_JNU_H_ */
