/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <string.h>
#include "jg_jnu.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org_gnu_javagnome_Struct
 * Method:    getNullHandle
 * Signature: ()Lorg/gnu/javagnome/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_javagnome_Struct_getNullHandle
(JNIEnv *env, jclass cls)
{
    return getHandleFromPointer(env, NULL);
}

#ifdef __cplusplus
}

#endif
