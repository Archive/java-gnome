/* 
 * JNI utility functions, from the book "The Java Native Interface" by
 * Sheng Liang.
 */

#include "jg_jnu.h"
#include <stddef.h>
#include <glib/gmem.h>
#include <glib.h>


#ifdef __cplusplus
extern "C" {
#endif

static JavaVM *cached_jvm;
static jfieldID pointerField64 = NULL;
static jfieldID pointerField32 = NULL;

/** From "The Java Native Interface", section 8.4.1 */
JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM *jvm, void *reserved)
{
    cached_jvm = jvm;
    return JNI_VERSION_1_2;
}

/** From "The Java Native Interface", section 8.4.1 */
JNIEnv *JNU_GetEnv()
{
    JNIEnv *env;
    (*cached_jvm)->GetEnv(cached_jvm, (void **)&env, JNI_VERSION_1_2);
    return env;
}

/** From "The Java Native Interface", section 6.1.2 */
void JNU_ThrowByName(JNIEnv *env, const char *name, const char *msg)
{
    jclass cls = (*env)->FindClass(env, name);
    /* if cls is NULL, an exception has already been thrown */
    if (cls != NULL)
	(*env)->ThrowNew(env, cls, msg);
    /* free the local ref */
    (*env)->DeleteLocalRef(env, cls);
}

gchar** getStringArray(JNIEnv *env, jobjectArray anArray) 
{
	jsize len = (*env)->GetArrayLength(env, anArray);
	gchar **strArray = g_malloc0((len+1) * sizeof(gchar*));
	int index;
	
	for (index = 0; index < len; index++) {
		jstring aString = (jstring)(*env)->GetObjectArrayElement(env, anArray, (jsize)index);
		strArray[index] = (gchar*)(*env)->GetStringUTFChars(env, aString, NULL);
	}
	strArray[len] = NULL;
	
	return strArray;
}

void freeStringArray(JNIEnv *env, jobjectArray anArray, gchar** str)
{
	jsize len = (*env)->GetArrayLength(env, anArray);
	int index;
	for (index = 0; index < len; index++) {
		jstring aString = (jstring)(*env)->GetObjectArrayElement(env, anArray, (jsize)index);
		(*env)->ReleaseStringUTFChars(env, aString, str[index]);
	}
}

jobjectArray getJavaStringArray(JNIEnv *env, const gchar* const * str) {
    int size = 0;
    // Length of array.
    while( str[size] ) { size++; }
    //
    jclass stringCls = (*env)->FindClass(env, "java/lang/String" );
    jobjectArray ret = (*env)->NewObjectArray(env, size, stringCls, NULL);
    int i = 0;
    for( i = 0; i < size; i++ ) {
        (*env)->SetObjectArrayElement(env, ret, i, (*env)->NewStringUTF(env, str[i]));
    }
    return ret;
}

jobjectArray getSList(JNIEnv *env, GSList* list)
{
    jobjectArray ar;
    int i = 0;
    jclass handleClass = NULL;
    
# if GLIB_SIZEOF_VOID_P == 8
		handleClass = (*env)->FindClass(env, "org/gnu/javagnome/Handle64Bits");
# elif GLIB_SIZEOF_VOID_P == 4
		handleClass = (*env)->FindClass(env, "org/gnu/javagnome/Handle32Bits");
# else
#	error "Your system has an unsupported pointer size"
#endif
	
    if (NULL == list)
        return NULL;
    ar = (*env)->NewObjectArray(env, g_slist_length(list), handleClass, NULL);
    while (list!=NULL) {
        (*env)->SetObjectArrayElement(env, ar, i, getHandleFromPointer(env, list->data));
        list = g_slist_next(list);
        i++;
    }
    return ar;
}

jobjectArray getList(JNIEnv *env, GList* list)
{
    jobjectArray ar;
    int i = 0;
    jclass handleClass = NULL;
    
# if GLIB_SIZEOF_VOID_P == 8
		handleClass = (*env)->FindClass(env, "org/gnu/javagnome/Handle64Bits");
# elif GLIB_SIZEOF_VOID_P == 4
		handleClass = (*env)->FindClass(env, "org/gnu/javagnome/Handle32Bits");
# else
#	error "Your system has an unsupported pointer size"
#endif

    if (NULL == list)
        return NULL;
    ar = (*env)->NewObjectArray(env, g_list_length(list), handleClass, NULL);
    while (list!=NULL) {
        (*env)->SetObjectArrayElement(env, ar, i, getHandleFromPointer(env, list->data));
        list = g_list_next(list);
        i++;
    }
    return ar;
}

void* getPointerFromHandle(JNIEnv* env, jobject handle) 
{
	jclass handleClass;
	void* pointer;
	
	if (!handle)
    	return NULL;

# if GLIB_SIZEOF_VOID_P == 8
		if (pointerField64 == NULL) {
			handleClass = (*env)->FindClass(env, "org/gnu/javagnome/Handle64Bits");
			if (handleClass == NULL)
				return NULL;
			pointerField64 = (*env)->GetFieldID(env, handleClass, 
						"pointer", "J");
			if (pointerField64 == NULL)
				return NULL;
	    }
		pointer = (void*)(*env)->GetLongField(env, handle, pointerField64);
# elif GLIB_SIZEOF_VOID_P == 4
		if (pointerField32 == NULL) {
			handleClass = (*env)->FindClass(env, "org/gnu/javagnome/Handle32Bits");
			if (handleClass == NULL)
				return NULL;
			pointerField32 = (*env)->GetFieldID(env, handleClass, 
						"pointer", "I");
			if (pointerField32 == NULL)
				return NULL;
	    }
		pointer = (void*)(*env)->GetIntField(env, handle, pointerField32);
# else
#	error "Your system has an unsupported pointer size"
#endif
	return pointer;
}


jobject getHandleFromPointer(JNIEnv* env, void* pointer)
{
	jclass handleClass;
	static jmethodID constructor = NULL;
	jobject handle;
	
# if GLIB_SIZEOF_VOID_P == 8
		handleClass = (*env)->FindClass(env, "org/gnu/javagnome/Handle64Bits");
		if (handleClass == NULL)
			return NULL;
		if (pointerField64 == NULL) {
			pointerField64 = (*env)->GetFieldID(env, handleClass, 
						"pointer", "J");
			if (pointerField64 == NULL)
				return NULL;
	    }
# elif GLIB_SIZEOF_VOID_P == 4
		handleClass = (*env)->FindClass(env, "org/gnu/javagnome/Handle32Bits");
		if (handleClass == NULL)
			return NULL;
		if (pointerField32 == NULL) {
			pointerField32 = (*env)->GetFieldID(env, handleClass, 
						"pointer", "I");
			if (pointerField32 == NULL)
				return NULL;
	    }
# else
#	error "Your system has an unsupported pointer size"
#endif

	if (constructor == NULL) {
		constructor = (*env)->GetMethodID(env, handleClass, "<init>", "()V");
		if (constructor == NULL)
			return NULL;
	}
	handle = (*env)->NewObject(env, handleClass, constructor);

# if GLIB_SIZEOF_VOID_P == 8
		(*env)->SetLongField(env, handle, pointerField64, (jlong)pointer);
# elif GLIB_SIZEOF_VOID_P == 4
		(*env)->SetIntField(env, handle, pointerField32, (jint)pointer);
# else
#	error "Your system has an unsupported pointer size"
#endif
	return handle;
}

void** getPointerArrayFromHandles(JNIEnv* env, jobjectArray handles)
{
	int index = 0;
	void** ret = NULL;
	jclass handleClass = NULL;
	jsize len = (*env)->GetArrayLength(env, handles);
	jobject obj;
	
	ret = g_malloc(sizeof(void*)*len);
	for (index = 0; index < len; index++)
	{
		obj = (*env)->GetObjectArrayElement(env, handles, index);
		if (NULL == handleClass)
			handleClass = (*env)->GetObjectClass(env, obj);
# if GLIB_SIZEOF_VOID_P == 8
		if (pointerField64 == NULL) {
			pointerField64 = (*env)->GetFieldID(env, handleClass, 
						"pointer", "J");
			if (pointerField64 == NULL)
				return NULL;
	    }
		ret[index] = (void*)(*env)->GetLongField(env, obj, pointerField64);
# elif GLIB_SIZEOF_VOID_P == 4
		if (pointerField32 == NULL) {
			pointerField32 = (*env)->GetFieldID(env, handleClass, 
						"pointer", "I");
			if (pointerField32 == NULL)
				return NULL;
	    }
		ret[index] = (void*)(*env)->GetIntField(env, obj, pointerField32);
# else
#	error "Your system has an unsupported pointer size"
#endif
	}
	return ret;
}

/*
 * Copy the contents pointed to by the handles into an array of structures.
 * The parameter update_handles should be "true" if you want to update the 
 * handles to refer to the corresponding new locations in the array.  The
 * parameter delete_originals should be "true" if you want to delete the memory
 * pointed to by the original handles.
 */
void* getArrayFromHandles(JNIEnv* env, jobjectArray handles, 
                          int size_of_object, gboolean update_handles,
                          gboolean delete_originals)
{
    int index = 0;
    void *ret = NULL;
    jsize len = (*env)->GetArrayLength(env, handles);
    jobject obj;

    ret = g_malloc(size_of_object * len);
    void *newloc = ret;
    void *origloc = NULL;
    for (index = 0; index < len; index++) {
        obj = (*env)->GetObjectArrayElement(env, handles, index);
        origloc = getPointerFromHandle(env, obj);
        memcpy(newloc, origloc, size_of_object);

        if (update_handles)
            updateHandle(env, obj, newloc);

        newloc += size_of_object;

        if (delete_originals)
            g_free(origloc);
    }
    return ret;
}

jobjectArray getHandleArrayFromPointers(JNIEnv* env, void** pointer, int numPtrs)
{
    int index = 0;
    jobjectArray ret;
    jclass handleClass;
    
# if GLIB_SIZEOF_VOID_P == 8
		handleClass = (*env)->FindClass(env, "org/gnu/javagnome/Handle64Bits");
# elif GLIB_SIZEOF_VOID_P == 4
		handleClass = (*env)->FindClass(env, "org/gnu/javagnome/Handle32Bits");
# else
#	error "Your system has an unsupported pointer size"
#endif
	
    ret = (*env)->NewObjectArray(env, numPtrs, handleClass, NULL);
    for (index = 0; index < numPtrs; index++)
    	(*env)->SetObjectArrayElement(env, ret, index, getHandleFromPointer(env, pointer[index]));
    return ret;
}

jobjectArray getHandleArrayFromGList(JNIEnv* env, GList* list)
{
    int index = 0;
    jobjectArray ret;
    jclass handleClass;
    
# if GLIB_SIZEOF_VOID_P == 8
		handleClass = (*env)->FindClass(env, "org/gnu/javagnome/Handle64Bits");
# elif GLIB_SIZEOF_VOID_P == 4
		handleClass = (*env)->FindClass(env, "org/gnu/javagnome/Handle32Bits");
# else
#	error "Your system has an unsupported pointer size"
#endif
	
    ret = (*env)->NewObjectArray(env, g_list_length(list), handleClass, NULL);
    for (index = 0; index < g_list_length(list); index++)
    	(*env)->SetObjectArrayElement(env, ret, index, getHandleFromPointer(env, g_list_nth_data(list, index)));
    return ret;
}

jobjectArray getHandleArrayFromGSList(JNIEnv* env, GSList* list)
{
    int index = 0;
    jobjectArray ret;
    jclass handleClass;
    
# if GLIB_SIZEOF_VOID_P == 8
		handleClass = (*env)->FindClass(env, "org/gnu/javagnome/Handle64Bits");
# elif GLIB_SIZEOF_VOID_P == 4
		handleClass = (*env)->FindClass(env, "org/gnu/javagnome/Handle32Bits");
# else
#	error "Your system has an unsupported pointer size"
#endif
	
    ret = (*env)->NewObjectArray(env, g_slist_length(list), handleClass, NULL);
    for (index = 0; index < g_slist_length(list); index++)
    	(*env)->SetObjectArrayElement(env, ret, index, getHandleFromPointer(env, g_slist_nth_data(list, index)));
    return ret;
}

jobjectArray getHandleArray(JNIEnv* env, int length)
{
    jobjectArray ret;
    jclass handleClass;

# if GLIB_SIZEOF_VOID_P == 8
		handleClass = (*env)->FindClass(env, "org/gnu/javagnome/Handle64Bits");
# elif GLIB_SIZEOF_VOID_P == 4
		handleClass = (*env)->FindClass(env, "org/gnu/javagnome/Handle32Bits");
# else
#	error "Your system has an unsupported pointer size"
#endif
	
    ret = (*env)->NewObjectArray(env, length, handleClass, NULL);
    return ret;
}

GSList* getGSListFromHandles(JNIEnv* env, jobjectArray handles)
{
    if (!handles)
        return NULL;

	int index = 0;
	GSList* ret = g_slist_alloc();
	jclass handleClass;
	jsize len = (*env)->GetArrayLength(env, handles);
	jobject obj;
	
	for (index = 0; index < len; index++)
	{
		obj = (*env)->GetObjectArrayElement(env, handles, index);
		handleClass = (*env)->GetObjectClass(env, obj);
# if GLIB_SIZEOF_VOID_P == 8
		if (pointerField64 == NULL) {
			pointerField64 = (*env)->GetFieldID(env, handleClass, 
						"pointer", "J");
			if (pointerField64 == NULL)
				return NULL;
	    }
		ret = g_slist_append(ret, (gpointer)(*env)->GetLongField(env, obj, pointerField64));
# elif GLIB_SIZEOF_VOID_P == 4
		if (pointerField32 == NULL) {
			pointerField32 = (*env)->GetFieldID(env, handleClass, 
						"pointer", "I");
			if (pointerField32 == NULL)
				return NULL;
	    }
		ret = g_slist_append(ret, (gpointer)(*env)->GetIntField(env, obj, pointerField32));
# else
#	error "Your system has an unsupported pointer size"
#endif
	}
	return ret;
}

GList* getGListFromHandles(JNIEnv* env, jobjectArray handles)
{
    if (!handles)
        return NULL;

	int index = 0;
	GList* ret = g_list_alloc();
	jclass handleClass;
	jsize len = (*env)->GetArrayLength(env, handles);
	jobject obj;
	
	for (index = 0; index < len; index++)
	{
		obj = (*env)->GetObjectArrayElement(env, handles, index);
		handleClass = (*env)->GetObjectClass(env, obj);
# if GLIB_SIZEOF_VOID_P == 8
		if (pointerField64 == NULL) {
			pointerField64 = (*env)->GetFieldID(env, handleClass, 
						"pointer", "J");
			if (pointerField64 == NULL)
				return NULL;
	    }
		ret = g_list_append(ret, (gpointer)(*env)->GetLongField(env, obj, pointerField64));
# elif GLIB_SIZEOF_VOID_P == 4
		if (pointerField32 == NULL) {
			pointerField32 = (*env)->GetFieldID(env, handleClass, 
						"pointer", "I");
			if (pointerField32 == NULL)
				return NULL;
	    }
		ret = g_list_append(ret, (gpointer)(*env)->GetIntField(env, obj, pointerField32));
# else
#	error "Your system has an unsupported pointer size"
#endif
	}
	return ret;
}

void updateHandle(JNIEnv* env, jobject handle, void* pointer) {
    jclass handleClass;

# if GLIB_SIZEOF_VOID_P == 8
    handleClass = (*env)->FindClass(env, "org/gnu/javagnome/Handle64Bits");
    if (handleClass == NULL)
        return;
    if (pointerField64 == NULL) {
        pointerField64 = (*env)->GetFieldID(env, handleClass, 
                                            "pointer", "J");
        if (pointerField64 == NULL)
            return;
    }
    (*env)->SetLongField(env, handle, pointerField64, (jlong)pointer);
# elif GLIB_SIZEOF_VOID_P == 4
    handleClass = (*env)->FindClass(env, "org/gnu/javagnome/Handle32Bits");
    if (handleClass == NULL)
        return;
    if (pointerField32 == NULL) {
        pointerField32 = (*env)->GetFieldID(env, handleClass, 
                                            "pointer", "I");
        if (pointerField32 == NULL)
            return;
    }
    (*env)->SetIntField(env, handle, pointerField32, (jint)pointer);
# else
#	error "Your system has an unsupported pointer size"
#endif

}

#ifdef __cplusplus
}
#endif
