/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.javagnome;

/**
 * Interface that represents a pointer to a native peer.
 */
public interface Handle {
    
    public boolean isNull();
    public boolean equals(Handle other);
}
