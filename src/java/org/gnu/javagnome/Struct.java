/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.javagnome;


/**
 */
public class Struct {

    /** Holder for the raw object pointer. */
    private Handle handle = null;

    /**
     * Create an uninitialized instance.  This has potential uses for 
     * derived classes.
     */
    protected Struct() {
    }

    /**
     * Create a new Struct with a handle to a native resource
     * returned from a call to the native libraries.
     * 
     * @param handle The handle that represents a pointer to a native resource.
     */
    public Struct(Handle handle) {
        setHandle(handle);
    }
	
    /**
     * Get the raw handle value.  This value
     * should never be modified by the application.  It's
     * sole use is to pass to native methods.
     *
     * @return the handle value.
     */
    public final Handle getHandle() {
        return handle;
    }

    /**
     * Check if two objects refer to the same (native) object.
     *
     * @param other the reference object with which to compare.
     * @return true if both objects refer to the same object.
     */
    public boolean equals( Object other ){
        return 
            other instanceof Struct && 
            handle.equals( ((Struct)other).getHandle() );
    }

    /**
     * Returns a hash code value for the object. This allows for
     * using Struct as keys in hashmaps.
     *
     * @return a hash code value for the object.
     */
    public int hashCode() {
        return getHandle().hashCode();
    }

    /**
     * Sets this object's native handle.
     */
    protected void setHandle(Handle hndl) {
        handle = hndl;
    }
	 
    /**
     * Get a native handle that refers to a null pointer.
     */
    native static final public Handle getNullHandle();


    // ------------------------------------------------------------------------
    // static init code
    static {
        System.loadLibrary(org.gnu.javagnome.Config.LIBRARY_NAME + org.gnu.javagnome.Config.API_VERSION);
    }

}
