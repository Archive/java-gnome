/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.javagnome;

/**
 * Base class for all classes resulting from a <code>define-flags</code>
 * in a <code>.defs</code> file.
 */
public class Flags {
	/** holder for the raw flags value */
	protected int value_;

	/**
	 * This class is only instantiable via subclasses.
	 */
	protected Flags() {
		// nothing to do
	}

	/**
	 * Get the raw value of the object.
	 * @return the raw value.
	 */
	public final int getValue() {
		return value_;
	}

	/**
	 * Get the hash code for this instance. It is the same as its value.
	 * @return the hash code
	 */
	public final int hashCode() {
		return value_;
	}

	/**
	 * Compare this to another object. The comparison is only
	 * <code>true</code> when the other object is also a <code>Flags</code>
	 * and when the values match.
	 *
	 * @param other the object to compare to
	 * @return the result of comparison
	 */
	public final boolean equals(java.lang.Object other) {
		if (!(other instanceof Flags)) {
			return false;
		}

		Flags otherFlags = (Flags) other;
		return (value_ == otherFlags.value_);
	}
}
