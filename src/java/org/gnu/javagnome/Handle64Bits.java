/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.javagnome;

/**
 * Holds pointer to native peer on 64 bit platforms.
 */
public class Handle64Bits implements Handle {

    long pointer;

    public boolean isNull() {
        return pointer == 0;
    }

    public boolean equals(Handle other) {
        if (other instanceof Handle64Bits)
            return pointer == ((Handle64Bits)other).pointer;
        return false;
    }

    public String toString() {
        return "Handle64Bits - pointer = " + String.valueOf( pointer );
    }
}
