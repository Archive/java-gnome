/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.javagnome;

/**
 * Holds pointer to native peer on 32 bit platforms.
 */
public class Handle32Bits implements Handle {
    
    int pointer;
    
    public boolean isNull() {
        return pointer == 0;
    }

    public boolean equals(Handle other) {
        if (other instanceof Handle32Bits)
            return pointer == ((Handle32Bits)other).pointer;
        return false;
    }

    public String toString() {
        return "Handle32Bits - pointer = " + String.valueOf( pointer );
    }
}
