package org.gnu.javagnome.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * @author Jeffrey S. Morgan
 *
 * A JUnit test class to build the base test tree for all packages.
 */
public class AllTests extends TestSuite {
	public static int unimplementedMethods;

	public static Test suite() {
		return new AllTests();
	}

	public AllTests() {
		super();
		addTest(org.gnu.gtk.tests.AllTests.suite());
	}

    /**
     * Starts the application.
     * @param args an array of command-line arguments
     */
    public static void main(String[] args) {
		unimplementedMethods = 0;
        boolean textMode = false;
        String test[] = new String[1];
        test[0] = AllTests.class.getName();

        if (args.length == 1) {
            if (args[0].equals("text")) {
                textMode = true;
            }
        }

        if (textMode) {
            junit.textui.TestRunner.main(test);
        } else {
            junit.swingui.TestRunner.main(test);
        }
    }

}
