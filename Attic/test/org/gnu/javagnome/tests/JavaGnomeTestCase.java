package org.gnu.javagnome.tests;

import junit.framework.TestCase;

/**
 * @author Jeffrey S. Morgan
 */
public class JavaGnomeTestCase extends TestCase {
	/**
	 * The following flags are used to mark test cases that
	 * are not handled correctly by java-gnome at this time, or 
	 * test cases that maybe themselves dubious (eg. when the correct
	 * behaviour may not be clear). Most of these flagged test
	 * cases involve handling error conditions.
	 *
	 * Setting these flags to true will run those tests. As api
	 * is implemented this gives us a convenient way to include
	 * it in the junit tests.
	 */

	// call should result in an 'Argument cannot be null' error
	public static boolean checkNullExceptions = false;

	// an out of range value is not handled gracefully
	public static boolean checkOutOfRangeBehaviour = false;

	// run test cases that may themselves be dubious
	// these should be eventually checked to see if 
	// there is a valid failure or the test is bogus
	public static boolean checkBogusTestCases = false;

	// check visibility api
	public static boolean checkVisibility = false;

	public JavaGnomeTestCase(String name) {
		super(name);
	}

	static public void assertEquals(String message, Object expected[], Object actual[]) {
		if (expected == null && actual == null)
			return;
		boolean equal = false;
		if (expected != null && actual != null && expected.length == actual.length) {
			if (expected.length == 0)
				return;
			equal = true;
			for (int i = 0; i < expected.length; i++) {
				if (!expected[i].equals(actual[i])) {
					equal = false;
				}
			}
		}
		if (!equal) {
			failNotEquals(message, expected, actual);
		}
	}

	static public void assertEquals(Object expected[], Object actual[]) {
		assertEquals(null, expected, actual);
	}

	static public void assertEquals(String message, int expected[], int actual[]) {
		if (expected == null && actual == null)
			return;
		boolean equal = false;
		if (expected != null && actual != null && expected.length == actual.length) {
			if (expected.length == 0)
				return;
			equal = true;
			for (int i = 0; i < expected.length; i++) {
				if (expected[i] != actual[i]) {
					equal = false;
				}
			}
		}
		if (!equal) {
			failNotEquals(message, expected, actual);
		}
	}

	static public void assertEquals(int expected[], int actual[]) {
		assertEquals(null, expected, actual);
	}

	// copied exactly from junit.framework.TestCase so that it can be called from here even though private
	static private void failNotEquals(String message, Object expected, Object actual) {
		String formatted = "";
		if (message != null)
			formatted = message + " ";
		fail(formatted + "expected:<" + expected + "> but was:<" + actual + ">");
	}

	protected void warnUnimpl(String message) {
		//System.out.println(this.getClass() + ": " + message);
		AllTests.unimplementedMethods++;
	}
}
