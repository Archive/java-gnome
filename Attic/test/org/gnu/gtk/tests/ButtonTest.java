/*
 * Java-Gnome Bindings Library
 *
 * * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gtk.tests;

import org.gnu.gtk.Button;
import org.gnu.gtk.ReliefStyle;

/**
 * Automated Test Suite for class org.gnu.gtk.Button
 *
 * @see org.gnu.gtk.Button
 */

public class ButtonTest extends ContainerTest {
	private Button button;

	/**
	 * Constructor for ButtonTest.
	 * @param name
	 */
	public ButtonTest(String name) {
		super(name);
	}

	protected void setUp() {
		button = new Button();
		setWidget(button);
	}

	protected void tearDown() {
		super.tearDown();
	}

	public void testSetGetRelief() {
		button.setRelief(ReliefStyle.NORMAL);
		assertEquals(button.getRelief().getValue(), ReliefStyle.NORMAL.getValue());
	}
	
	public void testSetGetLabel() {
		String label = "testLabel";
		button.setLabel(label);
		assertEquals(label, button.getLabel());
	}
	
	public void testSetGetUseUnderline() {
		button.setUseUnderline(false);
		assertEquals(false, button.getUseUnderline());
	}
	
	public void testSetGetUseStock() {
		button.setUseStock(false);
		assertEquals(false, button.getUseStock());
	}
}
