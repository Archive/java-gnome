/*
 * Java-Gnome Bindings Library
 *
 * * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.tests;

import org.gnu.gdk.Colormap;
import org.gnu.gtk.Widget;
import org.gnu.javagnome.tests.JavaGnomeTestCase;

/**
 * Automated Test Suite for class org.gnu.gtk.Widget
 *
 * @see org.gnu.gtk.Widget
 */

public class WidgetTest extends JavaGnomeTestCase {

	protected Widget widget;

	/**
	 * Constructor for Test_org_gnu_gtk_Widget.
	 * @param name
	 */
	public WidgetTest(String name) {
		super(name);
	}

	protected void setUp() {
	}

	protected void tearDown() {
		if (widget != null) {
			widget.destroy();
			widget = null;
		}
	}

	protected void setWidget(Widget w) {
		widget = w;
	}
	
	public void testSetGetName() {
		String name = "testName";
		widget.setName(name);
		assertEquals(name, widget.getName());
	}
	
	public void testSetGetColormap() {
		Colormap cm = new Colormap();
		widget.setColormap(cm);
		assertEquals(cm, widget.getColormap());
	}
}
