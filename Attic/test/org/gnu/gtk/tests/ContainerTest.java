/*
 * Java-Gnome Bindings Library
 *
 * * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gtk.tests;

import org.gnu.gtk.Container;
import org.gnu.gtk.ResizeMode;

/**
 * Automated Test Suite for class org.gnu.gtk.Container
 *
 * @see org.gnu.gtk.Container
 */

public class ContainerTest extends WidgetTest {
	
	private Container container;

	/**
	 * Constructor for ContainerTest.
	 * @param name
	 */
	public ContainerTest(String name) {
		super(name);
	}

	protected void setUp() {
	}

	protected void tearDown() {
		super.tearDown();
	}

	
	public void tesResizeMode() {
		container.setResizeMode(ResizeMode.PARENT);
		assertEquals(ResizeMode.PARENT, container.getResizeMode());
	}
	
	public void testBorderWidth() {
		container.setBorderWidth(20);
		assertEquals(container.getBorderWidth(), 20);
	}
}
