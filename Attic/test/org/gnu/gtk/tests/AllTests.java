package org.gnu.gtk.tests;

import org.gnu.gtk.tests.*;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * @author Jeffrey S. Morgan
 *
 * A JUnit test class to build the base test tree for all packages.
 */
public class AllTests extends TestSuite {

	public static Test suite() {
	    TestSuite suite = new TestSuite();
    	suite.addTestSuite(ButtonTest.class);
		return suite;		
	}

	public AllTests() {
		super();
	}

    /**
     * Starts the application.
     * @param args an array of command-line arguments
     */
    public static void main(String[] args) {
        boolean textMode = false;
        String test[] = new String[1];
        test[0] = AllTests.class.getName();

        if (args.length == 1) {
            if (args[0].equals("text")) {
                textMode = true;
            }
        }

        if (textMode) {
            junit.textui.TestRunner.main(test);
        } else {
            junit.swingui.TestRunner.main(test);
        }
    }

}
