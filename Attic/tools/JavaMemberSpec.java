// TODO: add a javadoc field and constructor arg; push it through to the
// subclass's constructors

/**
 * Representation of a member of a class. It consists of a name, a type
 * (the type of value; in the case of a method, this is the return type), a
 * static-or-instance flag, a final flag, and a scope.
 *
 * <p>The Java-Gnome bindings library is free software distributed under the
 * terms of the GNU Library General Public License version 2.</p>
 *
 * @author Dan Bornstein, danfuzz@milk.com
 * @author Copyright 2000 Dan Bornstein, all rights reserved. 
 */
abstract public class JavaMemberSpec
    implements Sourceable
{
    /** scope constant for <code>public</code> members */
    static public final int SCOPE_PUBLIC = 100;

    /** scope constant for <code>protected</code> members */
    static public final int SCOPE_PROTECTED = 101;

    /** scope constant for default (package scope) members */
    static public final int SCOPE_PACKAGE = 102;

    /** scope constant for <code>private</code> members */
    static public final int SCOPE_PRIVATE = 103;

    /** non-null; the name of the member */
    protected final String myName;

    /** non-null; the type of the member */
    protected final TypeSpecIface myType;

    /** <code>true</code> if this is to be a <code>static</code> member,
     * <code>false</code> if this is to be an instance member */
    protected final boolean myIsStatic;

    /** <code>true</code> if this is to be a <code>final</code> member,
     * <code>false</code> if not */
    protected final boolean myIsFinal;

    /** the scope of the member */
    protected final int myScope;

    

    // ------------------------------------------------------------------------
    // constructor

    /**
     * Construct an instance.
     *
     * @param name non-null; the name of the member
     * @param type non-null; the type of the member; in the case of a
     * method, this is the return type
     * @param isStatic <code>true</code> if this is to be a
     * <code>static</code> member, <code>false</code> if this is to be an
     * instance member
     * @param isFinal <code>true</code> if this is to be a
     * <code>final</code> member, <code>false</code> if not
     * @param scope the scope of the member, represented by one of
     * these constants: {@link #SCOPE_PUBLIC}, {@link #SCOPE_PROTECTED},
     * {@link #SCOPE_PACKAGE}, or {@link #SCOPE_PRIVATE}
     */
    public JavaMemberSpec (String name, TypeSpecIface type, boolean isStatic,
			   boolean isFinal, int scope)
    {
	if (name == null)
	{
	    throw new NullPointerException ("name == null");
	}

	if (type == null)
	{
	    throw new NullPointerException ("type == null");
	}

	if ((scope < SCOPE_PUBLIC) || (scope > SCOPE_PRIVATE))
	{
	    throw new IllegalArgumentException ("scope == " + scope);
	}

	myName = name;
	myType = type;
	myIsStatic = isStatic;
	myIsFinal = isFinal;
	myScope = scope;
    }



    // ------------------------------------------------------------------------
    // public instance methods

    /**
     * Return the string form for this object.
     *
     * @return the string form
     */
    public final String toString ()
    {
	StringBuffer sb = new StringBuffer ();
	sb.append (getClass ().getName ());
	sb.append ("[name=");
	sb.append (myName);
	sb.append (", type=");
	sb.append (myType);
	sb.append (", isStatic=");
	sb.append (myIsStatic);
	sb.append (", isFinal=");
	sb.append (myIsFinal);
	sb.append (", scope=");

	switch (myScope)
	{
	    case SCOPE_PUBLIC:    sb.append ("public");    break;
	    case SCOPE_PROTECTED: sb.append ("protected"); break;
	    case SCOPE_PACKAGE:   sb.append ("package");   break;
	    case SCOPE_PRIVATE:   sb.append ("private");   break;
	    default: sb.append ("unknown " + myScope);     break;
	}

	sb.append ("]");
	return sb.toString ();
    }

    /**
     * Get the name of this member.
     *
     * @return the name
     */
    public final String getName ()
    {
	return myName;
    }
    


    // ------------------------------------------------------------------------
    // public instance methods that must be overridden by the subclass

    // interface's javadoc suffices
    abstract public String javaSource ();

    // interface's javadoc suffices
    abstract public String cSource ();

    // interface's javadoc suffices
    abstract public String cHeader ();



    // ------------------------------------------------------------------------
    // protected instance methods

    /**
     * Return the string representation of the scope.
     *
     * @return the scope string
     */
    protected final String getScopeString ()
    {
	switch (myScope)
	{
	    case SCOPE_PUBLIC:    return "public";
	    case SCOPE_PROTECTED: return "protected";
	    case SCOPE_PACKAGE:   return "/*package*/";
	    case SCOPE_PRIVATE:   return "private";
	    default:
	    {
		throw new RuntimeException ("unknown scope: " + myScope);
	    }
	}
    }

    /**
     * Return the modifiers for this instance. For example, this might
     * return <code>"static final private"</code>. 
     */
    protected final String getModifiers ()
    {
	StringBuffer sb = new StringBuffer ();
	
	if (myIsStatic)
	{
	    sb.append ("static ");
	}

	if (myIsFinal)
	{
	    sb.append ("final ");
	}

	sb.append (getScopeString ());

	return sb.toString ();
    }

    /**
     * Return the initial Java declaration code for this instance.
     * For example, this might return 
     * <code>"static final private String milkFlavor"</code>.
     */
    protected final String getDeclaration ()
    {
	StringBuffer sb = new StringBuffer ();

	sb.append (getModifiers ());
	sb.append (' ');
	String className = myType.publicJavaType();
	int index = className.lastIndexOf(".");
	String shortName = className.substring(index+1);
	String pkg = className.substring(0, index+1);
	className = GlibTranslator.javaGnomeName(shortName);
	sb.append (pkg + className);
	sb.append (' ');
	sb.append (myName);

	return sb.toString ();
    }
}
