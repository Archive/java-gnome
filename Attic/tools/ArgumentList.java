
/**
 * Class representing an ordered list of arguments. Each argument is
 * represented by an instance of {@link ArgumentSpec}. This class is
 * responsible for emitting sets of declarations and conversions.
 *
 * <p>The Java-Gnome bindings library is free software distributed under the
 * terms of the GNU Library General Public License version 2.</p>
 *
 * @author Dan Bornstein, danfuzz@milk.com
 * @author Copyright 2000 Dan Bornstein, all rights reserved. 
 */
final public class ArgumentList {
	/** the actual list of arguments */
	private ArgumentSpec[] myList;

	// ------------------------------------------------------------------------
	// constructor

	/**
	 * Construct an instance.
	 *
	 * @param args null-ok; the array of argument specs
	 */
	public ArgumentList(ArgumentSpec[] args) {
		if (args != null) {
			// can't trust the outside world not to modify the array, so we
			// make a clone; too bad Java doesn't support a const array
			// type
			myList = (ArgumentSpec[]) args.clone();
		} else {
			myList = new ArgumentSpec[0];
		}
	}

	// ------------------------------------------------------------------------
	// public instance methods

	/**
	 * Return the string form for this instance.
	 *
	 * @return the string form
	 */
	public String toString() {
		StringBuffer result = new StringBuffer();

		result.append("(");

		for (int i = 0; i < myList.length; i++) {
			if (i != 0) {
				result.append(" ");
			}
			result.append(myList[i]);
		}

		result.append(")");
		return result.toString();
	}

	/**
	 * Return the number of arguments in this instance.
	 *
	 * @return the number of arguments
	 */
	public int getCount() {
		return myList.length;
	}

	/**
	 * Return the first argument in this instance.
	 *
	 * @return the first argument
	 */
	public ArgumentSpec getFirst() {
		return myList[0];
	}

	/**
	 * Returns the argument in this instance which is specified as
	 * 'isObject', or null if there is none.
	 */
	public ArgumentSpec getExplicitObject() {
		int i;
		for (i = 0; i < myList.length; i++)
			if (myList[i].getIsObject())
				return myList[i];
		return null;
	}

	/**
	 * Return a new instance, just like this one, except without the
	 * first element.
	 *
	 * @return the new instance
	 */
	//public ArgumentList withoutFirst ()
	//{
	//ArgumentSpec[] arr = new ArgumentSpec[myList.length - 1];
	//System.arraycopy (myList, 1, arr, 0, arr.length);
	//return new ArgumentList (arr);
	//}

	/**
	 * Return a new instance, just like this one, except without the
	 * last <code>n</code> elements.
	 *
	 * @param n how many arguments to chop off the end of the new instance
	 * @return the new instance
	 */
	public ArgumentList withoutLast(int n) {
		if (n == 0) {
			// easy out; yay for immutability
			return this;
		}

		ArgumentSpec[] arr = new ArgumentSpec[myList.length - n];
		System.arraycopy(myList, 0, arr, 0, arr.length);
		return new ArgumentList(arr);
	}

	/**
	 * Return a new instance, just like this one, except without
	 * any default argument definitions. (That is, it has the same 
	 * argument names and types, but none have a default value defined.)
	 *
	 * @return the new instance 
	 */
	public ArgumentList withoutDefaults() {
		ArgumentSpec[] arr = new ArgumentSpec[myList.length];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = myList[i].withoutDefault();
		}

		return new ArgumentList(arr);
	}

	/**
	 * Format this instance as a list of formal argument names.
	 *
	 * @return the formatted string
	 */
	public String argumentNames() {
		StringBuffer result = new StringBuffer();

		for (int i = 0; i < myList.length; i++) {
			if (i != 0) {
				result.append(", ");
			}
			result.append(myList[i].getFormalName());
		}

		return result.toString();
	}

	/**
	 * Format this instance as a list of variable names for the
	 * variables holding values to pass to a native Java method,
	 * at the Java level.
	 *
	 * @return the formatted string
	 */
	public String nativeJavaVariableNames() {
		StringBuffer result = new StringBuffer();

		for (int i = 0, n = 0; i < myList.length; i++) {
			if (!myList[i].getIsObject()) {
				if (n != 0)
					result.append(", ");
				result.append(myList[i].nativeJavaVariableName());
				n++;
			}
		}

		return result.toString();
	}

	/**
	 * Format this instance as a list of formal arguments to a public
	 * Java method.
	 *
	 * @return the formatted string
	 */
	public String publicJavaArguments() {
		return publicJavaArgumentsForDefault(0);
	}

	/**
	 * Format this instance as the list of statements used to convert
	 * public Java arguments to native Java variables.
	 *
	 * @return the formatted string
	 */
	public String publicToNativeJavaStatements() {
		StringBuffer result = new StringBuffer();

		for (int i = 0; i < myList.length; i++) {
			if (!myList[i].getIsObject())
				result.append(myList[i].publicToNativeJavaStatements());
		}

		return result.toString();
	}

	/**
	 * Format this instance as a list of formal arguments to a native
	 * Java method.
	 *
	 * @return the formatted string
	 */
	public String nativeJavaArguments() {
		StringBuffer result = new StringBuffer();

		for (int i = 0, n = 0; i < myList.length; i++) {
			if (!myList[i].getIsObject()) {
				if (n != 0)
					result.append(", ");
				result.append(myList[i].nativeJavaArgument());
				n++;
			}
		}

		return result.toString();
	}

	/**
	 * Format this instance as a Java signature string for the arguments
	 * to a native Java method. This returns the concatenation of the
	 * arguments but does not surround them with parentheses.
	 *
	 * @return the formatted string
	 */
	public String nativeJavaSignature() {
		StringBuffer result = new StringBuffer();

		for (int i = 0; i < myList.length; i++) {
			result.append(myList[i].nativeJavaSignature());
		}

		return result.toString();
	}

	/**
	 * Format this instance as a list of formal arguments to the C level
	 * of a native Java method. This returns an empty string (<code>""</code>),
	 * not <code>null</code> or <code>"void"</code> (heh, always wanted
	 * to say that) when this is an empty argument list.
	 *
	 * @return non-null; the formatted string
	 */
	public String nativeArguments() {
		StringBuffer result = new StringBuffer();
		int n = 0;

		// If there is an explicit object in the list, place that
		// *first* since that's the place where JNI always puts the
		// object value.
		ArgumentSpec obj = getExplicitObject();

		if (obj != null) {
			result.append(obj.nativeType() + " obj");
			n++;
		}

		for (int i = 0; i < myList.length; i++) {
			if (!myList[i].getIsObject()) {
				if (n != 0)
					result.append(", ");
				result.append(myList[i].nativeArgument());
				n++;
			}
		}

		return result.toString();
	}

	/**
	 * Format this instance as a list of the types for the C level
	 * of a native Java method (i.e., for use in a function prototype).
	 *
	 * @return the formatted string
	 */
	public String nativeTypes() {
		StringBuffer result = new StringBuffer();

		for (int i = 0, n = 0; i < myList.length; i++) {
			if (n != 0) {
				result.append(", ");
			}

			if (!myList[i].getIsObject()) {
				result.append(myList[i].nativeType());
				n++;
			} else
				result.append("ZZZ ");
		}

		return result.toString();
	}

	/**
	 * Format this instance as the list of statements used to convert
	 * the C layer native Java arguments to GLib/GTK variables. The
	 * return value is an array of two strings, consisting of the
	 * following elements:
	 *
	 * <ul>
	 * <li>the C statements declaring the variables, followed by any
	 * needed extra initialization statements</li>
	 * <li>the code needed to clean up after the variables are no
	 * longer needed</li>
	 * </ul>
	 *
	 * @return the array of two formatted strings
	 */
	public String[] javaToGlibNativeStatements() {
		StringBuffer decls = new StringBuffer();
		StringBuffer inits = new StringBuffer();
		StringBuffer cleanups = new StringBuffer();

		for (int i = 0; i < myList.length; i++) {
			if (!myList[i].getIsObject()) {
				String[] ss = myList[i].javaToGlibNativeStatements();
				decls.append(ss[0]);
				inits.append(ss[1]);
				cleanups.append(ss[2]);
			}
		}

		String[] result = new String[2];
		result[0] = decls.toString() + "\n" + inits.toString();
		result[1] = cleanups.toString();
		return result;
	}

	/**
	 * Format this instance as a list of formal arguments to a function
	 * taking raw GLib arguments. This returns <code>"void"</code> when
	 * this is an empty argument list.
	 *
	 * @return non-null; the formatted string 
	 */
	public String glibArguments() {
		if (myList.length == 0) {
			return "void";
		}

		StringBuffer result = new StringBuffer();

		for (int i = 0, n = 0; i < myList.length; i++) {
			// notice; here we don't need to do the object specialization
			// if (myList[i].getIsObject()) {
			if (n != 0)
				result.append(", ");

			result.append(myList[i].glibArgument());
			n++;
		}

		return result.toString();
	}

	/**
	 * Format this instance as a list of variable names for the
	 * variables holding values to pass to a GLib/GTK method,
	 * at the C level.
	 *
	 * @return non-null; the formatted string
	 */
	public String glibVariableNames() {
		StringBuffer result = new StringBuffer();

		for (int i = 0, n = 0; i < myList.length; i++) {
			if (n != 0)
				result.append(", ");

			if (!myList[i].getIsObject()) {
				result.append(myList[i].glibVariableName());
			} else {
				// object args are always named "cptr"
				result.append("cptr");
			}
			n++;
		}

		return result.toString();
	}

	/**
	 * Get the number of arguments in this instance that have default
	 * values.
	 * 
	 * @return the number of arguments with default values
	 */
	public int getDefaultCount() {
		for (int i = 0; i < myList.length; i++) {
			if (myList[i].getDefaultValue(null) != null) {
				return myList.length - i;
			}
		}

		return 0;
	}

	/**
	 * Return a string for the formal public Java arguments for the
	 * method variant that uses the given number of default arguments.
	 *
	 * @param defaults the number of default arguments
	 * @return the formatted string
	 */
	public String publicJavaArgumentsForDefault(int defaults) {
		StringBuffer result = new StringBuffer();

		for (int i = 0, n = 0; i < myList.length; i++) {
			if (!myList[i].getIsObject()) {
				if (n != 0)
					result.append(", ");
				String className = myList[i].publicJavaArgument();
				int index = className.lastIndexOf(".");
				String shortName = className.substring(index + 1);
				String pkg = className.substring(0, index + 1);
				className = GlibTranslator.javaGnomeName(shortName);
				result.append(pkg + className);
				n++;
			}
		}

		return result.toString();
	}

	/**
	 * Return a string for the arguments to call the full form of the
	 * public Java method <i>from</i> the public Java method that uses
	 * the given number of default arguments.
	 *
	 * @param translator the translator to use to translate the defaults
	 * @param defaults the number of default arguments
	 * @return the formatted string
	 */
	public String publicJavaActualsForDefault(
		DefaultTranslator translator,
		int defaults) {
		StringBuffer result = new StringBuffer();

		int passthrus = myList.length - defaults;

		for (int i = 0, n = 0; i < myList.length; i++) {
			if (myList[i].getIsObject())
				continue;

			if (n != 0) {
				result.append(", ");
			}

			if (i < passthrus) {
				result.append(myList[i].getFormalName());
			} else {
				result.append(myList[i].getDefaultValue(translator));
			}
			n++;
		}

		return result.toString();
	}

	// ------------------------------------------------------------------------
	// private static methods

	/**
	 * Given an argument name, return the corresponding variable name
	 * that should be used when converting it.
	 *
	 * @param argName the argument name
	 * @return the corresponding variable name
	 */
	private static String argToVariableName(String argName) {
		return argName + "_v";
	}
}
