/**
 * Specification of a single type. A type consists of a Scheme
 * type and a {@link TypeConverter} instance which knows how to generate code.
 *
 * <p>The Java-Gnome bindings library is free software distributed under the
 * terms of the GNU Library General Public License version 2.</p>
 *
 * @author Dan Bornstein, danfuzz@milk.com
 * @author based on code originally by Olivier Gutknecht, gutkneco@lirmm.fr
 * @author Copyright 2000 Dan Bornstein, all rights reserved. 
 */
public class TypeSpec implements TypeSpecIface {
	/** non-null; the {@link TypeConverter} to use */
	protected final TypeConverter myTypeConverter;

	/** non-null; the Scheme type name */
	protected final String mySchemeType;

	// ------------------------------------------------------------------------
	// constructor

	/**
	 * Construct an instance.
	 *
	 * @param typeConverter non-null; the {@link TypeConverter} to use
	 * @param schemeType non-null; the Scheme type name for the argument
	 */
	public TypeSpec(TypeConverter typeConverter, String schemeType) {
		if (typeConverter == null) {
			throw new NullPointerException("typeConverter == null");
		}

		if (schemeType == null) {
			throw new NullPointerException("schemeType == null");
		}

		myTypeConverter = typeConverter;
		mySchemeType = schemeType;
	}

	// ------------------------------------------------------------------------
	// public instance methods

	/**
	 * Return the string form for this instance.
	 *
	 * @return the string form
	 */
	public String toString() {
		return mySchemeType;
	}

	/**
	 * Get the hash code for this instance.
	 *
	 * @return the hash code
	 */
	public int hashCode() {
		return mySchemeType.hashCode();
	}

	/**
	 * Compare this to another instance. This returns <code>true</code>
	 * only if the converters are identical and the type names are
	 * equal.
	 *
	 * @param other the object to compare to
	 * @return the result of comparison
	 */
	public boolean equals(Object other) {
		if (!(other instanceof TypeSpec)) {
			return false;
		}

		TypeSpec otherType = (TypeSpec) other;
		return (
			(myTypeConverter == otherType.myTypeConverter)
				&& mySchemeType.equals(otherType.mySchemeType));
	}

	// interface's javadoc suffices
	final public String schemeType() {
		return mySchemeType;
	}

	// interface's javadoc suffices
	final public String publicJavaType() {
		return myTypeConverter.publicJavaType(mySchemeType);
	}

	// interface's javadoc suffices
	final public String publicJavaPackage() {
		String className = publicJavaSignature();
		if (!(className.startsWith("L") && className.endsWith(";"))) {
			throw new IllegalArgumentException(
				"type (" + this +") does not have a public Java class");
		}

		// strip off the "L" and ";" around the raw class path
		className = className.substring(1, className.length() - 1);

		// look for the last slash; if not found, then this class is
		// in the anonymous top-level package; return an empty string
		int lastSlash = className.lastIndexOf('/');
		if (lastSlash == -1) {
			return "";
		}

		// found a slash; remove it and everything after it
		className = className.substring(0, lastSlash);

		// convert slashes to dots before returning
		return className.replace('/', '.');
	}

	// interface's javadoc suffices
	final public String simpleJavaType() {
		String cls = publicJavaType();
		int lastDot = cls.lastIndexOf('.');
		return cls.substring(lastDot + 1);
	}

	// interface's javadoc suffices
	final public String publicJavaSignature() {
		return TypeConverter.javaSignature(publicJavaType());
	}

	// interface's javadoc suffices
	final public String publicJavaMethodName(String schemeName) {
		// FIXME: this conversion should be directed in a more generic way,
		// probably in TypeConverter; also, the code here is
		// near-duplicated in publicJavaConstantName; these two methods
		// should be combined in some meaningful way

		// attempt to remove the prefix the function name has in common
		// with the type
		int nlen = schemeName.length();
		int tlen = mySchemeType.length();
		int t = 0;
		int n;
		for (n = 0; n < nlen; n++) {
			char nc = schemeName.charAt(n);

			if (nc == '_') {
				continue;
			}

			if (t >= tlen) {
				break;
			}

			char tc = mySchemeType.charAt(t);
			t++;
			if (Character.toLowerCase(nc) != Character.toLowerCase(tc)) {
				break;
			}
		}

		if (n == nlen) {
			// the names are identical; what in the world does one do here?
			// (we presume this won't happen)
			throw new RuntimeException(
				"type and function names match exactly: "
					+ mySchemeType
					+ ", "
					+ schemeName);
		}

		// back off until just after an underscore, so we don't chop off
		// weird "fake" prefixes
		while (n > 0) {
			if (schemeName.charAt(n) == '_') {
				n++;
				break;
			}
			n--;
		}

		// truncate off the prefix, and make underscores cause a subsequent
		// letter to be capitalized
		boolean upcase = false;
		StringBuffer result = new StringBuffer(nlen - n);
		while (n < nlen) {
			char c = schemeName.charAt(n);

			if (c == '_') {
				upcase = true;
			} else {
				if (upcase) {
					c = Character.toUpperCase(c);
					upcase = false;
				}
				result.append(c);
			}

			n++;
		}

		return TypeConverter.makeValidJavaIdentifier(result.toString());
	}

	// interface's javadoc suffices
	public String publicJavaConstantName(String schemeName) {
		// FIXME: this conversion should be directed in a more generic
		// way, probably in TypeConverter; also, the code here is
		// near-duplicated in publicJavaMethodName; these two methods
		// should be combined in some meaningful way

		// attempt to remove the prefix the constant name has in common
		// with the type
		int nlen = schemeName.length();
		int tlen = mySchemeType.length();
		int t = 0;
		int n;
		for (n = 0; n < nlen; n++) {
			char nc = schemeName.charAt(n);

			if (nc == '_') {
				continue;
			}

			if (t >= tlen) {
				break;
			}

			char tc = mySchemeType.charAt(t);
			t++;
			if (Character.toLowerCase(nc) != Character.toLowerCase(tc)) {
				break;
			}
		}

		if (n == nlen) {
			// the names are identical; what in the world does one do here?
			// (we presume this won't happen)
			throw new RuntimeException(
				"type and constant names match exactly: "
					+ mySchemeType
					+ ", "
					+ schemeName);
		}

		// back off until just after an underscore, so we don't chop off
		// weird "fake" prefixes
		while (n > 0) {
			if (schemeName.charAt(n) == '_') {
				n++;
				break;
			}
			n--;
		}

		// truncate off the prefix
		String result = schemeName.substring(n);
		return TypeConverter.makeValidJavaIdentifier(result);
	}

	// interface's javadoc suffices
	final public String nativeJavaType() {
		return myTypeConverter.nativeJavaType(mySchemeType);
	}

	// interface's javadoc suffices
	final public String nativeJavaSignature() {
		return TypeConverter.javaSignature(nativeJavaType());
	}

	// interface's javadoc suffices
	final public String nativeType() {
		return myTypeConverter.nativeType(mySchemeType);
	}

	// interface's javadoc suffices
	final public boolean requiresNativeToPublicJavaReturn() {
		return myTypeConverter.requiresNativeToPublicJavaReturn(mySchemeType);
	}

	// interface's javadoc suffices
	final public String nativeToPublicJavaReturn(String call) {
		return myTypeConverter.nativeToPublicJavaReturn(mySchemeType, call);
	}

	// interface's javadoc suffices
	final public String glibToNativeReturn(String call, String cleanup) {
		return myTypeConverter.glibToNativeReturn(mySchemeType, call, cleanup);
	}

	// interface's javadoc suffices
	final public String glibType() {
		return myTypeConverter.glibType(mySchemeType);
	}
}
