;; -*- scheme -*-

;;
;; Pango definitions



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; TODO


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoRectangle
;; from pango-types.h
(define-boxed PangoRectangle ()
  (fields
   (int x)
   (int y)
   (int width)
   (int height)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoDirection
;; from pango-types.h
(define-enum PangoDirection
  (ltr PANGO_DIRECTION_LTR)
  (rtl PANGO_DIRECTION_RTL)
  (ttb-ltr PANGO_DIRECTION_TTB_LTR)
  (ttb-rtl PANGO_DIRECTION_TTB_RTL))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoLanguage
;; from pango-types.h
(define-boxed PangoLanguage ())

(define-func pango_language_get_type
  GType
  ())

(define-func pango_language_from_string
  PangoLanguage
  ((string language)))

(define-func pango_language_to_string
  string
  ((PangoLanguage language)))

(define-func pango_language_matches
  bool
  ((PangoLanguage language)
   (string rangeList)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoColor
;; from pango-attributes.h
(define-boxed PangoColor ()
  (fields
   (int red)
   (int green)
   (int blue)))

(define-func pango_color_get_type
  GType
  ())

(define-func pango_color_copy
  PangoColor
  ((PangoColor src)))

(define-func pango_color_free
  none
  ((PangoColor color)))

(define-func pango_color_parse
  bool
  ((PangoColor color)
   (string spec)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoAttrType
;; from pango-attribute.h
(define-enum PangoAttrType
  (invalid PANGO_ATTR_INVALID)
  (language PANGO_ATTR_LANGUAGE)
  (family PANGO_ATTR_FAMILY)
  (style PANGO_ATTR_STYLE)
  (weight PANGO_ATTR_WEIGHT)
  (variant PANGO_ATTR_VARIANT)
  (stretch PANGO_ATTR_STRETCH)
  (size PANGO_ATTR_SIZE)
  (font-desc PANGO_ATTR_FONT_DESC)
  (foreground PANGO_ATTR_FOREGROUND)
  (background PANGO_ATTR_BACKGROUND)
  (underline PANGO_ATTR_UNDERLINE)
  (strikethrough PANGO_ATTR_STRIKETHROUGH)
  (rise PANGO_ATTR_RISE)
  (shape PANGO_ATTR_SHAPE)
  (scale PANGO_ATTR_SCALE))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoUnderlne
;; from pango-attributes.h
(define-enum PangoUnderline
  (none PANGO_UNDERLINE_NONE)
  (single PANGO_UNDERLINE_SINGLE)
  (double PANGO_UNDERLINE_DOUBLE)
  (low PANGO_UNDERLINE_LOW))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoCoverageLevel
;; from pango-coverage.h
(define-enum PangoCoverageLevel
  (none PANGO_COVERAGE_NONE)
  (fallback PANGO_COVERAGE_FALLBACK)
  (approximate PANGO_COVERAGE_APPROXIMATE)
  (exact PANGO_COVERAGE_EXACT))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoCoverage
;; from pango-coverage.h
(define-boxed PangoCoverage ())

(define-func pango_coverage_new
  PangoCoverage
  ())

(define-func pango_coverage_ref
  PangoCoverage
  ((PangoCoverage coverage)))

(define-func pango_coverage_unref
  none
  ((PangoCoverage coverage)))

(define-func pango_coverage_copy
  PangoCoverage
  ((PangoCoverage coverage)))

(define-func pango_coverage_get
  PangoCoverageLevel
  ((PangoCoverage coverage)
   (int index)))

(define-func pango_coverage_set
  none
  ((PangoCoverage coverage)
   (int index)
   (PangoCoverageLevel level)))

(define-func pango_coverage_max
  none
  ((PangoCoverage coverage)
   (PangoCoverage other)))

(define-func pango_coverage_to_bytes
  none
  ((PangoCoverage coverage)
   (string bytes)
   (int* numBytes)))

(define-func pango_coverage_from_bytes
  PangoCoverage
  ((string bytes)
   (int numBytes)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoStyle
;; from pango-font.h
(define-enum PangoStyle
  (normal PANGO_STYLE_NORMAL)
  (oblique PANGO_STYLE_OBLIQUE)
  (italic PANGO_STYLE_ITALIC))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoVariant
;; from pango-font.h
(define-enum PangoVariant
  (normal PANGO_VARIANT_NORMAL)
  (small-caps PANGO_VARIANT_SMALL_CAPS))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoWeight
;; from pango-font.h
(define-enum PangoWeight
  (ultralight PANGO_WEIGHT_ULTRALIGHT (= 200))
  (light PANGO_WEIGHT_LIGHT (= 300))
  (normal PANGO_WEIGHT_NORMAL (= 400))
  (bold PANGO_WEIGHT_BOLD (= 700))
  (ultrabold PANGO_WEIGHT_ULTRABOLD (= 800))
  (heavy PANGO_WEIGHT_HEAVY (= 900)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoStretch
;; from pango-font.h
(define-enum PangoStretch
  (ultra-condensed PANGO_STRETCH_ULTRA_CONDENSED)
  (extra-condensed PANGO_STRETCH_EXTRA_CONDENSED)
  (condensed PANGO_STRETCH_CONDENSED)
  (semi-condensed PANGO_STRETCH_SEMI_CONDENSED)
  (normal PANGO_STRETCH_NORMAL)
  (semi-expanded PANGO_STRETCH_SEMI_EXPANDED)
  (expanded PANGO_STRETCH_EXPANDED)
  (extra-expanded PANGO_STRETCH_EXTRA_EXPANDED)
  (ultra-expanded PANGO_STRETCH_ULTRA_EXPANDED))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoFontMask
;; from pango-font.h
(define-flags PangoFontMask
  (family PANGO_FONT_MASK_FAMILY)
  (style PANGO_FONT_MASK_STYLE)
  (variant PANGO_FONT_MASK_VARIANT)
  (weight PANGO_FONT_MASK_WEIGHT)
  (stretch PANGO_FONT_MASK_STRETCH)
  (size PANGO_FONT_MASK_SIZE))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoFontDescription
;; from pango-font.h
(define-boxed PangoFontDescription ())

(define-func pango_font_description_get_type
  GType
  ())

(define-func pango_font_description_new
  PangoFontDescription
  ())

(define-func pango_font_description_copy
  PangoFontDescription
  ((PangoFontDescription desc)))

(define-func pango_font_description_hash
  int
  ((PangoFontDescription desc)))

(define-func pango_font_description_equal
  bool
  ((PangoFontDescription desc1)
   (PangoFontDescription desc2)))

(define-func pango_font_description_free
  none
  ((PangoFontDescription desc)))

(define-func pango_font_description_set_family
  none
  ((PangoFontDescription desc)
   (string family)))

(define-func pango_font_description_get_family
  string
  ((PangoFontDescription desc)))

(define-func pango_font_description_set_style
  none
  ((PangoFontDescription desc)
   (PangoStyle style)))

(define-func pango_font_description_get_style
  PangoStyle
  ((PangoFontDescription desc)))

(define-func pango_font_description_set_variant
  none
  ((PangoFontDescription desc)
   (PangoVariant variant)))

(define-func pango_font_description_get_variant
  PangoVariant
  ((PangoFontDescription desc)))

(define-func pango_font_description_set_weight
  none
  ((PangoFontDescription desc)
   (PangoWeight weight)))

(define-func pango_font_description_get_weight
  PangoWeight
  ((PangoFontDescription desc)))

(define-func pango_font_description_set_stretch
  none
  ((PangoFontDescription desc)
   (PangoStretch stretch)))

(define-func pango_font_description_get_stretch
  PangoStretch
  ((PangoFontDescription desc)))

(define-func pango_font_description_set_size
  none
  ((PangoFontDescription desc)
   (int size)))

(define-func pango_font_description_get_size
  int
  ((PangoFontDescription desc)))

(define-func pango_font_description_get_set_fields
  PangoFontMask
  ((PangoFontDescription desc)))

(define-func pango_font_description_unset_fields
  none
  ((PangoFontDescription desc)
   (PangoFontMask toUnset)))

(define-func pango_font_description_merge
  none
  ((PangoFontDescription desc)
   (PangoFontDescription descToMerge)
   (bool replaceExisting)))

(define-func pango_font_description_better_match
  bool
  ((PangoFontDescription desc)
   (PangoFontDescription oldMatch)
   (PangoFontDescription newMatch)))

(define-func pango_font_description_from_string
  PangoFontDescription
  ((string str)))

(define-func pango_font_description_to_string
  string
  ((PangoFontDescription desc)))

(define-func pango_font_description_to_filename
  string
  ((PangoFontDescription desc)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoFontMetrics
;; from pango-font.h
(define-boxed PangoFontMetrics ())

(define-func pango_font_metrics_get_type
  GType
  ())

(define-func pango_font_metrics_ref
  PangoFontMetrics
  ((PangoFontMetrics metrics)))

(define-func pango_font_metrics_unref
  none
  ((PangoFontMetrics metrics)))

(define-func pango_font_metrics_get_ascent
  int
  ((PangoFontMetrics metrics)))

(define-func pango_font_metrics_get_descent
  int
  ((PangoFontMetrics metrics)))

(define-func pango_font_metrics_get_approximate_char_width
  int
  ((PangoFontMetrics metrics)))

(define-func pango_font_metrics_get_approximate_digit_width
  int
  ((PangoFontMetrics metrics)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoFontFamily
;; from pango-font.h
(define-object PangoFontFamily ()
  (comment ()))

(define-func pango_font_family_get_type
  GType
  ())

(define-func pango_font_family_list_faces
  none
  ((PangoFontFamily family)
   (PangoFontFace* faces)
   (int* numFaces)))

(define-func pango_font_family_get_name
  string
  ((PangoFontFamily family)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoFontFace
;; from pango-font.h
(define-object PangoFontFace ()
  (comment ()))

(define-func pango_font_face_get_type
  GType
  ())

(define-func pango_font_face_describe
  PangoFontDescription
  ((PangoFontFace face)))

(define-func pango_font_face_get_face_name
  string
  ((PangoFontFace face)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoFont
;; from pango-font.h
(define-object PangoFont ()
  (comment ()))

(define-func pango_font_get_type
  GType
  ())

(define-func pango_font_describe
  PangoFontDescription
  ((PangoFont font)))

(define-func pango_font_get_coverage
  PangoCoverage
  ((PangoFont font)
   (PangoLanguage language)))

;(define-func pango_font_find_shaper
;  PangoEngineShape
;  ((PangoFont font)
;   (PangoLanguage language)
;   (char ch)))

(define-func pango_font_get_metrics
  PangoFontMetrics
  ((PangoFont font)
   (PangoLanguage language)))

;(define-func pango_font_get_glyph_extents
;  none
;  ((PangoFont font)
;   (PangoGlyph glyph)
;   (PangoRectangle inkRect)
;   (PangoRectangle logicalRect)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoAlignment
;; from pango-layout.h
(define-enum PangoAlignment
  (left PANGO_ALIGN_LEFT)
  (center PANGO_ALIGN_CENTER)
  (right PANGO_ALIGN_RIGHT))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoWrapMode
;; from pango-layout.h
(define-enum PangoWrapMode
  (word PANGO_WRAP_WORD)
  (char PANGO_WRAP_CHAR))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoContext
;; from pango-context.h
(define-object PangoContext ()
  (comment ()))

(define-func pango_context_get_type
  GType
  ())

(define-func pango_context_list_families
  none
  ((PangoContext context)
   (PangoFontFamily* families)
   (int* numFamilies)))

(define-func pango_context_load_font
  PangoFont
  ((PangoContext context)
   (PangoFontDescription desc)))

; TODO - need PangoFontset
;(define-func pango_context_load_fontset
;  PangoFontset
;  ((PangoContext context)
;   (PangoFontDescription desc)
;   (PangoLanguage language)))

(define-func pango_context_get_metrics
  PangoFontMetrics
  ((PangoContext context)
   (PangoFontDescription desc)
   (PangoLanguage language)))

(define-func pango_context_set_font_description
  none
  ((PangoContext context)
   (PangoFontDescription desc)))

(define-func pango_context_get_font_description
  PangoFontDescription
  ((PangoContext context)))

(define-func pango_context_get_language
  PangoLanguage
  ((PangoContext context)))

(define-func pango_context_set_language
  none
  ((PangoContext context)
   (PangoLanguage language)))

(define-func pango_context_set_base_dir
  none
  ((PangoContext context)
   (PangoDirection direction)))

(define-func pango_context_get_base_dir
  PangoDirection
  ((PangoContext context)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoLayout
;; from pango-layout.h
(define-object PangoLayout ()
  (comment ()))

(define-func pango_layout_get_type
  GType
  ())

(define-func pango_layout_new
  PangoLayout
  ((PangoContext context)))

(define-func pango_layout_copy
  PangoLayout
  ((PangoLayout src)))

(define-func pango_layout_get_context
  PangoContext
  ((PangoLayout layout)))

(define-func pango_layout_set_attributes
  none
  ((PangoLayout layout)
   (PangoAttrList attrs)))

(define-func pango_layout_get_attributes
  PangoAttrList
  ((PangoLayout layout)))

(define-func pango_layout_set_text
  none
  ((PangoLayout layout)
   (string text)
   (int length)))

(define-func pango_layout_get_text
  string
  ((PangoLayout layout)))

(define-func pango_layout_set_markup
  none
  ((PangoLayout layout)
   (string markup)
   (int length)))

(define-func pango_layout_set_markup_with_accel
  none
  ((PangoLayout layout)
   (string markup)
   (int length)
   (char accelMarker)
   (char accelChar)))

(define-func pango_layout_set_font_description
  none
  ((PangoLayout layout)
   (PangoFontDescription desc)))

(define-func pango_layout_set_width
  none
  ((PangoLayout layout)
   (int width)))

(define-func pango_layout_get_width
  int
  ((PangoLayout layout)))

(define-func pango_layout_set_wrap
  none
  ((PangoLayout layout)
   (PangoWrapMode wrap)))

(define-func pango_layout_get_wrap
  PangoWrapMode
  ((PangoLayout layout)))

(define-func pango_layout_set_indent
  none
  ((PangoLayout layout)
   (int indent)))

(define-func pango_layout_get_indent
  int
  ((PangoLayout layout)))

(define-func pango_layout_set_spacing
  none
  ((PangoLayout layout)
   (int spacing)))

(define-func pango_layout_get_spacing
  int
  ((PangoLayout layout)))

(define-func pango_layout_set_justify
  none
  ((PangoLayout layout)
   (bool justify)))

(define-func pango_layout_get_justify
  bool
  ((PangoLayout layout)))

(define-func pango_layout_set_alignment
  none
  ((PangoLayout layout)
   (PangoAlignment alignment)))

(define-func pango_layout_get_alignment
  PangoAlignment
  ((PangoLayout layout)))

(define-func pango_layout_set_tabs
  none
  ((PangoLayout layout)
   (PangoTabArray tabs)))

(define-func pango_layout_get_tabs
  PangoTabArray
  ((PangoLayout layout)))

(define-func pango_layout_set_single_paragraph_mode
  none
  ((PangoLayout layout)
   (bool setting)))

(define-func pango_layout_get_single_paragraph_mode
  bool
  ((PangoLayout layout)))

(define-func pango_layout_context_changed
  none
  ((PangoLayout layout)))

; Need PangoLogAttr
;(define-func pango_layout_get_log_attrs
;  none
;  ((PangoLayout layout)
;   (PangoLogAttr attrs (read-write))
;   (int numAttrs (read-write)))

(define-func pango_layout_index_to_pos
  none
  ((PangoLayout layout)
   (int index)
   (PangoRectangle pos)))

(define-func pango_layout_get_cursor_pos
  none
  ((PangoLayout layout)
   (int index)
   (PangoRectangle* strongPos)
   (PangoRectangle* weakPos)))

(define-func pango_layout_move_cursor_visually
  none
  ((PangoLayout layout)
   (bool strong)
   (int oldIndex)
   (int oldTrailing)
   (int direction)
   (int* newIndex)
   (int* newTrailing)))

(define-func pango_layout_xy_to_index
  bool
  ((PangoLayout layout)
   (int x)
   (int y)
   (int* index)
   (int* trailing)))

(define-func pango_layout_get_extents
  none
  ((PangoLayout layout)
   (PangoRectangle* inkRect)
   (PangoRectangle* logicalRect)))

(define-func pango_layout_get_pixel_extents
  none
  ((PangoLayout layout)
   (PangoRectangle* inkRect)
   (PangoRectangle* logicalRect)))

(define-func pango_layout_get_size
  none
  ((PangoLayout layout)
   (int* width)
   (int* height)))

(define-func pango_layout_get_pixel_size
  none
  ((PangoLayout layout)
   (int* width)
   (int* height)))

(define-func pango_layout_get_line_count
  int
  ((PangoLayout layout)))

(define-func pango_layout_get_line
  PangoLayoutLine
  ((PangoLayout layout)
   (int line)))

;(define-func pango_layout_get_lines
;  GList
;  ((PangoLayout layout)))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoLayoutLine
;; from pango-layout.h
(define-boxed PangoLayoutLine ())

(define-func pango_layout_line_ref
  none
  ((PangoLayoutLine line)))

(define-func pango_layout_line_unref
  none
  ((PangoLayoutLine line)))

(define-func pango_layout_line_x_to_index
  bool
  ((PangoLayoutLine line)
   (int xPos)
   (int* index)
   (int* trailing)))

(define-func pango_layout_line_index_to_x
  none
  ((PangoLayoutLine line)
   (int index)
   (bool trailing)
   (int* xPos)))

(define-func pango_layout_line_get_x_ranges
  none
  ((PangoLayoutLine line)
   (int startIndex)
   (int endIndex)
   (int* ranges)
   (int* numRanges)))

(define-func pango_layout_line_get_extents
  none
  ((PangoLayoutLine line)
   (PangoRectangle* inkRect)
   (PangoRectangle* logicalRect)))

(define-func pango_layout_line_get_pixel_extents
  none
  ((PangoLayoutLine line)
   (PangoRectangle* inkRect)
   (PangoRectangle* logicalRect)))


;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoAttribute
;; from pango-attributes.h
(define-boxed PangoAttribute ()
  (fields
   (int start_index)
   (int end_index)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoAttrString
;; from pango-attributes.h
(define-boxed PangoAttrString (PangoAttribute)
  (fields
   (string value)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoAttrLanguage
;; from pango-attributes.h
(define-boxed PangoAttrLanguage (PangoAttribute)
  (fields
   (PangoLanguage value)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoAttrInt
;; from pango-attributes.h
(define-boxed PangoAttrInt (PangoAttribute)
  (fields
   (int value)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoAttrFloat
;; from pango-attributes.h
(define-boxed PangoAttrFloat (PangoAttribute)
  (fields
   (double value)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoAttrColor
;; from pango-attributes.h
(define-boxed PangoAttrColor (PangoAttribute))
;  (fields
;   (PangoColor color)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoAttrShape
;; from pango-attributes.h
(define-boxed PangoAttrShape (PangoAttribute)
  (fields
   (PangoRectangle ink_rect)
   (PangoRectangle logical_rect)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoSttrFontDesc
;; from pango-attributes.h
(define-boxed PangoAttrFontDesc (PangoAttribute)
  (fields
   (PangoFontDescription desc)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoAttrList
;; from pango-attributes.h
(define-object PangoAttrList ()
  (comment ()))

(define-func pango_attr_list_get_type
  GType
  ())

(define-func pango_attr_list_new
  PangoAttrList
  ())

(define-func pango_attr_list_ref
  none
  ((PangoAttrList list)))

(define-func pango_attr_list_unref
  none
  ((PangoAttrList list)))

(define-func pango_attr_list_copy
  PangoAttrList
  ((PangoAttrList list)))

(define-func pango_attr_list_insert
  none
  ((PangoAttrList list)
   (PangoAttribute attr)))

(define-func pango_attr_list_insert_before
  none
  ((PangoAttrList list)
   (PangoAttribute attr)))

(define-func pango_attr_list_change
  none
  ((PangoAttrList list)
   (PangoAttribute attr)))

(define-func pango_attr_list_splice
  none
  ((PangoAttrList list)
   (PangoAttrList other)
   (int pos)
   (int len)))

(define-func pango_attr_list_get_iterator
  PangoAttrIterator
  ((PangoAttrList list)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoAttrIterator
;; from pango-attributes.h
(define-boxed PangoAttrIterator ())

(define-func pango_attr_iterator_range
  none
  ((PangoAttrIterator iterator)
   (int* start)
   (int* end)))

(define-func pango_attr_iterator_next
  bool
  ((PangoAttrIterator iterator)))

(define-func pango_attr_iterator_copy
  PangoAttrIterator
  ((PangoAttrIterator iterator)))

(define-func pango_attr_iterator_destroy
  none
  ((PangoAttrIterator iterator)))

(define-func pango_attr_iterator_get
  PangoAttribute
  ((PangoAttrIterator iterator)
   (PangoAttrType type)))

;(define-func pango_attr_iterator_get_font
;  none
;  ((PangoAttrIterator)
;   (PangoFontDescription desc)
;   (PangoLanguage* language (read-write))
;   (GList* (read-write))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoTabArray
;; from pango-tabs.h
(define-boxed PangoTabArray ())

(define-func pango_tab_array_new
  PangoTabArray
  ((int initialSize)
   (bool positionInPixels)))

;(define-func pango_tab_array_new_with_positions
;  PangoTabArray
;  ((int size)
;   (bool positionsInPixels)
;   (PangoTabAlign firstAlignment)
;   (int firstPosition)
;   (...)

(define-func pango_tab_array_get_type
  GType
  ())

(define-func pango_tab_array_copy
  PangoTabArray
  ((PangoTabArray src)))

(define-func pango_tab_array_free
  none
  ((PangoTabArray tabArray)))

(define-func pango_tab_array_get_size
  int
  ((PangoTabArray tabArray)))

(define-func pango_tab_array_resize
  none
  ((PangoTabArray tabArray)
   (int newSize)))

(define-func pango_tab_array_set_tab
  none
  ((PangoTabArray tabArray)
   (int tabIndex)
   (PangoTabAlign alignment)
   (int location)))

(define-func pango_tab_array_get_tab
  none
  ((PangoTabArray tabArray)
   (int tabIndex)
   (PangoAlignment alignment)
   (int* location)))

;(define-func pango_tab_array_get_tabs
;  none
;  ((PangoTabArray tabArray)
;   (PangoTabAlign** alignments)
;   (int** locations)))

(define-func pango_tab_array_get_positions_in_pixels
  bool
  ((PangoTabArray tabArray)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoTabAlign
;; from pango-tabs.h
(define-enum PangoTabAlign
  (left PANGO_TAB_LEFT))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PangoGlyphString
;; from pango-glyph.h
(define-boxed PangoGlyphString ())

(define-func pango_glyph_string_new
  PangoGlyphString
  ())

(define-func pango_glyph_string_set_size
  none
  ((PangoGlyphString string)
   (int newLen)))

(define-func pango_glyph_string_get_type
  GType
  ())

(define-func pango_glyph_string_copy
  PangoGlyphString
  ((PangoGlyphString string)))

(define-func pango_glyph_string_free
  none
  ((PangoGlyphString string)))

(define-func pango_glyph_string_extents
  none
  ((PangoGlyphString glyphs)
   (PangoFont font)
   (PangoRectangle inkRect)
   (PangoRectangle logicalRect)))

(define-func pango_glyph_string_extents_range
  none
  ((PangoGlyphString glyphs)
   (int start)
   (int end)
   (PangoFont font)
   (PangoRectangle inkRect)
   (PangoRectangle logicalRect)))

(define-func pango_glyph_string_get_logical_widths
  none
  ((PangoGlyphString glyphs)
   (string text)
   (int length)
   (int embeddingLevel)
   (int* logicalWidths)))

; Need PangoAnalysis
;(define-func pango_glyph_string_index_to_x
;  none
;  ((PangoGlyphString glyphs)
;   (string text)
;   (int length)
;   (PangoAnalysis analysis)
;   (int index)
;   (bool trailing)
;   (int* x_pos)))

; Need PangoAnalysis
;(define-func pango_glyph_string_x_to_index
;  none
;  ((PangoGlyphString glyphs)
;   (string text)
;   (int length)
;   (PangoAnalysis analysis)
;   (int xPos)
;   (int* index)
;   (int* trailing)))

