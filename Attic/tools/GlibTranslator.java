// TODO: figure out the right way to cause a TYPE static variable to
// be spit out for GTK classes, like this:
//     static public GtkType TYPE = getType ();

import java.util.Hashtable;
import java.util.Vector;

/**
 * Class containing methods that bridge the gap between the
 * <code>define-*</code> forms and {@link Sourceable} objects.
 *
 * <p>The Java-Gnome bindings library is free software distributed under the
 * terms of the GNU Library General Public License version 2.</p>
 *
 * @author Dan Bornstein, danfuzz@milk.com
 * @author based on code originally by Olivier Gutknecht, gutkneco@lirmm.fr
 * @author Copyright 2000 Dan Bornstein, all rights reserved. 
 */
public final class GlibTranslator {
	/** non-null; the {@link TypeConverter} to use */
	private TypeConverter myTypeConverter;

	/** non-null; the {@link DefaultTranslator} to use */
	private DefaultTranslator myDefaultTranslator;

	/** non-null; special class to hold otherwise-unmatched GDK functions */
	private ClassSpec myGdkCatchall;

	/** non-null; special class to hold otherwise-unmatched GTK functions */
	private ClassSpec myGtkCatchall;

	/** non-null; special class to hold otherwise-unmatched Gnome functions */
	private ClassSpec myGnomeCatchall;

	/** non-null; special class to hold otherwise-unmatched Bonobo functions */
	private ClassSpec myBonoboCatchall;

	/** count of warnings that occurred during processing */
	private int myWarningCount;

	// ------------------------------------------------------------------------
	// constructors

	/**
	 * Construct an instance.
	 *
	 * @param typeConverter non-null; the {@link TypeConverter} to use
	 * for type translations
	 * @param defaultTranslator non-null; the {@link DefaultTranslator}
	 * to use for translating default argument values
	 */
	public GlibTranslator(
		TypeConverter typeConverter,
		DefaultTranslator defaultTranslator) {
		if (typeConverter == null) {
			throw new NullPointerException("typeConverter == null");
		}

		if (defaultTranslator == null) {
			throw new NullPointerException("defaultTranslator == null");
		}

		myTypeConverter = typeConverter;
		myDefaultTranslator = defaultTranslator;

		myGdkCatchall =
			new ClassSpec(new JavaTypeSpec("org.gnu.gdk.Gdk"), null);
		myGtkCatchall =
			new ClassSpec(new JavaTypeSpec("org.gnu.gtk.Gtk"), null);
		myGnomeCatchall =
			new ClassSpec(new JavaTypeSpec("org.gnu.gnome.Gnome"), null);
		myBonoboCatchall =
			new ClassSpec(new JavaTypeSpec("org.gnu.bonobo.Bonobo"), null);

		myWarningCount = 0;
	}

	// ------------------------------------------------------------------------
	// public instance methods

	/**
	 * Get the array of "catch-all" classes that have all of the leftover
	 * functions that had nowhere else to go.
	 *
	 * @see #translate(DefineFunc)
	 * @return the array of classes
	 */
	public ClassSpec[] getCatchalls() {
		Vector v = new Vector();

		if (myGdkCatchall.getMemberCount() != 0) {
			v.addElement(myGdkCatchall);
		}

		if (myGtkCatchall.getMemberCount() != 0) {
			v.addElement(myGtkCatchall);
		}
		if (myGnomeCatchall.getMemberCount() != 0) {
			v.addElement(myGnomeCatchall);
		}
		if (myBonoboCatchall.getMemberCount() != 0) {
			v.addElement(myBonoboCatchall);
		}
		ClassSpec[] result = new ClassSpec[v.size()];
		v.copyInto(result);
		return result;
	}

	/**
	 * Translate a {@link DefineBoxed} into a {@link ClassSpec} with the
	 * right nascent structure. (It might later get more methods added to it.)
	 *
	 * @param def the definition to translate
	 * @return the translated class 
	 */
	public ClassSpec translate(DefineBoxed def) {
		TypeSpec type = def.getType();
		myTypeConverter.registerBoxedType(type.schemeType());

		TypeSpecIface superclass = def.getExtends();
		if (superclass == null) {
			superclass = new JavaTypeSpec("org.gnu.glib.GBoxed");
		}

		ClassSpec spec = new ClassSpec(type, superclass);

		// iterate over the fields, adding accessors
		ArgumentSpec[] fields = def.getFieldList();
		for (int i = 0; i < fields.length; i++) {
			addFieldAccessors(spec, fields[i]);
		}

		return spec;
	}

	/**
	 * Translate a {@link DefineEnumOrFlags} into a {@link ClassSpec}.
	 *
	 * @param def the definition to translate
	 * @return the translated class 
	 */
	public ClassSpec translate(DefineEnumOrFlags def) {
		// the eventual return value
		ClassSpec spec;

		TypeSpec type = def.getType();
		int variant = def.getVariant();
		switch (variant) {
			case DefineEnumOrFlags.ENUM :
				{
					myTypeConverter.registerEnumType(type.schemeType());
					spec =
						new ClassSpec(
							type,
							new JavaTypeSpec("org.gnu.glib.GEnum"));
					break;
				}
			case DefineEnumOrFlags.FLAGS :
				{
					myTypeConverter.registerFlagsType(type.schemeType());
					spec =
						new ClassSpec(
							type,
							new JavaTypeSpec("org.gnu.glib.GFlags"));
					break;
				}
			case DefineEnumOrFlags.STRING_ENUM :
				{
					myTypeConverter.registerStringEnumType(type.schemeType());
					spec = new ClassSpec(type, new JavaTypeSpec("Object"));
					break;
				}
			default :
				{
					throw new RuntimeException("unknown variant: " + variant);
				}
		}

		String sjType = null;
		if (variant != DefineEnumOrFlags.STRING_ENUM) {
			// not needed for string enums
			if (variant == DefineEnumOrFlags.ENUM
				|| variant == DefineEnumOrFlags.FLAGS) {
				String className = type.publicJavaType();
				int index = className.lastIndexOf(".");
				String shortName = className.substring(index + 1);
				String pkg = className.substring(0, index + 1);
				className = GlibTranslator.javaGnomeName(shortName);
				sjType = pkg + className;

			} else {
				sjType = type.simpleJavaType();
			}
		}

		// table mapping all of the named values, mapping from an Integer
		// to a string
		Hashtable namedValues = new Hashtable();

		// max value found
		int maxValue = 0;

		// iterate over all of the names, issuing appropriate field
		// definitions
		NamedConstantDef[] consts = def.getValueList();
		for (int i = 0; i < consts.length; i++) {
			NamedConstantDef one = consts[i];

			String init = one.getValue();
			String schemeName = one.getSchemeName();

			// fix the scheme name: replace "-" with "_"
			schemeName = schemeName.replace('-', '_');

			// variables cannot start with a number
			char firstChar = schemeName.charAt(0);
			if (Character.isDigit(firstChar)) {
				schemeName = schemeName.substring(1);
				int insertAt = schemeName.indexOf('_');
				if (insertAt > 0)
					schemeName =
						schemeName.substring(0, insertAt)
							+ firstChar
							+ schemeName.substring(insertAt);
				else
					schemeName += firstChar;
			}

			// make the name upper case
			String name = schemeName.toString().toUpperCase();

			if (variant == DefineEnumOrFlags.STRING_ENUM) {
				spec.add(
					new FieldSpec(
						type.publicJavaConstantName(name),
						new JavaTypeSpec("String"),
						true,
						true,
						JavaMemberSpec.SCOPE_PUBLIC,
						"\"" + init + "\""));
			} else {
				String intName = '_' + name;
				int intValue = Integer.parseInt(init);

				// add a mapping from the GLib name to the Java field name
				spec.addConstantTranslation(one.getGlibName(), intName);

				if (variant == DefineEnumOrFlags.FLAGS) {
					init = "1 << " + init;
					intValue = 1 << intValue;
				}

				if (intValue > maxValue) {
					maxValue = intValue;
				}

				namedValues.put(new Integer(intValue), name);

				spec.add(
					new FieldSpec(
						intName,
						new JavaTypeSpec("int"),
						true,
						true,
						JavaMemberSpec.SCOPE_PUBLIC,
						init));

				init = "new " + sjType + " (" + intName + ")";
				spec.add(
					new FieldSpec(
						name,
						type,
						true,
						true,
						JavaMemberSpec.SCOPE_PUBLIC,
						init));
			}
		}

		if (variant == DefineEnumOrFlags.STRING_ENUM) {
			// no more to add for a string enum
			return spec;
		}

		// add the array part of the intern table
		StringBuffer sb = new StringBuffer();
		sb.append("new ");
		sb.append(sjType);
		sb.append("[] { ");
		if (maxValue > 255) {
			// never spit out more than a 256 element array
			maxValue = 255;
		}
		for (int i = 0; i <= maxValue; i++) {
			if (i != 0) {
				sb.append(", ");
			}
			String init = (String) namedValues.get(new Integer(i));
			if (init == null) {
				init = "new " + sjType + " (" + i + ")";
			}
			sb.append(init);
		}
		sb.append(" }");
		spec.add(
			new FieldSpec(
				"theInterned",
				new JavaTypeSpec(sjType + "[]"),
				true,
				true,
				JavaMemberSpec.SCOPE_PRIVATE,
				sb.toString()));

		// add the intern table declaration
		spec.add(
			new FieldSpec(
				"theInternedExtras",
				new JavaTypeSpec("java.util.Hashtable"),
				true,
				false,
				JavaMemberSpec.SCOPE_PRIVATE,
				null));

		// add the declaration for the "sacrificial" element used
		// during interning
		spec.add(
			new FieldSpec(
				"theSacrificialOne",
				new JavaTypeSpec(sjType),
				true,
				true,
				JavaMemberSpec.SCOPE_PRIVATE,
				"new " + sjType + " (0)"));

		// the argument list for both the constructor and the intern
		// method
		ArgumentList argList =
			new ArgumentList(
				new ArgumentSpec[] {
					 new ArgumentSpec(
						myTypeConverter,
						"int",
						"value",
						null,
						false,
						false,
						false)});

		// add the actual intern method
		String internCode =
			"if (value < theInterned.length)\n"
				+ "{ return theInterned[value]; }\n"
				+ "theSacrificialOne.value_ = value;\n"
				+ "if (theInternedExtras == null)\n"
				+ "{ theInternedExtras = new java.util.Hashtable(); }\n"
				+ sjType
				+ " already  = ("
				+ sjType
				+ ") theInternedExtras.get (theSacrificialOne);\n"
				+ "if (already == null) {\n"
				+ "already = new "
				+ sjType
				+ "(value);\n"
				+ "theInternedExtras.put(already, already);\n"
				+ "}\n"
				+ "return already;\n";
		spec.add(
			new JavaMethodSpec(
				"intern",
				new JavaTypeSpec(sjType),
				true,
				false,
				JavaMemberSpec.SCOPE_PUBLIC,
				argList,
				internCode));

		// the constructor is private; all public instantiation happens
		// via the interning mechanism
		spec.add(
			new JavaConstructorSpec(
				type,
				JavaMemberSpec.SCOPE_PRIVATE,
				argList,
				"value_ = value;"));

		// the argument list for the or, and, xor, and test methods
		argList =
			new ArgumentList(
				new ArgumentSpec[] {
					 new ArgumentSpec(
						myTypeConverter,
						type.simpleJavaType(),
						"other",
						null,
						false,
						false,
						false)});

		String ORCode = "return intern(value_ | other.value_);";
		String ANDCode = "return intern(value_ & other.value_);";
		String XORCode = "return intern(value_ ^ other.value_);";
		String testCode = "return (value_ & other.value_) == other.value_;";

		spec.add(
			new JavaMethodSpec(
				"or",
				new JavaTypeSpec(sjType),
				false,
				false,
				JavaMemberSpec.SCOPE_PUBLIC,
				argList,
				ORCode));
		spec.add(
			new JavaMethodSpec(
				"and",
				new JavaTypeSpec(sjType),
				false,
				false,
				JavaMemberSpec.SCOPE_PUBLIC,
				argList,
				ANDCode));
		spec.add(
			new JavaMethodSpec(
				"xor",
				new JavaTypeSpec(sjType),
				false,
				false,
				JavaMemberSpec.SCOPE_PUBLIC,
				argList,
				XORCode));
		spec.add(
			new JavaMethodSpec(
				"test",
				new JavaTypeSpec("boolean"),
				false,
				false,
				JavaMemberSpec.SCOPE_PUBLIC,
				argList,
				testCode));

		return spec;
	}

	/**
	 * Translate a {@link DefineObject} into a {@link ClassSpec} with the
	 * right nascent structure. (It might later get more methods added to it.)
	 *
	 * @param def the definition to translate
	 * @return the translated class 
	 */
	public ClassSpec translate(DefineObject def) {
		TypeSpec type = def.getType();
		myTypeConverter.registerObjectType(type.schemeType());

		TypeSpecIface superclass = def.getExtends();
		if (superclass == null) {
			superclass = new JavaTypeSpec("org.gnu.glib.GObject");
		}

		// the object we will eventually return
		ClassSpec spec = new ClassSpec(type, superclass, def.getComment());

		// iterate over the fields, adding accessors
		ArgumentSpec[] fields = def.getFieldList();
		for (int i = 0; i < fields.length; i++) {
			addFieldAccessors(spec, fields[i]);
		}

		// if it's struct constructor we also need to add C side
		// routines..
		if (def.getStructConstructorFlag()) {
			spec.add(
				new StructConstructorSpec(
					spec.getType(),
					JavaMemberSpec.SCOPE_PUBLIC));
		}

		return spec;
	}

	/**
	 * Translate a {@link DefineFunc} into a {@link ClassSpec} which
	 * has the right type and contains appropriate member declarations.
	 * Depending on the form of the function, the result could contain
	 * multiple constructors and methods, including native declarations
	 * and associated native code. If there's no obvious normal class
	 * to attach a function to, then one of three "catch-all" classes
	 * are used (one each for GDK, GTK, and Gnome); in this case, this
	 * method returns <code>null</code> (see {@link #getCatchalls}).
	 *
	 * @param def the definition to translate
	 * @return null-ok; the translated class, or <code>null</code> if
	 * this was handled as a "catch-all"
	 */
	public ClassSpec translate(DefineFunc def) {
		String name = def.getSchemeName();

		// this is the type we will presume this is to be defined on
		String presumedName = myTypeConverter.fuzzyTypeMatch(name);
		ArgumentList argl = def.getArgumentList();

		if (presumedName == null
			&& argl != null
			&& argl.getExplicitObject() != null)
			presumedName = argl.getExplicitObject().schemeType();

		if (presumedName == null) {
			// if there's no matching type, then bind it as a static method
			// to one of the catchalls
			ClassSpec addTo;
			if (name.startsWith("gdk_")) {
				addTo = myGdkCatchall;
			} else if (name.startsWith("gtk_")) {
				addTo = myGtkCatchall;
			} else if (name.startsWith("gnome_")) {
				if (myGnomeCatchall.getMemberCount() == 0) {
					// add the static init part if there haven't been
					// any declarations yet
					String body =
						"System.loadLibrary (\"GTKJava\");\n"
							+ "System.loadLibrary (\"GNOMEJava\");\n";
					myGnomeCatchall.add(
						new JavaMethodSpec(
							"<clinit>",
							new JavaTypeSpec("void"),
							true,
							false,
							JavaMemberSpec.SCOPE_PRIVATE,
							new ArgumentList(null),
							body,
							def.getComment()));
				}
				addTo = myGnomeCatchall;
			} else if (name.startsWith("bonobo_")) {
				addTo = myBonoboCatchall;
			} else {
				throw new RuntimeException(
					"error: have no idea what class "
						+ "to attach this function to: "
						+ name);
			}

			ClassSpec spec = translateAsStaticMethod(def, addTo.getType());
			addTo.addMembers(spec);
			return null;
		}

		TypeSpec presumedType = new TypeSpec(myTypeConverter, presumedName);

		return translateAsStaticMethod(def, presumedType);

	}

	/**
	 * Add an empty protected no-arg constructor to the given {@link
	 * ClassSpec} if it doesn't already define a no-arg constructor.
	 *
	 * @param spec the class to (possibly) modify 
	 */
	public void addConstructorIfNecessary(ClassSpec spec) {
		JavaMemberSpec[] mems = spec.getMembers();
		for (int i = 0; i < mems.length; i++) {
			if (mems[i] instanceof JavaConstructorSpec) {
				JavaConstructorSpec cspec = (JavaConstructorSpec) mems[i];
				if (cspec.getArgumentList().getCount() == 0) {
					// found one; no need to do anything
					return;
				}
			}
			if (mems[i] instanceof StructConstructorSpec)
				return;
		}

		// didn't find one; gotta add it

		spec.add(
			new JavaConstructorSpec(
				spec.getType(),
				JavaMemberSpec.SCOPE_PROTECTED,
				new ArgumentList(null),
				"// this space intentionally left blank\n"));
	}

	// ------------------------------------------------------------------------
	// private instance methods

	/**
	 * Get the count of warnings that occurred while using this instance.
	 *
	 * @return the warning count
	 */
	public int getWarningCount() {
		return myWarningCount;
	}

	/**
	 * Translate a {@link DefineFunc} into a form which treats it as a
	 * constructor for the given type.
	 *
	 * @param def the definition to translate
	 * @param presumedType the type to presume the result should be
	 * @return the translated class
	 */
	private ClassSpec translateAsConstructor(
		DefineFunc def,
		TypeSpec presumedType) {
		TypeSpec ret = def.getReturnType();

		if (!ret.publicJavaSignature().startsWith("L")) {
			// the supposed constructor type is for a non-class; that
			// can't happen
			throw new RuntimeException(
				"error: constructor function ("
					+ def.getSchemeName()
					+ ") declared a non-class return "
					+ "type: "
					+ ret);
		}

		// check to see if the return type matches our presumption
		if (!presumedType.equals(ret)) {
			// they don't match; issue a warning, but keep going, assuming
			// that the function name has more authority than the declared
			// return type (the typical case is that the return type is
			// declared to be a superclass of the type that the function is
			// defined on, but this isn't actually checked)
			System.err.println(
				"warning: constructor function's "
					+ "name ("
					+ def.getSchemeName()
					+ ") doesn't match its return type: "
					+ ret
					+ "; binding it to: "
					+ presumedType);
			myWarningCount++;
			def = def.withReturnType(presumedType);
		}

		// first, convert it as static method that does the actual
		// underlying call; note, since lots of constructor names would be
		// identical after munging (e.g., turn into "nNew" or
		// "newWithLabel"), and some of them share an inheritence
		// relationship that would cause the compiler to complain if there
		// were more than one method with the same name, we don't translate
		// the GLib names of constructors. They just get spit out as
		// "<type-name>_new" or what-have-you.
		String schemeName = def.getSchemeName();

		ClassSpec spec =
			translateAsMethod2(
				presumedType,
				schemeName,
				presumedType,
				true,
				def.getArgumentList(),
				schemeName,
				null);

		// create the public constructor(s), using calls to the static
		// method we just defined

		ArgumentList argList = def.getArgumentList();
		int defaultCount = argList.getDefaultCount();

		for (int i = 0; i <= defaultCount; i++) {
			ArgumentList defArgl = argList.withoutLast(i);
			StringBuffer body = new StringBuffer(100);
			body.append("nativepeer = ");
			body.append(schemeName);
			body.append(" (");
			body.append(
				argList.publicJavaActualsForDefault(myDefaultTranslator, i));
			body.append(").nativepeer;\n");
			spec.add(
				new JavaConstructorSpec(
					presumedType,
					JavaMemberSpec.SCOPE_PUBLIC,
					defArgl,
					body.toString(),
					def.getComment()));
		}

		return spec;
	}

	/**
	 * Translate a {@link DefineFunc} into a form which treats it as
	 * a static method on the given type.
	 *
	 * @param def the definition to translate
	 * @param type the type representing the class that the result should
	 * be an instance of
	 * @return the translated class 
	 */
	private ClassSpec translateAsStaticMethod(
		DefineFunc def,
		TypeSpecIface type) {
		ArgumentList argl = def.getArgumentList();
		return translateAsMethod1(def, type, true, argl);
	}

	/**
	 * Translate a {@link DefineFunc} into a form which treats it as
	 * an instance method on the class of the type of its first argument.
	 *
	 * @param def the definition to translate
	 * @param type the type that the method is associated with
	 * @param isStatic whether or not the method should be static
	 * @param argList the argument list to the method (contains the
	 * object argument!)
	 * @return the translated class 
	 */
	private ClassSpec translateAsMethod1(
		DefineFunc def,
		TypeSpecIface type,
		boolean isStatic,
		ArgumentList argList) {
		// the underlying function to call
		String schemeName = def.getSchemeName();

		// the public Java method name
		String pubName = schemeName;

		// the return type
		TypeSpec retType = def.getReturnType();

		// translate the full argument list form, including all of
		// the native code and (un)wrapping methods
		ClassSpec spec =
			translateAsMethod2(
				type,
				pubName,
				retType,
				isStatic,
				argList,
				schemeName,
				def.getComment());

		// now, add a method for each of the possible sets of defaulted
		// arguments

		// XXX: this needs to be fixed to work with the arglist
		// containing object type..

		int defaultCount = argList.getDefaultCount();
		for (int i = 1; i <= defaultCount; i++) {
			ArgumentList defArgl = argList.withoutLast(i);
			String defBody = publicDefaultBody(retType, argList, pubName, i);

			spec.add(
				new JavaMethodSpec(
					pubName,
					retType,
					isStatic,
					false,
					JavaMemberSpec.SCOPE_PUBLIC,
					defArgl,
					defBody,
					def.getComment()));
		}

		return spec;
	}

	/**
	 * Construct a {@link ClassSpec} which consists of a public method
	 * capable of calling through to the given raw function, with the
	 * given other properties (public name, return type, etc). This does
	 * <i>not</i> do any default argument processing.
	 *
	 * @param type the type that the method is associated with
	 * @param pubName the public name for the method
	 * @param retType the return type for the method
	 * @param isStatic whether or not the method should be static
	 * @param argList the argument list to the method
	 * @param rawFuncName the name of the lowest-level function to call
	 * @return the new class
	 */
	private ClassSpec translateAsMethod2(
		TypeSpecIface type,
		String pubName,
		TypeSpec retType,
		boolean isStatic,
		ArgumentList argList,
		String rawFuncName,
		String comment) {
		// make the class we will eventually return
		ClassSpec spec = new ClassSpec(type, null);

		// the statements to unwrap the public arguments into native form
		String pubToNative = argList.publicToNativeJavaStatements();

		// whether the native method is wrapped with a public implementation
		boolean wrapNative = false;
		//retType.requiresNativeToPublicJavaReturn()
		//|| (pubToNative.length() != 0);

		// the native method body
		String nativeBody =
			nativeMethodBody(
				isStatic ? null : type,
				retType,
				argList,
				rawFuncName);

		if (!wrapNative) {
			// the native method *is* the public method; that is, no
			// need to wrap it because its arguments are all fine for
			// public consumption
			spec.add(
				new NativeMethodSpec(
					type,
					pubName,
					retType,
					isStatic,
					true,
					JavaMemberSpec.SCOPE_PROTECTED,
					argList,
					nativeBody,
					""));
		} else {
			// the native method needs wrapping to make it palatable in
			// public; so include a private native method which uses the
			// unwrapped types, and a public method which does the
			// (un)wrapping

			String natName = pubName + '0';

			spec.add(
				new NativeMethodSpec(
					type,
					natName,
					retType,
					true,
					true,
					JavaMemberSpec.SCOPE_PROTECTED,
					argList,
					nativeBody));

			String publicBody = publicWrapperBody(retType, argList, natName);

			spec.add(
				new JavaMethodSpec(
					pubName,
					retType,
					isStatic,
					false,
					JavaMemberSpec.SCOPE_PUBLIC,
					argList,
					publicBody,
					comment));
		}

		return spec;
	}

	/**
	 * Return the C source native method body for a method which just
	 * translates to a call to a GLib/GTK method.
	 *
	 * @param targetType null-ok; the type the method is associated with, or
	 * <code>null</code> if this is for a static method
	 * @param returnType non-null; the return type for the method
	 * @param argumentList non-null; the list of arguments to the method
	 * @param glibFuncName non-null; the name of the GLib/GTK function to
	 * call through to
	 * @return the native method body 
	 */
	private String nativeMethodBody(
		TypeSpecIface targetType,
		TypeSpec returnType,
		ArgumentList argumentList,
		String glibFuncName) {
		boolean isStatic = (targetType == null);
		StringBuffer result = new StringBuffer(1000);

		// get the argument variable conversions
		String[] javaToGlib = argumentList.javaToGlibNativeStatements();

		// append the initial declarations and conversions
		result.append(javaToGlib[0]);

		// construct the call
		String call = glibFuncName + " (";
		if (isStatic) {
			call += argumentList.glibVariableNames();
		} else {
			//call += "cptr";
			String gvn = argumentList.glibVariableNames();
			if (gvn.length() != 0) {
				//call += ", " + gvn;
				call += gvn;
			}
		}
		call += ")";

		// append the call/cleanup/return code
		result.append(returnType.glibToNativeReturn(call, javaToGlib[1]));

		return result.toString();
	}

	/*
	 * Return the Java source for a method body which does (un)wrapping
	 * conversions to call through to another Java method, which takes
	 * native-unwrapped arguments (presumably, but not necessarily, a
	 * native method).
	 *
	 * @param returnType non-null; the return type for the method
	 * @param argumentList non-null; the list of arguments to the method
	 * @param methodToCall non-null; the name of the method to call through
	 * to
	 * @return the method body 
	 */
	private String publicWrapperBody(
		TypeSpec returnType,
		ArgumentList argumentList,
		String methodToCall) {
		StringBuffer result = new StringBuffer();

		result.append(argumentList.publicToNativeJavaStatements());

		String call =
			methodToCall + " (" + argumentList.nativeJavaVariableNames() + ")";

		result.append(returnType.nativeToPublicJavaReturn(call));

		return result.toString();
	}

	/*
	 * Return the Java source for a method body which just calls
	 * through to the same method name, using the given number of
	 * default arguments.
	 *
	 * @param returnType non-null; the return type for the method
	 * @param argumentList non-null; the list of arguments to the method
	 * @param methodToCall non-null; the name of the method to call through
	 * to
	 * @param howMany how many default arguments to use
	 * @return the method body 
	 */
	private String publicDefaultBody(
		TypeSpec returnType,
		ArgumentList argumentList,
		String methodToCall,
		int howMany) {
		StringBuffer result = new StringBuffer();

		if (!returnType.publicJavaType().equals("void")) {
			result.append("return ");
		}

		result.append(methodToCall);

		result.append(" (");
		result.append(
			argumentList.publicJavaActualsForDefault(
				myDefaultTranslator,
				howMany));
		result.append(");\n");

		return result.toString();
	}

	/**
	 * Add appropriate field accessor native methods to the given class.
	 * FIXME: This ignores read-only (assumes true) and null-ok (ditto)
	 * flags.
	 *
	 * @param spec the {@link ClassSpec} to add to
	 * @param field the field to make an accessor for 
	 */
	private void addFieldAccessors(ClassSpec spec, ArgumentSpec field) {
		// the type for the class
		TypeSpecIface type = spec.getType();

		// the formal name of the field
		String fieldName = field.getFormalName();

		// the type of the field
		TypeSpec fieldType = field.getType();

		// argument spec for glib-level pointer to the object
		ArgumentSpec cptr =
			new ArgumentSpec(
				myTypeConverter,
				type.schemeType(),
				"cptr",
				null,
				true,
				true,
				false);

		// "this" obj argument spec
		ArgumentSpec obj =
			new ArgumentSpec(
				myTypeConverter,
				type.schemeType(),
				"obj",
				null,
				true,
				true,
				false);

		// make the getter and accoutrements

		String rawGetterName = type.schemeType() + "_get_" + fieldName;
		String getterName = type.publicJavaMethodName(rawGetterName);

		// Remember that since translatemethod2 doesn't anymore do
		// explicit object args we have to add it to the getterArgs
		// ourselves
		ArgumentList getterArgs = new ArgumentList(new ArgumentSpec[] { obj });

		// make the C function that actually does the getting
		getterArgs = new ArgumentList(new ArgumentSpec[] { cptr });
		String getterBody = "return cptr->" + fieldName + ";";
		spec.add(
			new CFunctionSpec(
				rawGetterName,
				fieldType,
				getterArgs,
				getterBody));

		ClassSpec getter =
			translateAsMethod2(
				type,
				getterName,
				fieldType,
				true,
				getterArgs,
				rawGetterName,
				null);

		spec.addMembers(getter);

		if (field.getReadOnly()) {
			// read-only; no setter
			return;
		}

		// make the setter and accoutrements

		String rawSetterName = type.schemeType() + "_set_" + fieldName;
		String setterName = type.publicJavaMethodName(rawSetterName);
		ArgumentList setterArgs =
			new ArgumentList(new ArgumentSpec[] { obj, field.withoutDefault()});

		// make the C function that actually does the setting
		setterArgs =
			new ArgumentList(
				new ArgumentSpec[] { cptr, field.withoutDefault()});

		String setterBody = "cptr->" + fieldName + " = " + fieldName + ";";
		spec.add(
			new CFunctionSpec(
				rawSetterName,
				new TypeSpec(myTypeConverter, "none"),
				setterArgs,
				setterBody));

		ClassSpec setter =
			translateAsMethod2(
				type,
				setterName,
				new TypeSpec(myTypeConverter, "none"),
				false,
				setterArgs,
				rawSetterName,
				null);

		spec.addMembers(setter);
	}

	/**
	 * Method that will convert the SchemeName to the
	 * class name used by java-gnome
	 */
	public static String javaGnomeName(String name) {
		if (name.startsWith("Gdk")) {
			if (name.length() == 3)
				return name;
			return name.substring(3);
		} else if (name.startsWith("Gtk")) {
			if (name.length() == 3)
				return name;
			if ("GtkObject".equals(name))
				return name;
			return name.substring(3);
		} else if (name.startsWith("Gnome")) {
			if (name.length() == 5)
				return name;
			return name.substring(5);
		} else if (name.startsWith("Atk")) {
			if ("AtkObject".equals(name))
				return name;
			if (name.length() == 3)
				return name;
			return name.substring(3);
		} else if (name.startsWith("Bonobo")) {
			if (name.length() == 6)
				return name;
			return name.substring(6);
		} else if (name.startsWith("Pango")) {
			if (name.length() == 5)
				return name;
			return name.substring(5);
		} else if (
			name.startsWith("GType")
				|| name.startsWith("GValue")
				|| name.startsWith("GError")
				|| name.startsWith("GSpawnError")
				|| name.startsWith("GQuark")
				|| name.startsWith("GClosure")
				|| name.startsWith("GBoxed")
				|| name.startsWith("GEnum")
				|| name.startsWith("GFlags")
				|| name.startsWith("GTypeInterface")
				|| name.startsWith("GList")) {
			return name.substring(1);
		} else {
			return name;
		}
	}
}
