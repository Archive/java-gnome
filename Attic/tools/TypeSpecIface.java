/**
 * Interface for specification of a single type. This is a hairy area
 * of the system right now; Java types and Scheme types end up getting
 * intermingled in bad ways, and this interface (which ends up getting
 * implemented by throwing lots of exceptions for the most pernicious
 * cases) is a first step toward the road to disentangelment.
 *
 * <p>The Java-Gnome bindings library is free software distributed under the
 * terms of the GNU Library General Public License version 2.</p>
 *
 * @author Dan Bornstein, danfuzz@milk.com
 * @author Copyright 2000 Dan Bornstein, all rights reserved. 
 */
public interface TypeSpecIface
{
    /**
     * Get the Scheme form for this instance.
     *
     * @return non-null; the Scheme form
     */
    public String schemeType ();

    /**
     * Get the public Java type name for this instance. For classes,
     * this returns the fully qualified class name.
     *
     * @return non-null; the Java type 
     */
    public String publicJavaType ();

    /**
     * Get just the package name for the public Java type for this instance.
     * This throws an exception if the type doesn't have a public Java
     * class.
     *
     * @return non-null; the public Java package name 
     */
    public String publicJavaPackage ();

    /**
     * Get just the simple (public) Java type for this instance. For
     * classes, this returns the unqualified class name.
     *
     * @return non-null; the simple Java class name 
     */
    public String simpleJavaType ();

    /**
     * Return the Java signature corresponding to the public Java
     * type for this instance.
     *
     * @return non-null; the Java type signature string
     */
    public String publicJavaSignature ();

    /**
     * Given a Scheme function name, get the right name for a public Java
     * method on this type.
     * 
     * @param schemeName non-null; the Scheme function name
     * @return non-null; the corresponding public Java method name
     */
    public String publicJavaMethodName (String schemeName);

    /**
     * Given a Scheme constant name, get the right name for a public Java
     * constant on this type.
     * 
     * @param schemeName non-null; the Scheme constant name
     * @return non-null; the corresponding public Java constant name
     */
    public String publicJavaConstantName (String schemeName);

    /**
     * Get the native Java type name for this instance.
     *
     * @return non-null; the Java type 
     */
    public String nativeJavaType ();

    /**
     * Return the Java signature corresponding to the Java level of a
     * native declaration for this instance.
     *
     * @return non-null; the Java type signature string
     */
    public String nativeJavaSignature ();

    /**
     * Return the C level type for this instance as an argument at the
     * C level to a native Java method.
     *
     * @return non-null; the native type
     */
    public String nativeType ();

    /**
     * Return <code>true</code> if code needs to be used to wrap
     * a native return value into a public Java return value, for this
     * instance's type, or <code>false</code> otherwise.
     *
     * @return whether a native return value must be wrapped
     */
    public boolean requiresNativeToPublicJavaReturn ();

    /**
     * Return the Java level wrapping code to convert a call of this
     * instance's type from the native Java type to a return the public
     * Java type.
     *
     * @param call non-null; the function/method call to wrap
     * @return non-null; the formatted string
     */
    public String nativeToPublicJavaReturn (String call);

    /**
     * Return the C level wrapping code to convert a call of this
     * instance's type from the underlying GLib/GTK type to a return of the
     * native Java type.
     *
     * @param call non-null; the function/method call to wrap
     * @param cleanup non-null; a set of cleanup statements to execute
     * before returning
     * @return non-null; the formatted string 
     */
    public String glibToNativeReturn (String call, String cleanup);

    /**
     * Get the raw GLib/GTK type corresponding to this instance's type, to
     * pass as an argument or accept as a return value.
     *
     * @return non-null; the raw GLib/GTK type 
     */
    public String glibType ();
}
