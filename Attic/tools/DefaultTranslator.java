/**
 * Interface for translating default values into strings usable in
 * Java.
 *
 * <p>The Java-Gnome bindings library is free software distributed under the
 * terms of the GNU Library General Public License version 2.</p>
 *
 * @see ArgumentSpec#getDefaultValue
 * @see ArgumentList@publicJavaActualsForDefault
 *
 * @author Dan Bornstein, danfuzz@milk.com
 * @author Copyright 2000 Dan Bornstein, all rights reserved. 
 */
public interface DefaultTranslator
{
    /**
     * For the given type, translate the given default value (which may be
     * a simple expression) into a suitable expression in Java. 
     *
     * @param schemeType the name of the Scheme type of the value
     * @param expr the expression denoting the value
     */
    public String translate (String schemeType, String expr);
}
