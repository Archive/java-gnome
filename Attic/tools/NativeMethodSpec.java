/**
 * Representation of a native method on a class. In addition to the
 * standard Java member properties, it has a target type (that is, the
 * class it is defined on), an argument list, and a piece of C code.
 *
 * <p>The Java-Gnome bindings library is free software distributed under the
 * terms of the GNU Library General Public License version 2.</p>
 *
 * @author Dan Bornstein, danfuzz@milk.com
 * @author Copyright 2000 Dan Bornstein, all rights reserved. 
 */
final public class NativeMethodSpec extends JavaMemberSpec {
	/** non-null; the type this is defined on (that is, what class it
	 * should be a member of) */
	private TypeSpecIface myTargetType;

	/** non-null; the list of arguments to the method */
	private ArgumentList myArgumentList;

	/** non-null; the C code for the method body */
	private String myBody;

	/** non-null; the comment associated with this method */
	private String myComment;

	// ------------------------------------------------------------------------
	// constructor

	public NativeMethodSpec(
		TypeSpecIface targetType,
		String name,
		TypeSpecIface returnType,
		boolean isStatic,
		boolean isFinal,
		int scope,
		ArgumentList argumentList,
		String body) {
		this(
			targetType,
			name,
			returnType,
			isStatic,
			isFinal,
			scope,
			argumentList,
			body,
			null);
	}

	/**
	 * Construct an instance.
	 *
	 * @param targetType non-null; the target type (that is, what class the
	 * instance should be a member of)
	 * @param name non-null; the name of the method
	 * @param returnType non-null; the return type of the method
	 * @param isStatic <code>true</code> if this is to be a
	 * <code>static</code> method, <code>false</code> if this is to be an
	 * instance method
	 * @param isFinal <code>true</code> if this is to be a
	 * <code>final</code> method, <code>false</code> if not
	 * @param scope the scope of the instance, represented by one of
	 * these constants: {@link #SCOPE_PUBLIC}, {@link #SCOPE_PROTECTED},
	 * {@link #SCOPE_PACKAGE}, or {@link #SCOPE_PRIVATE}
	 * @param argumentList non-null; list of arguments to the method
	 * @param body non-null; the C code for the method body; it should not
	 * have braces around its entirety (those are provided for you at
	 * no additional cost)
	 * @param comment non-null; the comment associated with this method.
	 */
	public NativeMethodSpec(
		TypeSpecIface targetType,
		String name,
		TypeSpecIface returnType,
		boolean isStatic,
		boolean isFinal,
		int scope,
		ArgumentList argumentList,
		String body,
		String comment) {
		super(name, returnType, isStatic, isFinal, scope);

		if (targetType == null) {
			throw new NullPointerException("targetType == null");
		}

		if (argumentList == null) {
			throw new NullPointerException("argumentList == null");
		}

		if (body == null) {
			throw new NullPointerException("body == null");
		}

		myTargetType = targetType;
		myArgumentList = argumentList;
		myBody = body;
		myComment = comment;
	}

	// ------------------------------------------------------------------------
	// public instance methods

	/**
	 * Return the Java source for this instance.
	 *
	 * @return the Java source
	 */
	public String javaSource() {
		StringBuffer sb = new StringBuffer();

		sb.append("native ");
		sb.append(getModifiers());
		sb.append(' ');
		sb.append(myType.nativeJavaType());
		sb.append(' ');
		sb.append(myName);
		sb.append(" (");
		sb.append(myArgumentList.nativeJavaArguments());
		sb.append(");\n");

		return sb.toString();
	}

	/**
	 * Return the C source for this instance.
	 *
	 * @return the C source
	 */
	public String cSource() {
		StringBuffer sb = new StringBuffer();

		sb.append(cFunctionDeclaraion());
		sb.append("\n");
		sb.append("{\n");
		sb.append(myBody);
		sb.append("}\n");

		return sb.toString();
	}

	/**
	 * Return the C header for this instance.
	 *
	 * @return the C header
	 */
	public String cHeader() {
		return cFunctionDeclaraion() + ";\n";
		// wow, that was easy!
	}

	// ------------------------------------------------------------------------
	// private instance methods

	/**
	 * Return the C level function declaration for the method.
	 *
	 * @return the C level function declaration for the method
	 */
	private String cFunctionDeclaraion() {
		StringBuffer result = new StringBuffer();

		result.append("/*\n");

		result.append(" * Class:     ");
		String className = myTargetType.publicJavaType();
		int index = className.lastIndexOf(".");
		String shortName = className.substring(index+1);
		String pkg = className.substring(0, index+1);
		className = GlibTranslator.javaGnomeName(shortName);
		result.append(pkg + className);
		result.append("\n");

		result.append(" * Method:    ");
		result.append(myName);
		result.append("\n");

		result.append(" * Signature: (");
		result.append(myArgumentList.nativeJavaSignature());
		result.append(")");
		result.append(myType.nativeJavaSignature());
		result.append("\n");

		result.append(" */\n");

		result.append("JNIEXPORT ");
		result.append(myType.nativeType());
		result.append(" JNICALL ");
		result.append(cFunctionName(myTargetType, myName));
		result.append(" (JNIEnv *env, ");

		//result.append (myIsStatic ? "jclass cls" : "jobject obj");
		// XXX
		result.append(myIsStatic ? "jclass cls" : "");

		String natArgs = myArgumentList.nativeArguments();
		if (natArgs.length() != 0) {
			if (myIsStatic)
				result.append(", ");
			result.append(natArgs);
		}

		result.append(")");

		return result.toString();
	}

	// ------------------------------------------------------------------------
	// private static methods

	/**
	 * Return the C function name corresponding to the native method
	 * for the an object of the given type with the given name.
	 *
	 * @param type the type of the declaring object
	 * @param name the method name
	 */
	private static String cFunctionName(TypeSpecIface type, String name) {
		StringBuffer sb = new StringBuffer(100);
		sb.append("Java_");

		String javaName = type.publicJavaType();
		int index = javaName.lastIndexOf(".");
		String simple = javaName.substring(index+1);
		String rawName = javaName.substring(0, index);

		simple = GlibTranslator.javaGnomeName(simple);

		rawName += '.' + simple + '.' + name;
		int len = rawName.length();

		for (int i = 0; i < len; i++) {
			char c = rawName.charAt(i);
			switch (c) {
				case '.' :
					sb.append("_");
					break;
				case '_' :
					sb.append("_1");
					break;
				case ';' :
					sb.append("_2");
					break;
				case '[' :
					sb.append("_3");
					break;
				default :
					{
						if ((c >= 'a' && c <= 'z')
							|| (c >= 'A' && c <= 'Z')
							|| (c >= '0' && c <= '9')) {
							sb.append(c);
						} else {
							// FIXME: maybe this is right, but see the text of
							// the exception that's thrown anyway for the
							// explanation
							sb.append("_0");
							String digits =
								Integer
									.toString(((int) c) + 0x10000, 16)
									.toUpperCase();
							sb.append(digits.substring(digits.length() - 4));
							throw new RuntimeException(
								"The JNI spec doesn't say if weird characters "
									+ "get represented in decimal or hex, and if in "
									+ "hex, what case.  Name:"
									+ name);
						}
						break;
					}
			}
		}

		return sb.toString();
	}
}
