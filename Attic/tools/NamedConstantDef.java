/**
 * Representation of a named constant, such as an element of an enumerated
 * or flag type. It consists of a Scheme name, a GLib/GTK name, and a 
 * value.
 *
 * <p>The Java-Gnome bindings library is free software distributed under the
 * terms of the GNU Library General Public License version 2.</p>
 *
 * @author Dan Bornstein, danfuzz@milk.com
 * @author Copyright 2000 Dan Bornstein, all rights reserved. 
 */
final public class NamedConstantDef
{
    /** the Scheme name */
    private String mySchemeName;

    /** the GLib/GTK name */
    private String myGlibName;

    /** the value */
    private String myValue;



    // ------------------------------------------------------------------------
    // constructor

    /**
     * Construct an instance.
     *
     * @param schemeName non-null; the Scheme name
     * @param glibName non-null; the GLib/GTK name
     * @param value non-null; the value
     */
    public NamedConstantDef (String schemeName, String glibName,
			     String value)
    {
	if (schemeName == null)
	{
	    throw new NullPointerException ("schemeName == null");
	}

	if (glibName == null)
	{
	    throw new NullPointerException ("glibName == null");
	}

	if (value == null)
	{
	    throw new NullPointerException ("value == null");
	}

	mySchemeName = schemeName;
	myGlibName = glibName;
	myValue = value;
    }



    // ------------------------------------------------------------------------
    // public instance methods

    /**
     * Return the string form for this instance.
     *
     * @return the string form
     */
    public String toString ()
    {
	return "(" + mySchemeName + " " + myGlibName + 
	    " (= \"" + myValue + "\"))";
    }

    /**
     * Get the Scheme name.
     *
     * @return the Scheme name
     */
    public String getSchemeName ()
    {
	return mySchemeName;
    }

    /**
     * Get the GLib/GTK name.
     *
     * @return the GLib/GTK name
     */
    public String getGlibName ()
    {
	return myGlibName;
    }

    /**
     * Get the value.
     *
     * @return the value
     */
    public String getValue ()
    {
	return myValue;
    }
}
