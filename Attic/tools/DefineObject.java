/**
 * Representation of a Scheme object declaration, which will eventually
 * get translated into a {@link ClassDefinition}.
 * It consists of a type (that is, the type being defined),
 * optional superclass type, and optional field list.
 *
 * <p>The Java-Gnome bindings library is free software distributed under the
 * terms of the GNU Library General Public License version 2.</p>
 *
 * @author Dan Bornstein, danfuzz@milk.com
 * @author Copyright 2000 Dan Bornstein, all rights reserved. 
 */
final public class DefineObject
{
    /** non-null; the type being defined */
    private TypeSpec myType;

    /** null-ok; the superclass's type */
    private TypeSpec myExtends;

    /** non-null; the field list */
    private ArgumentSpec[] myFieldList;

    /** true if the "struct-constructor" option was set */
    private boolean myStructConstructorFlag;

    /** holds a comment that will be included in the generated Java class */
    private String myComment;

    // ------------------------------------------------------------------------
    // constructor

    /**
     * Construct an instance.
     *
     * @param type non-null; the type being defined
     * @param extendsType null-ok; the superclass of this type
     * @param fieldList null-ok; the list of defined fields
     */
    public DefineObject (TypeSpec type, TypeSpec extendsType,
			 ArgumentSpec[] fieldList,
			 boolean structConstructorFlag,
			 String comment)
    {
	if (type == null)
	{
	    throw new NullPointerException ("type == null");
	}

	myType = type;
	myExtends = extendsType;
	myStructConstructorFlag = structConstructorFlag;
	myComment = comment;
	
	if (fieldList == null)
	{
	    myFieldList = new ArgumentSpec[0];
	}
	else
	{
	    // can't trust the caller to drop its reference; too bad
	    // Java doesn't have declared const arrays
	    myFieldList = (ArgumentSpec[]) fieldList.clone ();
	}
    }



    // ------------------------------------------------------------------------
    // public instance methods

    /**
     * Return the string form for this instance.
     *
     * @return the string form
     */
    public String toString ()
    {
	StringBuffer result = new StringBuffer ();

	result.append ("(define-object ");
	result.append (myType);
	result.append (" (");

	if (myExtends != null)
	{
	    result.append (myExtends);
	}

	result.append (") ");
	result.append (myFieldList);
	result.append (")");

	return result.toString ();
    }

    /**
     * Get the type being defined by this instance.
     *
     * @return non-null; the type
     */
    public TypeSpec getType ()
    {
	return myType;
    }

    /**
     * Get the superclass type.
     *
     * @return null-ok; the superclass type
     */
    public TypeSpec getExtends ()
    {
	return myExtends;
    }

    /**
     * Get the field list.
     *
     * @return non-null; the field list
     */
    public ArgumentSpec[] getFieldList ()
    {
	return myFieldList;
    }

    /** Get the structor constructor value.
     *
     * @return true if option set.
     */
    public boolean getStructConstructorFlag() {
	return myStructConstructorFlag;
    }

    /** 
     * Get the comment value.
     *
     * @return non-null; the comment
     */
    public String getComment() {
	return myComment;
    }
}
