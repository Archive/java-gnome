/**
 * Specification of a single Java type. This is just a wrapper for
 * a string containing the name of the type in question.
 *
 * <p>The Java-Gnome bindings library is free software distributed under the
 * terms of the GNU Library General Public License version 2.</p>
 *
 * @author Dan Bornstein, danfuzz@milk.com
 * @author Copyright 2000 Dan Bornstein, all rights reserved. 
 */
public class JavaTypeSpec implements TypeSpecIface {
	/** non-null; the type name */
	protected final String myType;

	// ------------------------------------------------------------------------
	// constructor

	/**
	 * Construct an instance.
	 *
	 * @param type non-null; the Java type name
	 */
	public JavaTypeSpec(String type) {
		if (type == null) {
			throw new NullPointerException("type == null");
		}

		myType = type;
	}

	// ------------------------------------------------------------------------
	// public instance methods

	/**
	 * Return the string form for this instance.
	 *
	 * @return the string form
	 */
	public String toString() {
		return myType;
	}

	/**
	 * Get the hash code for this instance.
	 *
	 * @return the hash code
	 */
	public int hashCode() {
		return myType.hashCode();
	}

	/**
	 * Compare this to another instance. This returns <code>true</code>
	 * only if the converters are identical and the type names are
	 * equal.
	 *
	 * @param other the object to compare to
	 * @return the result of comparison
	 */
	public boolean equals(Object other) {
		if (!(other instanceof JavaTypeSpec)) {
			return false;
		}

		JavaTypeSpec otherType = (JavaTypeSpec) other;
		return myType.equals(otherType.myType);
	}

	// interface's javadoc suffices
	public String schemeType() {
		throw new RuntimeException("not implemented for this class");
	}

	// interface's javadoc suffices
	public String publicJavaType() {
		// FIXME: we don't actually do any analysis of the type right now
		return myType;
	}

	// interface's javadoc suffices
	public String publicJavaPackage() {
		// FIXME: need to revisit this once this class is more fleshed out
		int dotAt = myType.lastIndexOf('.');
		if (dotAt == -1) {
			// it's in the unnamed package
			return "";
		} else {
			return myType.substring(0, dotAt);
		}
	}

	// interface's javadoc suffices
	public String simpleJavaType() {
		// FIXME: need to revisit this at some point
		int dotAt = myType.lastIndexOf('.');
		if (dotAt == -1) {
			// it's in the unnamed package
			return myType;
		} else {
			return myType.substring(dotAt + 1);
		}
	}

	// interface's javadoc suffices
	public String publicJavaSignature() {
		// FIXME: need to revisit this at some point
		return "L" + myType.replace('.', '/') + ";";
	}

	// interface's javadoc suffices
	public String publicJavaMethodName(String schemeName) {
		// FIXME: need to revisit this at some point

		// what this does: looks for the last component of the package
		// name (e.g., "gtk"), and, if the given name matches with an
		// underscore after it, it removes that prefix
		String pkg = publicJavaPackage();
		int dotAt = pkg.lastIndexOf('.');
		if (dotAt != -1) {
			pkg = pkg.substring(dotAt + 1);
		}
		pkg += '_';
		if (schemeName.startsWith(pkg)) {
			schemeName = schemeName.substring(pkg.length());
		}

		// and studlyCap the result
		return TypeConverter.toStudlyCaps(schemeName);
	}

	// interface's javadoc suffices
	public String publicJavaConstantName(String schemeName) {
		throw new RuntimeException("not implemented for this class");
	}

	// interface's javadoc suffices
	public String nativeJavaType() {
		// FIXME: we don't actually do any analysis of the type right now
		return myType;
	}

	// interface's javadoc suffices
	public String nativeJavaSignature() {
		throw new RuntimeException("not implemented for this class");
	}

	// interface's javadoc suffices
	public String nativeType() {
		throw new RuntimeException("not implemented for this class");
	}

	// interface's javadoc suffices
	public boolean requiresNativeToPublicJavaReturn() {
		// FIXME: we don't actually do any analysis of the type right now
		return false;
	}

	// interface's javadoc suffices
	public String nativeToPublicJavaReturn(String call) {
		String result = call + ";";

		if (!myType.equals("void")) {
			result = "return " + result;
		}

		return result;
	}

	// interface's javadoc suffices
	public String glibToNativeReturn(String call, String cleanup) {
		throw new RuntimeException("not implemented for this class");
	}

	// interface's javadoc suffices
	public String glibType() {
		throw new RuntimeException("not implemented for this class");
	}
}
