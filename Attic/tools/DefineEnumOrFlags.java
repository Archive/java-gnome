/**
 * Representation of a Scheme enum or flags declaration, which will
 * eventually get translated into a {@link ClassSpec}. It consists of a
 * type (the type being defined), a field indicating what variant is being
 * defined (<code>define-enum</code>, <code>define-flags</code>, or
 * <code>define-string-enum</code>), and a list of {@link NamedConstantDef}
 * objects containing the declared values for the type.
 *
 * <p>The Java-Gnome bindings library is free software distributed under the
 * terms of the GNU Library General Public License version 2.</p>
 *
 * @author Dan Bornstein, danfuzz@milk.com
 * @author Copyright 2000 Dan Bornstein, all rights reserved. 
 */
final public class DefineEnumOrFlags {
	/** constant for <code>define-enum</code> */
	static final public int ENUM = 1;

	/** constant for <code>define-flags</code> */
	static final public int FLAGS = 2;

	/** constant for <code>define-string-enum</code> */
	static final public int STRING_ENUM = 3;

	/** non-null; the type being defined */
	private TypeSpec myType;

	/** the variant of definition, one of the statically-defined constants */
	private int myVariant;

	/** non-null; the list of declared values */
	private NamedConstantDef[] myValueList;

	// ------------------------------------------------------------------------
	// constructor

	/**
	 * Construct an instance.
	 *
	 * @param type non-null; the type being defined
	 * @param variant the variant, one of the three statically-defined
	 * constants
	 * <code>false</code> if it is an enumerated type
	 * @param valueList non-null; the list of declared values for this
	 * instance
	 */
	public DefineEnumOrFlags(
		TypeSpec type,
		int variant,
		NamedConstantDef[] valueList) {
		if (type == null) {
			throw new NullPointerException("type == null");
		}

		if ((variant < ENUM) || (variant > STRING_ENUM)) {
			throw new IllegalArgumentException("variant == " + variant);
		}

		if (valueList == null) {
			throw new NullPointerException("valueList == null");
		}

		myType = type;
		myVariant = variant;
		myValueList = valueList;
	}

	// ------------------------------------------------------------------------
	// public instance methods

	/**
	 * Return the string form for this instance.
	 *
	 * @return the string form
	 */
	public String toString() {
		StringBuffer result = new StringBuffer();

		result.append("(");

		switch (myVariant) {
			case ENUM :
				result.append("define-enum");
				break;
			case FLAGS :
				result.append("define-flags");
				break;
			case STRING_ENUM :
				result.append("define-string-enum");
				break;
		}

		result.append(" ");
		result.append(myType);

		for (int i = 0; i < myValueList.length; i++) {
			result.append(" ");
			result.append(myValueList[i]);
		}

		result.append(")");
		return result.toString();
	}

	/**
	 * Get the type being defined by this instance.
	 *
	 * @return non-null; the type
	 */
	public TypeSpec getType() {
		return myType;
	}

	/**
	 * Return the variant of this definition.
	 *
	 * @return the variant
	 */
	public int getVariant() {
		return myVariant;
	}

	/**
	 * Get the declared value list.
	 *
	 * @return the declared value list
	 */
	public NamedConstantDef[] getValueList() {
		return myValueList;
	}
}
