/**
 * Simple indenter for Java and C code.
 *
 * <p>The Java-Gnome bindings library is free software distributed under the
 * terms of the GNU Library General Public License version 2.</p>
 *
 * @author Dan Bornstein, danfuzz@milk.com
 * @author Copyright 2000 Dan Bornstein, all rights reserved. 
 */
public final class CodeCleanser
{
    /** the number of columns to indent per indentation level */
    static private final int INDENT_AMOUNT = 4;

    /** the intended maximum width of a line */
    static private final int MAX_COLUMNS = 100;

    /** the original code, as an array of characters */
    private char[] myCode;

    /** the current indentation level */
    private int myIndent;

    /** the current output column */
    private int myColumn;

    /** <code>true</code> if a newline was just issued */
    private boolean myNewline;

    /** <code>true</code> if a space was just issued */
    private boolean myJustSpaced;

    /** the current index into the code */
    private int myAt;

    /** the current result buffer */
    private StringBuffer myResult;



    // ------------------------------------------------------------------------
    // constructor

    /**
     * This class is not publicly instantiable. It is used internally in
     * the static methods.
     *
     * @param code the original code
     * @param indent the initial indentation level
     */
    private CodeCleanser (String code, int indent)
    {
	myCode = code.toCharArray ();
	myIndent = indent;

	myColumn = 0;
	myNewline = true;
	myJustSpaced = false;
	myAt = 0;
	myResult = new StringBuffer ();
    }



    // ------------------------------------------------------------------------
    // static public methods
    
    /**
     * Indent the given piece of code to the given indentation
     * level.
     *
     * @param code the code to indent
     * @param indent the number of columns to indent 
     */
    static public String indent (String code, int indent)
    {
	CodeCleanser cc = new CodeCleanser (code, indent);
	cc.doit ();
	return cc.myResult.toString ();
    }



    // ------------------------------------------------------------------------
    // private instance methods

    /**
     * Process the code. This is the main loop for this class.
     */
    private void doit ()
    {
	// iterate over all of the characters
	while (myAt < myCode.length)
	{
	    char c = peek ();

	    switch (c)
	    {
		case ' ':
		case '\t':
		case '\n':
		case '\r':
		{
		    // all spaces are equal; if at a newline or just after
		    // another space, ignore it; if past the max width,
		    // append a newline; otherwise, append a single space
		    if (myColumn >= MAX_COLUMNS)
		    {
			output ('\n');
		    }
		    else if (! (myNewline || myJustSpaced))
		    {
			output (' ');
		    }
		    consume ();
		    break;
		}
		case '{':
		{
		    // open braces are always on their own line, and
		    // they increase the indentation level
		    if (! myNewline)
		    {
			output ('\n');
		    }
		    outputPrefix ();
		    output ('{');
		    output ('\n');
		    myIndent++;
		    consume ();
		    break;
		}
		case '}':
		{
		    // close braces are always on their own line, and
		    // back one indentation level, and they decrease
		    // the indentation level
		    myIndent--;
		    if (! myNewline)
		    {
			output ('\n');
		    }
		    outputPrefix ();
		    output ('}');
		    output ('\n');
		    if (myIndent < 2)
		    {
			// ...and at the first two indentation levels,
			// they are separated by a blank line
			output ('\n');
		    }
		    consume ();
		    break;
		}
		case ';':
		{
		    // semicolons always end lines
		    output (';');
		    output ('\n');
		    if (myIndent < 2)
		    {
			// ...and at the first two indentation levels,
			// they are separated by a blank line
			//output ('\n');
		    }
		    consume ();
		    break;
		}
		case '\"':
		case '\'':
		{
		    // handle strings/character literals specially
		    handleString ();
		    break;
		}
		case '(':
		case '[':
		{
		    // these get output as-is, but cause subsequent lines
		    // to indent more
		    if (myNewline)
		    {
			outputPrefix ();
		    }
		    output (c);
		    consume ();
		    myIndent++;
		    break;
		}
		case ']':
		case ')':
		{
		    // these get output as-is, but cause subsequent lines
		    // to indent less
		    if (myNewline)
		    {
			outputPrefix ();
		    }
		    output (c);
		    consume ();
		    myIndent--;
		    break;
		}
		case '#':
		{
		    // '#' at the beginning of the line is special;
		    // anywhere else it is treated literally
		    if (myNewline)
		    {
			handlePound ();
		    }
		    else
		    {
			output (c);
			consume ();
		    }
		    break;
		}
		case '/':
		{
		    c = peek (1);
		    if ((c == '*') || (c == '/'))
		    {
			// handle comments specially
			handleComment ();
			break;
		    }
		    // if not a comment, fall through to the default
		}
		default:
		{
		    // anything else is a "word" to output; handle it
		    // specially
		    handleWord ();
		    break;
		}
	    }
	}
    }

    /**
     * Output a string or character literal.
     */
    private void handleString ()
    {
	char delim = peek ();

	// find the end of the string
	int endAt = 1;
	boolean hasNewlines = false;
	for (;;)
	{
	    if (endAt >= myCode.length)
	    {
		break;
	    }

	    char c = peek (endAt);
	    if (c == delim)
	    {
		endAt++;
		break;
	    }
	    else if (c == '\\')
	    {
		endAt++;
		if (peek (endAt) == '\n')
		{
		    hasNewlines = true;
		}
	    }
	    endAt++;
	}

	// output!
	outputConsume (endAt);
    }

    /**
     * Output a word.
     */
    private void handleWord ()
    {
	// find the end of the word
	int endAt = 1;
	for (;;)
	{
	    if (endAt >= myCode.length)
	    {
		break;
	    }

	    char c = peek (endAt);
	    if ((c    == ' ')
		|| (c == '\t')
		|| (c == '\r')
		|| (c == '\n')
		|| (c == '\'')
		|| (c == '\"')
		|| (c == '(')
		|| (c == ')')
		|| (c == '[')
		|| (c == ']')
		|| (c == '{')
		|| (c == '}')
		|| (c == ';'))
	    {
		break;
	    }
	    endAt++;
	}

	// output!
	outputConsume (endAt);
    }

    /**
     * Output a comment.
     */
    private void handleComment ()
    {
	if (peek (1) == '/')
	{
	    // C++ style; output till a newline
	    if (myNewline)
	    {
		outputPrefix ();
	    }

	    for (;;)
	    {
		char c = peek ();
		outputConsume (1);
		if ((c == '\n') || (c == '\r'))
		{
		    break;
		}
	    }
	}
	else
	{
	    // C style; output till a close marker
	    if (myNewline)
	    {
		outputPrefix ();
	    }

	    boolean lineStart = false;

	    for (;;)
	    {
		if (myNewline)
		{
		    outputPrefix ();
		    output (' '); // align with the star
		    lineStart = true;
		}
		
		char c = peek ();

		if ((c == '*') && (peek (1) == '/'))
		{
		    outputConsume (2);
		    if (peek () == '\n')
		    {
			// let end-of-line after comment happen,
			// no matter what
			outputConsume (1);
		    }
		    break;
		}

		if (lineStart && ((c == ' ') || (c == '\t')))
		{
		    // ignore spaces at the start of a line
		    consume ();
		}
		else
		{
		    outputConsume (1);
		    lineStart = false;
		}
	    }
	}
    }

    /**
     * Output a pound (<code>"#"</code>) directive.
     */
    private void handlePound ()
    {
	for (;;)
	{
	    char c = peek ();
	    output (consume ());
	    if ((c == '\n') || (c == '\r'))
	    {
		break;
	    }
	    if (c == '\\')
	    {
		c = peek ();
		if ((c == '\n') || (c == '\r'))
		{
		    // it's got a continuation
		    output (consume ());
		}
	    }
	}
    }

    /**
     * Consume the given number of characters of input, passing them
     * directly to the output. This will start a new line if deemed
     * appropriate
     *
     * @param count the number of characters to consume and output
     */
    private void outputConsume (int count)
    {
	// if not at a newline, and it won't fit on the current line,
	// output a newline
	if ((! myNewline)
	    && (myColumn + count) >= MAX_COLUMNS)
	{
	    output ('\n');
	}

	// if at a newline, output the prefix
	if (myNewline)
	{
	    outputPrefix ();
	}

	// output!
	while (count > 0)
	{
	    output (consume ());
	    count--;
	}

	// if past the max column, then output a newline now
	if (myColumn >= MAX_COLUMNS)
	{
	    output ('\n');
	}
    }

    /**
     * Peek at the next character without consuming it.
     *
     * @return the next character
     */
    private char peek ()
    {
	return myCode[myAt];
    }

    /**
     * Peek the given number of characters ahead.
     *
     * @param hawFar how far to look ahead
     * @return the given character
     */
    private char peek (int howFar)
    {
	return myCode[myAt + howFar];
    }

    /**
     * Return <code>true</code> if the given string is what would
     * be consumed next.
     *
     * @param str the string to look for
     * @return <code>true</code> if the given string is what would
     * be consumed next
     */
    private boolean peekEquals (String str)
    {
	int len = str.length ();
	if ((myAt + len) > myCode.length)
	{
	    // not enough characters in the code left to match
	    return false;
	}

	for (int i = 0; i < len; i++)
	{
	    if (peek (i) != str.charAt (i))
	    {
		return false;
	    }
	}

	return true;
    }

    /**
     * Consume the next character
     *
     * @return the next character
     */
    private char consume ()
    {
	myAt++;
	return myCode[myAt - 1];
    }

    /**
     * Output the given character.
     * 
     * @param c the character to output
     */
    private void output (char c)
    {
	myResult.append (c);
	if ((c == '\n') || (c == '\r'))
	{
	    myNewline = true;
	    myJustSpaced = false;
	    myColumn = 0;
	}
	else if (c == '\t')
	{
	    myNewline = false;
	    myJustSpaced = true;
	    myColumn = (myColumn % 8) + 8;
	}
	else if (c == ' ')
	{
	    myNewline = false;
	    myJustSpaced = true;
	    myColumn++;
	}
	else if (c > ' ')
	{
	    myNewline = false;
	    myJustSpaced = false;
	    myColumn++;
	}
    }

    /**
     * Output the indentation prefix.
     */
    private void outputPrefix ()
    {
	for (int i = 0; i < myIndent * INDENT_AMOUNT; i++)
	{
	    output (' ');
	}

	// even if nothing was output, we are now effectively past the
	// start of the line (this affects how C-style comments indent)
	myNewline = false;
    }
}
