import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Class that does all of the file emitting. It knows how to deal
 * with the basic configuration and factors out some common code snippets.
 * In particular, the methods in this class always emit the standard
 * header info to the start of every file.
 *
 * <p>The Java-Gnome bindings library is free software distributed under the
 * terms of the GNU Library General Public License version 2.</p>
 *
 * @author Dan Bornstein, danfuzz@milk.com
 * @author Copyright 2000 Dan Bornstein, all rights reserved.
 */
final public class FileEmitter {
	/** how to format dates; used by {@link #emitStandardHeader} */
	static private final SimpleDateFormat theDateFormat =
		new SimpleDateFormat("yyyy-MM-dd HH:mm:ss zzz");

	/** the base directory for Java source */
	private String myJavaDirectory;

	/** the directory for C source */
	private String myCSourceDirectory;

	/** the directory for C headers */
	private String myCHeaderDirectory;

	// ------------------------------------------------------------------------
	// constructor

	/**
	 * Construct an instance. If any of the directories is passed
	 * as <code>null</code>, then it will be impossible to open a
	 * file of the associated type.
	 *
	 * @param javaDirectory null-ok; the base directory for Java source
	 * @param cSourceDirectory null-ok; the directory for C source
	 * @param cHeaderDirectory null-ok; the directory for C headers
	 */
	public FileEmitter(
		String javaDirectory,
		String cSourceDirectory,
		String cHeaderDirectory) {
		// make sure that all of the directories end with the
		// separator character
		String sep = File.separator;

		if (!javaDirectory.endsWith(sep)) {
			javaDirectory += sep;
		}

		if (!cSourceDirectory.endsWith(sep)) {
			cSourceDirectory += sep;
		}

		if (!cHeaderDirectory.endsWith(sep)) {
			cHeaderDirectory += sep;
		}

		// set the instance variables
		myJavaDirectory = javaDirectory;
		myCSourceDirectory = cSourceDirectory;
		myCHeaderDirectory = cHeaderDirectory;
	}

	// ------------------------------------------------------------------------
	// public instance methods

	/**
	 * Emit a Java source file for the class associated with the given type.
	 *
	 * @param type non-null; the type representing the class to emit for
	 * @param code non-null; the code to emit
	 */
	public void emitJavaSource(TypeSpecIface type, String code)
		throws IOException {
		if (code == null) {
			throw new NullPointerException("code == null");
		}

		// convert the slashes in the class name to the appropriate
		// separator character (this is a no-op on Unix), and append
		// ".java"
		String className = typeToRawPath(type);
			int index = className.lastIndexOf("/");
		String simple = className.substring(index+1);
		String rawName = className.substring(0, index);

		simple = GlibTranslator.javaGnomeName(simple);

		rawName += '/' + simple;
		className = rawName.replace('/', File.separatorChar);
		className += ".java";

		FileWriter fw = makeFileWriter(myJavaDirectory + className);
		emitStandardHeader(fw);
		fw.write(code);
		fw.close();
	}

	/**
	 * Emit a C source file for the class associated with the given type.
	 *
	 * @param type non-null; the type representing the class to emit for
	 * @param code non-null; the code to emit
	 */
	public void emitCSource(TypeSpecIface type, String code)
		throws IOException {
		if (code == null) {
			throw new NullPointerException("code == null");
		}

		// convert the slashes in the class name to underscores, and append
		// ".c"
		String className = typeToRawPath(type);
		int index = className.lastIndexOf("/");
		String simple = className.substring(index+1);
		String rawName = className.substring(0, index);

		simple = GlibTranslator.javaGnomeName(simple);

		rawName += '/' + simple;
		
		className = rawName.replace('/', '_');
		className += ".c";

		FileWriter fw = makeFileWriter(myCSourceDirectory + className);
		emitStandardHeader(fw);
		fw.write(code);
		fw.close();
	}

	/**
	 * Convert the given type to a raw path to the Java class. This is the
	 * fully-qualified class name, except with slashes (<code>"/"</code>)
	 * instead of dots (<code>"."</code>) between components.
	 *
	 * @param type non-null; the type to convert
	 * @return the raw path
	 */
	static private String typeToRawPath(TypeSpecIface type) {
		if (type == null) {
			throw new NullPointerException("type == null");
		}

		String className = type.publicJavaSignature();
		if (!(className.startsWith("L") && className.endsWith(";"))) {
			throw new IllegalArgumentException(
				"type (" + type + ") does not have a public Java class");
		}

		// strip off the "L" and ";" around the raw class path
		return className.substring(1, className.length() - 1);
	}

	/**
	 * Open the named file for writing, returning a
	 * <code>FileWriter</code>. Before doing so, create the directory for
	 * the file if needed, and verify that the directory is writable.
	 *
	 * @param fileName the name of the file to open 
	 */
	static private FileWriter makeFileWriter(String fileName)
		throws IOException {
		File f = new File(fileName);
		File dir = new File(f.getParent());

		// boy, it'd be nice if File threw exceptions just like the
		// rest of the core Java libraries; c'est la guerre
		if (dir.exists()) {
			if (!dir.isDirectory()) {
				throw new IOException("error: not a directory: " + dir);
			}
		} else {
			if (!dir.mkdirs()) {
				throw new IOException(
					"error: could not create directory: " + dir);
			}
		}

		if (!dir.canWrite()) {
			throw new IOException("error: can't write to directory: " + dir);
		}

		//return new FileWriter (f);
		return new ComparingFileWriter(f);
	}

	/**
	 * Emit the standard header to the given <code>Writer</code>.
	 *
	 * @param writer where to write to
	 */
	static private void emitStandardHeader(Writer writer) throws IOException {
		StringBuffer sb = new StringBuffer(1000);
		sb.append(
			"/*\n"
				+ " * Java-Gnome Bindings Library\n"
				+ " *\n"
				+ " * Copyright 1998-2002 the Java-Gnome Team, "
				+ "all rights reserved.\n"
				+ " *\n"
				+ " * The Java-Gnome Team Members:\n"
				+ " *   Jean Van Wyk <jeanvanwyk@iname.com>\n"
				+ " *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>\n"
				+ " *   Dan Bornstein <danfuzz@milk.com>\n"
				+ " *\n"
				+ " * The Java-Gnome bindings library is free software distributed under\n"
				+ " * the terms of the GNU Library General Public License version 2.\n"
				+ " *\n"
				+ " * This file was orriginally generated by the Java-GNOME Code Generator\n"
				+ " * Please do not modify the code that is identified as generated.  Also,\n"
				+ " * please insert your code above the generated code.\n"
				+ " *\n");

		sb.append(" * Generation date: ");
		synchronized (theDateFormat) {
			// note: always synchronize static object method calls, unless
			// given a really good reason not to
			sb.append(theDateFormat.format(new Date()));
		}
		sb.append("\n");

		sb.append(" */\n\n");

		writer.write(sb.toString());
	}
}

/* Private class, we use this to wrap the written files so that we
 * will compare them to the previous (if any) same file, and
 * replace the old file with the new file only if they
 * differ.  This is very handy with the Makefile dependencies,
 * since we avoid recompiling *all* source and java files, and
 * instead only those which have really changed.
 */

class ComparingFileWriter extends FileWriter {
	private File myRealFile;

	ComparingFileWriter(File file) throws IOException {
		super(new File(file.getCanonicalPath() + ".tmp"));
		myRealFile = file;
	}

	public void close() throws IOException {
		super.close();

		File newFile = new File(myRealFile.getCanonicalPath() + ".tmp");
		if (myRealFile.exists()) {
			// Open both files for reading.
			LineNumberReader newReader =
				new LineNumberReader(new FileReader(newFile));
			LineNumberReader oldReader =
				new LineNumberReader(new FileReader(myRealFile));

			String newLine, oldLine;
			boolean equals = true;

			while (true) {
				newLine = newReader.readLine();
				oldLine = oldReader.readLine();

				if (newLine == null && oldLine == null)
					break;

				if (newLine == null || oldLine == null) {
					equals = false;
					break;
				}

				// Exception: check for the " * Generation date: "
				// line and skip it!
				if (newLine.startsWith(" * Generation date: ")
					&& oldLine.startsWith(" * Generation date: "))
					continue;

				if (!newLine.equals(oldLine)) {
					equals = false;
					break;
				}
			}

			if (equals) {
				newFile.delete();
				return;
			}
		}

		// Not equal, rename the new over the old.
		newFile.renameTo(myRealFile);
	}
}
