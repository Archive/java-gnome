/**
 * Representation of a Java method (that is, non-native) on a class. In
 * addition to the standard Java member properties, it has
 * an argument list and a Java method body.
 *
 * <p>The Java-Gnome bindings library is free software distributed under the
 * terms of the GNU Library General Public License version 2.</p>
 *
 * @author Dan Bornstein, danfuzz@milk.com
 * @author Copyright 2000 Dan Bornstein, all rights reserved. 
 */
final public class JavaMethodSpec
    extends JavaMemberSpec
{
    /** non-null; the list of arguments to the method */
    private ArgumentList myArgumentList;

    /** non-null; the Java code for the method body */
    private String myBody;

    /** non-null; the comment associated with the method */
    private String myComment;

    // ------------------------------------------------------------------------
    // constructor

    public JavaMethodSpec (String name, TypeSpecIface returnType, 
			   boolean isStatic, boolean isFinal, int scope,
			   ArgumentList argumentList, String body)
    {
	this (name, returnType, isStatic, isFinal, scope,
	      argumentList, body, null);
    }

    /**
     * Construct an instance.
     *
     * @param name non-null; the name of the method
     * @param returnType non-null; the return type of the method
     * @param isStatic <code>true</code> if this is to be a
     * <code>static</code> method, <code>false</code> if this is to be an
     * instance method
     * @param isFinal <code>true</code> if this is to be a
     * <code>final</code> method, <code>false</code> if not
     * @param scope the scope of the instance, represented by one of
     * these constants: {@link #SCOPE_PUBLIC}, {@link #SCOPE_PROTECTED},
     * {@link #SCOPE_PACKAGE}, or {@link #SCOPE_PRIVATE}
     * @param argumentList non-null; list of arguments to the method
     * @param body non-null; the Java code for the method body; it should not
     * have braces around its entirety (those are provided for you at
     * no additional cost)
     * @param comment non-null; the comment associated with the method.
     */
    public JavaMethodSpec (String name, TypeSpecIface returnType, 
			   boolean isStatic, boolean isFinal, int scope,
			   ArgumentList argumentList, String body,
			   String comment)
    {
	super (name, returnType, isStatic, isFinal, scope);

	if (argumentList == null)
	{
	    throw new NullPointerException ("argumentList == null");
	}

	if (body == null)
	{
	    throw new NullPointerException ("body == null");
	}

	myArgumentList = argumentList;
	myBody = body;
	myComment = comment;
    }


    // ------------------------------------------------------------------------
    // public instance methods

    // interface's javadoc suffices
    public String javaSource ()
    {
	StringBuffer sb = new StringBuffer ();

	// Insert the method comment here
	if (myComment != null) {
	    sb.append("/**\n");
	    String output = "*";
	    java.util.StringTokenizer st = 
		new java.util.StringTokenizer(myComment);

	    while (st.hasMoreElements()) {
		String tmp = st.nextToken();
		if (output.length() + tmp.length() < 70) {
		    output += " " + tmp;
		}
		else {
		    sb.append(output + "\n");
		    output = "* " + tmp;
		}
	    }
	    sb.append(output + "\n*/\n\n");
	}

	if (getName ().equals ("<clinit>"))
	{
	    // it's a static initialization section
	    sb.append ("static");
	}
	else
	{
	    sb.append (getDeclaration ());
	    sb.append (" (");
	    sb.append (myArgumentList.publicJavaArguments ());
	    sb.append (")\n");
	}

	sb.append ("{\n");
	sb.append (myBody);
	sb.append ("}\n");

	return sb.toString ();
    }

    // interface's javadoc suffices
    public String cSource ()
    {
	// there is no C source associated with this
	return "";
    }

    // interface's javadoc suffices
    public String cHeader ()
    {
	// there is no C header associated with this
	return "";
    }
}
