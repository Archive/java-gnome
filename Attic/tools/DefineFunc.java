/**
 * Representation of a Scheme function declaration, which will eventually
 * get translated into a (partial) {@link ClassSpec}. It consists of a
 * Scheme name, return type, and an argument list.
 *
 * <p>The Java-Gnome bindings library is free software distributed under the
 * terms of the GNU Library General Public License version 2.</p>
 *
 * @author Dan Bornstein, danfuzz@milk.com
 * @author Copyright 2000 Dan Bornstein, all rights reserved. 
 */
final public class DefineFunc
{
    /** the Scheme name */
    private String mySchemeName;

    /** the return type */
    private TypeSpec myReturnType;

    /** the argument list */
    private ArgumentList myArgumentList;

    /** the comment associated with this function */
    private String myComment;


    // ------------------------------------------------------------------------
    // constructor

    /**
     * Construct an instance.
     *
     * @param schemeName non-null; the Scheme name
     * @param returnType non-null; the return type
     * @param argumentList non-null; the argument list
     */
    public DefineFunc (String schemeName, TypeSpec returnType,
		       ArgumentList argumentList)
    {
	this(schemeName, returnType, argumentList, null);
    }


    /**
     * Construct an instance.
     *
     * @param schemeName non-null; the Scheme name
     * @param returnType non-null; the return type
     * @param argumentList non-null; the argument list
     * @param comment non-null; the comment
     */
    public DefineFunc (String schemeName, TypeSpec returnType,
		       ArgumentList argumentList, String comment)
    {
	if (schemeName == null)
	{
	    throw new NullPointerException ("schemeName == null");
	}

	if (returnType == null)
	{
	    throw new NullPointerException ("returnType == null");
	}

	if (argumentList == null)
	{
	    throw new NullPointerException ("argumentList == null");
	}

	mySchemeName = schemeName;
	myReturnType = returnType;
	myArgumentList = argumentList;
	myComment = comment;
    }

    // ------------------------------------------------------------------------
    // public instance methods

    /**
     * Return the string form for this instance.
     *
     * @return the string form
     */
    public String toString ()
    {
	return "(define-func " + mySchemeName + " " + myReturnType + " " + 
	    myArgumentList + ")";
    }

    /**
     * Return a new instance, just like this one, except its argument list
     * has been stripped of any default arguments.
     *
     * @return the new instance 
     */
    public DefineFunc withoutDefaults ()
    {
	return new DefineFunc (mySchemeName, 
			       myReturnType, 
			       myArgumentList.withoutDefaults (),
			       myComment);
    }

    /**
     * Return a new instance, just like this one, except its return type
     * is changed to the one given
     *
     * @param returnType the new return type
     * @return the new instance 
     */
    public DefineFunc withReturnType (TypeSpec returnType)
    {
	return new DefineFunc (mySchemeName, returnType, 
			       myArgumentList, myComment);
    }

    /**
     * Get the Scheme name.
     *
     * @return the Scheme name
     */
    public String getSchemeName ()
    {
	return mySchemeName;
    }

    /**
     * Get the return type.
     *
     * @return the return type
     */
    public TypeSpec getReturnType ()
    {
	return myReturnType;
    }

    /**
     * Get the argument list.
     *
     * @return the argument list
     */
    public ArgumentList getArgumentList ()
    {
	return myArgumentList;
    }

    /**
     * Get the comment.
     *
     * @return the comment
     */
    public String getComment() {
	return myComment;
    }
}
