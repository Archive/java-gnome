import java.io.EOFException;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PushbackReader;
import java.util.Hashtable;
import java.util.Vector;

/**
 * Reader/parser of the Scheme <code>.defs</code> file format. It collects
 * lists of definitions of the various things found in the files it is
 * directed to read.
 *
 * <p>The Java-Gnome bindings library is free software distributed under the
 * terms of the GNU Library General Public License version 2.</p>
 *
 * @author Dan Bornstein, danfuzz@milk.com
 * @author Copyright 2000 Dan Bornstein, all rights reserved. 
 */
final public class DefsReader
{
    /** whether or not to print debugging output */
    private static final boolean DEBUG = false;
     
    /** non-null; the {@link TypeConverter} to use */
    private final TypeConverter myTypeConverter;

    /** list of function definitions */
    private Vector myDefineFuncs;

    /** list of enum type definitions */
    private Vector myDefineEnums;

    /** list of string enum type definitions */
    private Vector myDefineStringEnums;

    /** list of flag type definitions */
    private Vector myDefineFlags;

    /** list of boxed definitions */
    private Vector myDefineBoxeds;

    /** list of object definitions */
    private Vector myDefineObjects;



    // ------------------------------------------------------------------------
    // constructor

    /**
     * Construct an instance. It initially contains no definitions.
     *
     * @param typeConverter non-null; the {@link TypeConverter} to use when
     * constructing instances 
     */
    public DefsReader (TypeConverter typeConverter)
    {
	if (typeConverter == null)
	{
	    throw new NullPointerException ("typeConverter == null");
	}

	myTypeConverter = typeConverter;

	myDefineFuncs = new Vector ();
	myDefineEnums = new Vector ();
	myDefineStringEnums = new Vector ();
	myDefineFlags = new Vector ();
	myDefineBoxeds = new Vector ();
	myDefineObjects = new Vector ();
    }



    // ------------------------------------------------------------------------
    // public instance methods

    /**
     * Return an array of all of the parsed {@link DefineFunc} objects.
     *
     * @return the array
     */
    public DefineFunc[] getDefineFuncs ()
    {
	DefineFunc[] result = new DefineFunc[myDefineFuncs.size ()];
	myDefineFuncs.copyInto (result);
	return result;
    }

    /**
     * Return an array of all of the parsed {@link DefineEnumOrFlags} objects
     * corresponding to enumerated types.
     *
     * @return the array
     */
    public DefineEnumOrFlags[] getDefineEnums ()
    {
	DefineEnumOrFlags[] result = 
	    new DefineEnumOrFlags[myDefineEnums.size ()];
	myDefineEnums.copyInto (result);
	return result;
    }

    /**
     * Return an array of all of the parsed {@link DefineEnumOrFlags} objects
     * corresponding to enumerated string types.
     *
     * @return the array
     */
    public DefineEnumOrFlags[] getDefineStringEnums ()
    {
	DefineEnumOrFlags[] result = 
	    new DefineEnumOrFlags[myDefineStringEnums.size ()];
	myDefineStringEnums.copyInto (result);
	return result;
    }

    /**
     * Return an array of all of the parsed {@link DefineEnumOrFlags} objects
     * corresponding to flags types.
     *
     * @return the array
     */
    public DefineEnumOrFlags[] getDefineFlags ()
    {
	DefineEnumOrFlags[] result = 
	    new DefineEnumOrFlags[myDefineFlags.size ()];
	myDefineFlags.copyInto (result);
	return result;
    }

    /**
     * Return an array of all of the parsed {@link DefineBoxed} objects.
     *
     * @return the array
     */
    public DefineBoxed[] getDefineBoxeds ()
    {
	DefineBoxed[] result = new DefineBoxed[myDefineBoxeds.size ()];
	myDefineBoxeds.copyInto (result);
	return result;
    }

    /**
     * Return an array of all of the parsed {@link DefineObject} objects.
     *
     * @return the array
     */
    public DefineObject[] getDefineObjects ()
    {
	DefineObject[] result = new DefineObject[myDefineObjects.size ()];
	myDefineObjects.copyInto (result);
	return result;
    }

    /**
     * Read the given file, and add appropriate definitions to this
     * instance. If there is an exceptions during processing, then a
     * message is issued, and processing continues; at the end of
     * processing, a single exception will be thrown if there were
     * any problems.
     *
     * @param fileName the name of the file to read.
     */
    public void readFile (String fileName)
    {
	int errorCount = 0;
	int warningCount = 0;

	System.err.println ("processing file: " + fileName);

	try
	{
	    PushbackReader reader = 
		new PushbackReader (new FileReader (fileName));

	    String lastStr0 = null;
	    String lastStr1 = null;
	    String lastStr2 = "<beginning-of-file>";

	    for (;;)
	    {
		Object sexp;
		String sexpStr = null;
		try
		{
		    sexp = readSexp (reader);
		    sexpStr = sexpToString (sexp);
		    if (sexpStr.length () > 60)
		    {
			sexpStr = sexpStr.substring (0, 60) + " ...";
		    }
		    lastStr0 = lastStr1;
		    lastStr1 = lastStr2;
		    lastStr2 = sexpStr;

		    if (sexp instanceof Object[])
		    {
			// handle import here
			Object[] list = (Object[]) sexp;
			if ((list.length == 2)
			    && list[0].equals ("import"))
			{
			    String dir = new File (fileName).getParent ();
			    readFile (dir + File.separator + list[1]);
			    continue;
			}
		    }

		    evalSexp (sexp);
		}
		catch (RuntimeException ex)
		{
		    String prefix;
		    String msg = ex.getMessage ();
		    if (msg == null)
		    {
			msg = "(no additional information provided)";
		    }

		    if (msg.startsWith ("warning: "))
		    {
			warningCount++;
			prefix = "warning: ";
		    }
		    else
		    {
			errorCount++;
			prefix = "error: ";
			if (! msg.startsWith ("error: "))
			{
			    msg = prefix + msg;
			    ex.printStackTrace ();
			}
		    }
			
		    System.err.println (msg);
		    if (sexpStr != null)
		    {
			System.err.println (prefix + "occurred while " +
					    "processing top-level form");
			System.err.println (prefix + "  " + sexpStr);
		    }
		    else
		    {
			System.err.println (prefix + "occurred while " +
					    "reading, after top-level forms");
			if (lastStr0 != null)
			{
			    System.err.println (prefix + "  " + lastStr0);
			}
			if (lastStr1 != null)
			{
			    System.err.println (prefix + "  " + lastStr1);
			}
			System.err.println (prefix + "  " + lastStr2);
		    }
		}
	    }
	}
	catch (IOException ex)
	{
	    // make sure it's expected; if so, ignore it; if not, rethrow
	    if (! ex.getMessage ().equals ("expected"))
	    {
		errorCount++;
		System.err.println ("Error while processing file (" +
				    fileName + ")");
		ex.printStackTrace ();
	    }
	}

	RuntimeException ex = null;
	if (warningCount != 0)
	{
	    String msg = "warning: Got " + warningCount + 
		" warning(s) while processing " + fileName + ".";
	    System.err.println (msg);
	    ex = new RuntimeException (msg);
	}

	if (errorCount != 0)
	{
	    String msg = 
		"Got " + errorCount + " error(s) while processing " +
		fileName + ".";
	    System.err.println ("error: " + msg);
	    ex = new RuntimeException (msg);
	}

	System.err.println ("done processing file: " + fileName);
	System.err.println ();

	if (ex != null)
	{
	    throw ex;
	}
    }



    // ------------------------------------------------------------------------
    // private instance methods

    /**
     * Evaluate the given sexp. It should be a list with one of these
     * as the first element:
     *
     * <ul>
     * <li><code>define-boxed</code></li>
     * <Li><code>define-enum</code></li>
     * <Li><code>define-string-enum</code></li>
     * <li><code>define-flags</code></li>
     * <li><code>define-func</code></li>
     * <li><code>define-object</code></li>
     * </ul>
     *
     * <p>Anything else is treated as an opportunity to issue a warning,
     * but is otherwise ignored. This will throw an exception in cases
     * where one of the above names is matched, but the rest of the
     * sexp is in a bad form.</p>
     *
     * @param sexp the sexp to evaluate
     */
    private void evalSexp (Object sexp)
    {
	if (! (sexp instanceof Object[]))
	{
	    throw new RuntimeException ("warning: ignoring " + sexp);
	}

	Object[] list = (Object[]) sexp;

	if (list.length == 0)
	{
	    throw new RuntimeException ("warning: ignoring ()");
	}

	if (! (list[0] instanceof String))
	{
	    throw new RuntimeException ("warning: ignoring " + 
					sexpToString (list));
	}

	String first = (String) list[0];

	if ("define-boxed".equals(first))
	{
	    evalDefineBoxed (list);
	}
	else if ("define-enum".equals(first))
	{
	    evalDefineEnum (list);
	}
	else if ("define-string-enum".equals(first))
	{
	    evalDefineStringEnum (list);
	}
	else if ("define-flags".equals(first))
	{
	    evalDefineFlags (list);
	}
	else if ("define-func".equals(first))
	{
	    evalDefineFunc (list);
	}
	else if ("define-object".equals(first))
	{
	    evalDefineObject (list);
	}
	else
	{
	    throw new RuntimeException ("warning: ignoring (" + 
					first + " ...)");
	}
    }

    /**
     * Evaluate a <code>define-boxed</code> list. The form syntax is:
     *
     * <blockquote><pre>(define-boxed <i>name</i>
     *   <i>boxed-spec</i>*)</pre></blockquote>
     *
     * where <code><i>boxed-spec</i></code> consists of these possible
     * forms:
     *
     * <ul>
     * <li><code>(copy <i>function-name</i>)</code></li>
     * <li><code>(free <i>function-name</i>)</code></li>
     * <li><code>(size <i>code</i>)</code></li>
     * <li><code>(conversion <i>function-name</i>)</code></li>
     * <li><code>(fields (<i>type name</i>)+)</code></li>
     * </ul>
     *
     * @param list the list to evaluate
     */
    private void evalDefineBoxed (Object[] list)
    {
	if ((list.length < 3)
	    || !(list[1] instanceof String)
	    || !(list[2] instanceof Object[]))
	{
	    throw new RuntimeException ("error: bad define-boxed form");
	}

	TypeSpec type = new TypeSpec (myTypeConverter, (String) list[1]);
	TypeSpec superclass;

	Hashtable specs = new Hashtable ();
	Object[] inheritList = (Object[])list[2];
	if (inheritList.length == 0) {
	    superclass = null;
	}
	else if ((inheritList.length == 1) 
		 && (inheritList[0] instanceof String)) {
	    superclass = new TypeSpec(myTypeConverter, (String)inheritList[0]);
	}
	else {
	    throw new RuntimeException("error: bad define-boxed form");
	}

	for (int i = 3; i < list.length; i++)
	{
	    if (! (list[i] instanceof Object[]))
	    {
		throw new RuntimeException ("error: bad define-boxed form");
	    }
	    Object[] one = (Object[]) list[i];
	    if ((one.length < 2)
		|| !(one[0] instanceof String))
	    {
		throw new RuntimeException ("error: bad define-boxed form");
	    }

	    String specName = ((String) one[0]).intern ();
	    Object value;
	    if (specName.equals("fields"))
	    {
		value = evalFields (one);
	    }
	    else
	    {
		if ((one.length != 2)
		    || !(one[1] instanceof String))
		{
		    throw new RuntimeException (
                        "error: bad define-boxed form");
		}
		
		if (!(specName.equals("copy"))
		    && !(specName.equals("free"))
		    && !(specName.equals("size"))
		    && !(specName.equals("conversion")))
		{
		    throw new RuntimeException (
                        "error: bad define-boxed form");
		}

		value = one[1];
	    }

	    if (specs.get (specName) != null)
	    {
		throw new RuntimeException ("error: bad define-boxed form");
	    }

	    specs.put (specName, value);
	}

	DefineBoxed def = new DefineBoxed (type, superclass, specs);
	myDefineBoxeds.addElement (def);

	if (DEBUG)
	{
	    System.err.println ("parsed " + def);
	}
    }

    /**
     * Evaluate a <code>define-enum</code> list. The form syntax is:
     *
     * <blockquote><pre>(define-enum <i>name</i>
     *   (<i>name GLIB_NAME</i> (= <i>value</i>)?)*)</pre></blockquote>
     *
     * @param list the list to evaluate
     */
    private void evalDefineEnum (Object[] list)
    {
	if ((list.length < 2)
	    || !(list[1] instanceof String))
	{
	    throw new RuntimeException ("error: bad define-enum form");
	}

	TypeSpec type = new TypeSpec (myTypeConverter, (String) list[1]);

	Object[] rawValues = new Object[list.length - 2];
	System.arraycopy (list, 2, rawValues, 0, rawValues.length);
	NamedConstantDef[] values = evalValueBindings (rawValues);

	DefineEnumOrFlags def = 
	    new DefineEnumOrFlags (type, 
				   DefineEnumOrFlags.ENUM, 
				   values);
	myDefineEnums.addElement (def);

	if (DEBUG)
	{
	    System.err.println ("parsed " + def);
	}
    }

    /**
     * Evaluate a <code>define-string-enum</code> list. The form syntax is:
     *
     * <blockquote><pre>(define-string-enum <i>name</i>
     *   ("<i>value</i>" <i>GLIB_NAME</i>)*)</pre></blockquote>
     *
     * @param list the list to evaluate 
     */
    private void evalDefineStringEnum (Object[] list)
    {
	if ((list.length < 2)
	    || !(list[1] instanceof String))
	{
	    throw new RuntimeException ("error: bad define-string-enum form");
	}

	TypeSpec type = new TypeSpec (myTypeConverter, (String) list[1]);

	Object[] rawValues = new Object[list.length - 2];
	System.arraycopy (list, 2, rawValues, 0, rawValues.length);
	NamedConstantDef[] values = evalStringBindings (rawValues);

	DefineEnumOrFlags def = 
	    new DefineEnumOrFlags (type, 
				   DefineEnumOrFlags.STRING_ENUM, 
				   values);

	myDefineStringEnums.addElement (def);

	if (DEBUG)
	{
	    System.err.println ("parsed " + def);
	}
    }

    /**
     * Evaluate a <code>define-flags</code> list. The form syntax is:
     *
     * <blockquote><pre>(define-flags <i>name</i>
     *   (<i>name GLIB_NAME</i> (= <i>value</i>)?)*)</pre></blockquote>
     *
     * @param list the list to evaluate
     */
    private void evalDefineFlags (Object[] list)
    {
	if ((list.length < 2)
	    || !(list[1] instanceof String))
	{
	    throw new RuntimeException ("error: bad define-flags form");
	}

	TypeSpec type = new TypeSpec (myTypeConverter, (String) list[1]);

	Object[] rawValues = new Object[list.length - 2];
	System.arraycopy (list, 2, rawValues, 0, rawValues.length);
	NamedConstantDef[] values = evalValueBindings (rawValues);

	DefineEnumOrFlags def = 
	    new DefineEnumOrFlags (type, 
				   DefineEnumOrFlags.FLAGS, 
				   values);
	myDefineFlags.addElement (def);

	if (DEBUG)
	{
	    System.err.println ("parsed " + def);
	}
    }

    /**
     * Evaluate a <code>define-func</code> list. The form syntax is:
     *
     * <blockquote><pre>(define-func <i>name</i>
     *              <i>return-type</i>
     *              (<i>arg-spec</i>*)
     *              (comment (<i>function-comment</i>))?</pre></blockquote>
     *
     * where <code><i>arg-spec</i></code> is of the form:
     *
     * <blockquote><pre>(<i>type</i> 
     *  <i>name</i>
     *  [ (null-ok) | (read-only) | (read-write) | (object)
     *     (= <i>default-value</i>) ]*)</pre></blockquote>
     *
     * @param list the list to evaluate
     */
    private void evalDefineFunc (Object[] list)
    {
	String warning = null;
	String comment = "";
	if ((list.length == 5)
	    && sexpToString (list[4]).startsWith ("(comment"))
	{
	    Object[] commentBlock = (Object[]) list[4];
	    if (commentBlock.length > 1) {
		Object[] text = (Object[])commentBlock[1];
		for (int i = 0; i < text.length; i++) {
		    comment += " " + (String)text[i];
		}
	    }

	    // cut off the comment part
	    Object[] newList = new Object[4];
	    System.arraycopy (list, 0, newList, 0, 4);
	    list = newList;
	}

	if ((list.length != 4)
	    || !(list[1] instanceof String)
	    || !(list[2] instanceof String)
	    || !(list[3] instanceof Object[]))
	{
	    throw new RuntimeException ("error: bad define-func form");
	}

	String name = (String) list[1];
	TypeSpec returnType = new TypeSpec (myTypeConverter, (String) list[2]);
	ArgumentList args = 
	    new ArgumentList (evalArgList ((Object[]) list[3]));

	DefineFunc def = new DefineFunc (name, returnType, args, comment);
	myDefineFuncs.addElement (def);

	if (DEBUG)
	{
	    System.err.println ("parsed " + def);
	}

	if (warning != null)
	{
	    throw new RuntimeException (warning);
	}
    }

    /**
     * Evaluate a <code>define-object</code> list. The form syntax is:
     *
     * <blockquote><pre>(define-object <i>name</i>
     *                (<i>super-class</i>?)
     *                (comment (<i>class_comment</i>))?
     *                (fields (<i>type name</i> {(read-only|read-write)})*)?
     *                (struct-constructor)?)
     * </pre></blockquote>
     *
     * @param list the list to evaluate
     */
    private void evalDefineObject (Object[] list)
    {
	if ((list.length < 3)
	    || (list.length > 6)
	    || !(list[1] instanceof String)
	    || !(list[2] instanceof Object[]))
	{
	    throw new RuntimeException ("error: bad define-object form");
	}

	TypeSpec name = new TypeSpec (myTypeConverter, (String) list[1]);
	TypeSpec superclass;
	ArgumentSpec[] fields;
	String comment = null;
	boolean optStructConstructor = false;

	Object[] inheritList = (Object[]) list[2];
	if (inheritList.length == 0)
	{
	    superclass = null;
	}
	else if ((inheritList.length == 1)
		 && (inheritList[0] instanceof String))
	{
	    superclass = 
		new TypeSpec (myTypeConverter, (String) inheritList[0]);
	}
	else
	{
	    throw new RuntimeException ("error: bad define-object form");
	}

	if (list.length > 3) {
	    if (! (list[3] instanceof Object[])) {
		throw new RuntimeException ("error: bad define-object form");
	    }

	    Object[] comm = (Object[]) list[3];
	    if (((String)comm[0]).equals("comment")) {
		if (comm.length > 1) {
		    comment = "";
		    Object[] text = (Object[]) comm[1];
		    for (int i = 0; i < text.length; i++) {
			comment += " " + (String)text[i];
		    }
		}
	    }
	    else {
		throw new RuntimeException("error: bad define-object form");
	    }
	}

	if (list.length < 5)
	{
	    fields = null;
	}
	else
	{
	    fields = evalFields ((Object[]) list[4]);
	}
	    
	if (list.length == 6) {
	    if (!(list[5] instanceof Object[]))
		throw new RuntimeException("error: bad define-object form");

	    Object subl[] = (Object[])list[5];
	    
	    if (subl.length != 1 || !(subl[0] instanceof String))
		throw new RuntimeException("error: bad define-object form");
	    
	    if (subl[0].equals("struct-constructor")) {
		optStructConstructor = true;
	    }
	    else
		throw new RuntimeException("error: bad define-object form");
	}

	DefineObject def = new DefineObject (name, superclass, fields,
					     optStructConstructor, comment);
	myDefineObjects.addElement (def);

	if (DEBUG)
	{
	    System.err.println ("parsed " + def);
	}
    }

    /**
     * Evaluate a <code>fields</code> list. The form syntax is:
     *
     * <blockquote><pre>(fields (<i>type name</i>)*)</pre></blockquote>
     *
     * @param list the list to evaluate
     * @return the result of evaluation
     */
    private ArgumentSpec[] evalFields (Object[] list)
    {
	if ((list.length < 1)
	    || !list[0].equals ("fields"))
	{
	    throw new RuntimeException ("error: bad fields form");
	}

	Object[] list1 = new Object[list.length - 1];
	System.arraycopy (list, 1, list1, 0, list1.length);
	return evalArgList (list1);
    }

    /**
     * Evaluate an argument declaration list. The form syntax is
     * a list of elements of the form:
     *
     * <blockquote><pre>(<i>type</i> 
     *  <i>name</i>
     *  [ (null-ok) | (read-only) | (read-write) |
     *     (= <i>default-value</i>) ]*)</pre></blockquote>
     *
     * @param list the list to evaluate
     * @return the result of evaluation
     */
    private ArgumentSpec[] evalArgList (Object[] list)
    {
	ArgumentSpec[] args = new ArgumentSpec[list.length];
	boolean hasObject = false;

	for (int i = 0; i < list.length; i++)
	{
	    if (! (list[i] instanceof Object[]))
	    {
		throw new RuntimeException ("error: bad argument list form");
	    }
	    Object[] one = (Object[]) list[i];
	    if ((one.length < 2)
		|| (one.length > 4)
		|| !(one[0] instanceof String)
		|| !(one[1] instanceof String))
	    {
		throw new RuntimeException ("error: bad argument list form");
	    }
	    String type = (String) one[0];
	    String name = (String) one[1];
	    boolean nullOk = false;
	    boolean readOnly = false;
	    boolean readWrite = false;
	    String defaultVal = null;
	    boolean isObject = false;
	    
	    for (int j = 2; j < one.length; j++)
	    {
		if (! (one[j] instanceof Object[]))
		{
		    throw new RuntimeException (
                        "error: bad argument list form");
		}
		Object[] propList = (Object[]) one[j];

		if (propList.length == 1)
		{
		    if (propList[0].equals ("null-ok"))
		    {
			if (nullOk)
			{
			    throw new RuntimeException (
                                "error: bad argument list form");
			}
			nullOk = true;
		    }
		    else if (propList[0].equals ("read-only"))
		    {
			if (readOnly || readWrite)
			{
			    throw new RuntimeException (
                                "error: bad argument list form");
			}
			readOnly = true;
		    }
		    else if (propList[0].equals ("read-write"))
		    {
			if (readOnly || readWrite)
			{
			    throw new RuntimeException (
                                "error: bad argument list form");
			}
			readWrite = true;
		    }
		    else if (propList[0].equals("object")) {
			if (isObject || hasObject) {
			    throw new RuntimeException(
			      "error: bad argument list form");
			}
			hasObject = isObject = true;
		    }
		    else
		    {
			throw new RuntimeException (
                            "error: bad argument list form");
		    }
		}
		else if ((propList.length == 2)
			 && propList[0].equals ("=")
			 && (propList[1] instanceof String))
		{
		    if (defaultVal != null)
		    {
			throw new RuntimeException (
                            "error: bad argument list form");
		    }

		    defaultVal = (String) propList[1];
		}
		else
		{
		    throw new RuntimeException (
                        "error: bad argument list form");
		}
	    }

	    if (! readWrite)
	    {
		// default is read-only if unspecified
		readOnly = true;
	    }

	    ArgumentSpec arg = 
		new ArgumentSpec (myTypeConverter, type, name, defaultVal, 
				  nullOk, readOnly, isObject);
	    args[i] = arg;
	}

	return args;
    }

    /**
     * Evaluate a list of value bindings. The form syntax is
     * a list of elements of the form:
     *
     * <blockquote><code>(<i>name GLIB_NAME</i> (= 
     * <i>value</i>)?)*)</code></blockquote>
     *
     * @param list the list to evaluate
     * @return the result of evaluation
     */
    private NamedConstantDef[] evalValueBindings (Object[] list)
    {
	NamedConstantDef[] result = new NamedConstantDef[list.length];
	int value = 0;

	for (int i = 0; i < list.length; i++)
	{
	    if (! (list[i] instanceof Object[]))
	    {
		throw new RuntimeException ("error: bad value bindings form");
	    }
	    Object[] one = (Object[]) list[i];
	    if ((one.length < 2)
		|| (one.length > 3)
		|| !(one[0] instanceof String)
		|| !(one[1] instanceof String))
	    {
		throw new RuntimeException ("error: bad value bindings form");
	    }

	    if (one.length == 3)
	    {
		if (! (one[2] instanceof Object[]))
		{
		    throw new RuntimeException (
                        "error: bad value bindings form");
		}

		Object[] valExpr = (Object[]) one[2];
		if ((valExpr.length != 2)
		    || !valExpr[0].equals ("="))
		{
		    throw new RuntimeException (
                        "error: bad value bindings form");
		}
		
	        try
		{
		    value = Integer.parseInt (valExpr[1].toString ());
		}
		catch (NumberFormatException ex)
		{
		    throw new RuntimeException (
                        "error: bad value bindings form");
		}
	    }

	    String schemeName = (String) one[0];
	    String glibName = (String) one[1];
	    result[i] = 
		new NamedConstantDef (schemeName, glibName, "" + value);

	    value++;
	}

	return result;
    }

    /**
     * Evaluate a list of string bindings. The form syntax is
     * a list of elements of the form:
     *
     * <blockquote><pre>("<i>name</i>" <i>GLIB_NAME</i>)</pre></blockquote>
     *
     * @param list the list to evaluate
     * @return the result of evaluation
     */
    private NamedConstantDef[] evalStringBindings (Object[] list)
    {
	NamedConstantDef[] result = new NamedConstantDef[list.length];

	for (int i = 0; i < list.length; i++)
	{
	    if (! (list[i] instanceof Object[]))
	    {
		throw new RuntimeException ("error: bad string bindings form");
	    }
	    Object[] one = (Object[]) list[i];
	    if ((one.length != 2)
		|| !(one[0] instanceof String)
		|| !(one[1] instanceof String))
	    {
		throw new RuntimeException ("error: bad string bindings form");
	    }

	    String value = (String) one[0];
	    String glibName = (String) one[1];
	    result[i] = new NamedConstantDef (glibName, glibName, value);
	}

	return result;
    }



    // ------------------------------------------------------------------------
    // private static methods: sexp reader

    /**
     * Return a string form for the given sexp.
     *
     * @param sexp the sexp to format
     * @return the string form
     */
    static private String sexpToString (Object sexp)
    {
	if (sexp instanceof Object[])
	{
	    Object[] arr = (Object[]) sexp;
	    StringBuffer sb = new StringBuffer ();
	    sb.append ('(');
	    for (int i = 0; i < arr.length; i++)
	    {
		if (i != 0)
		{
		    sb.append (' ');
		}
		sb.append (sexpToString (arr[i]));
	    }
	    sb.append (')');
	    return sb.toString ();
	}
	else
	{
	    return sexp.toString ();
	}
    }

    /**
     * Read a sexp from the given reader. The returned form of a sexp
     * is a string-ar-array. A string is a leaf of the tree,
     * and an array is an internal node, itself in the analogous form
     * of a string-or-array.
     *
     * @param reader the reader to read from
     * @return the parsed sexp
     */
    static private Object readSexp (PushbackReader reader)
    throws IOException
    {
	skipWhitespace (reader);
	int c = reader.read ();
	if (c == -1)
	{
	    throw new EOFException ("expected");
	}

	reader.unread (c);

	switch (c)
	{
	    case ')':  
	    {
		reader.read (); // avoid infinite loop
		throw new RuntimeException ("error: unexpected character: )");
	    }
	    case '(':  return readList (reader);
	    case '\"': return readString (reader);
	    default:   return readIdent (reader);
	}
    }

    /**
     * Read a parenthesized list from the given reader.
     *
     * @param reader the reader to read from
     * @return the parsed list
     */
    private static Object[] readList (PushbackReader reader)
    throws IOException
    {
	int c = reader.read ();
	if (c != '(')
	{
	    throw new RuntimeException ("error: unexpected character: " + c);
	}

	Vector items = new Vector ();
	for (;;)
	{
	    skipWhitespace (reader);
	    c = reader.read ();
	    if (c == ')')
	    {
		break;
	    }
	    if (c == -1)
	    {
		throw new RuntimeException ("error: unexpected EOF");
	    }
	    reader.unread (c);
		
	    Object item = readSexp (reader);
	    items.addElement (item);
	}

	Object[] result = new Object[items.size ()];
	items.copyInto (result);
	return result;
    }

    /**
     * Read a double-quoted string from the given reader.
     *
     * @param reader the reader to read from
     * @return the parsed string
     */
    private static String readString (PushbackReader reader)
    throws IOException
    {
	int c = reader.read ();
	if (c != '\"')
	{
	    throw new RuntimeException ("error: unexpected character: " + c);
	}

	StringBuffer result = new StringBuffer ();
	for (;;)
	{
	    c = reader.read ();
	    if (c == '\"')
	    {
		break;
	    }
	    if (c == -1)
	    {
		throw new RuntimeException ("error: unexpected EOF: result: " + result);
	    }
	    if (c == '\\')
	    {
		c = reader.read ();
		switch (c)
		{
		    case '\"': c = '\"'; break;
		    case '\\': c = '\\'; break;
		    case 'n':  c = '\n'; break;
		    case 'r':  c = '\r'; break;
		    case 't':  c = '\t'; break;
		    case -1:
			throw new RuntimeException ("error: unexpected EOF");
		    default:
			throw new RuntimeException (
                            "error: unexpected character: " + (char) c);
		}
	    }
	    result.append ((char) c);
	}

	return result.toString ();
    }

    /**
     * Read a simple identifier from the given reader.
     *
     * @param reader the reader to read from
     * @return the parsed identifier
     */
    private static String readIdent (PushbackReader reader)
    throws IOException
    {
	StringBuffer result = new StringBuffer ();
	for (;;)
	{
	    int c = reader.read ();
	    if ((c == '(') || (c == ')') || (c == ';'))
	    {
		reader.unread (c);
		break;
	    }
	    else if ((c == -1) || Character.isWhitespace ((char) c))
	    {
		break;
	    }
	    result.append ((char) c);
	}

	return result.toString ().intern ();
    }

    /**
     * Skip whitespace on the given reader.
     *
     * @param reader the reader to manipulate
     */
    private static void skipWhitespace (PushbackReader reader)
    throws IOException
    {
	for (;;)
	{
	    int c = reader.read ();
	    if (c == -1)
	    {
		break;
	    }
	    if (c == ';')
	    {
		while ((c != -1) && (c != '\n') && (c != '\r'))
		{
		    c = reader.read ();
		}
	    }
	    if (! Character.isWhitespace ((char) c))
	    {
		reader.unread (c);
		break;
	    }
	}
    }
}
