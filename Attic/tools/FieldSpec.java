/**
 * Representation of a field on a class. Im addition to the standard
 * Java member properties, it has an optional initialization expression.
 *
 * <p>The Java-Gnome bindings library is free software distributed under the
 * terms of the GNU Library General Public License version 2.</p>
 *
 * @author Dan Bornstein, danfuzz@milk.com
 * @author Copyright 2000 Dan Bornstein, all rights reserved. 
 */
final public class FieldSpec
    extends JavaMemberSpec
{
    /** null-ok; the expression used to initialize this field */
    private String myInitializer;

    

    // ------------------------------------------------------------------------
    // constructor

    /**
     * Construct an instance.
     *
     * @param name non-null; the name of the field
     * @param type non-null; the type of the field
     * @param isStatic <code>true</code> if this is to be a
     * <code>static</code> member, <code>false</code> if this is to be an
     * instance member
     * @param isFinal <code>true</code> if this is to be a
     * <code>final</code> member, <code>false</code> if not
     * @param scope the scope of the member, represented by one of
     * these constants: {@link #SCOPE_PUBLIC}, {@link #SCOPE_PROTECTED},
     * {@link #SCOPE_PACKAGE}, or {@link #SCOPE_PRIVATE}
     * @param initializer null-ok; an expression which initializes this
     * instance
     */
    public FieldSpec (String name, TypeSpecIface type, boolean isStatic,
		      boolean isFinal, int scope, String initializer)
    {
	super (name, type, isStatic, isFinal, scope);

	myInitializer = initializer;
    }



    // ------------------------------------------------------------------------
    // public instance methods

    // interface's javadoc suffices
    public String javaSource ()
    {
	StringBuffer sb = new StringBuffer ();

	sb.append (getDeclaration ());

	if (myInitializer != null)
	{
	    sb.append (" = ");
	    sb.append (myInitializer);
	}

	sb.append (";\n");
	return sb.toString ();
    }

    // interface's javadoc suffices
    public String cSource ()
    {
	// there is no C source associated with this
	return "";
    }

    // interface's javadoc suffices
    public String cHeader ()
    {
	// there is no C header associated with this
	return "";
    }
}
