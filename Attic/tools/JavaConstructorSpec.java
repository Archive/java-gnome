/**
 * Representation of a Java constructor (that is, non-native) on a class.
 * In addition to the standard Java member properties, it has an argument
 * list and a Java method body. The inherited name is degenerate on
 * instances of this class and is set to the simple (unqualified) name of
 * the class the instance is a constructor for. Similarly, for the purposes
 * of the superclass, this member is considered non-<code>static</code> and
 * non-<code>final</code>.
 *
 * <p>The Java-Gnome bindings library is free software distributed under the
 * terms of the GNU Library General Public License version 2.</p>
 *
 * @author Dan Bornstein, danfuzz@milk.com
 * @author Copyright 2000 Dan Bornstein, all rights reserved. 
 */
final public class JavaConstructorSpec extends JavaMemberSpec {
	/** non-null; the list of arguments to the constructor */
	private ArgumentList myArgumentList;

	/** non-null; the Java code for the constructor body */
	private String myBody;

	/** non-null; the comment associated with this constructor */
	private String myComment;

	// ------------------------------------------------------------------------
	// constructor

	public JavaConstructorSpec(
		TypeSpecIface type,
		int scope,
		ArgumentList argumentList,
		String body) {
		this(type, scope, argumentList, body, null);
	}

	/**
	 * Construct an instance.
	 *
	 * @param type non-null; the type the constructor is for
	 * @param scope the scope of the instance, represented by one of
	 * these constants: {@link #SCOPE_PUBLIC}, {@link #SCOPE_PROTECTED},
	 * {@link #SCOPE_PACKAGE}, or {@link #SCOPE_PRIVATE}
	 * @param argumentList non-null; list of arguments to the method
	 * @param body non-null; the Java code for the constructor body; it
	 * should not have braces around its entirety (those are provided for
	 * you at no additional cost) 
	 * @param comment non-null; the comment asscoiated with this constructor
	 */
	public JavaConstructorSpec(
		TypeSpecIface type,
		int scope,
		ArgumentList argumentList,
		String body,
		String comment) {
		super(type.simpleJavaType(), type, false, false, scope);

		if (argumentList == null) {
			throw new NullPointerException("argumentList == null");
		}

		if (body == null) {
			throw new NullPointerException("body == null");
		}

		myArgumentList = argumentList;
		myBody = body;
		myComment = comment;
	}

	// ------------------------------------------------------------------------
	// public instance methods

	/**
	 * Get the argument list.
	 *
	 * @return the argument list
	 */
	public ArgumentList getArgumentList() {
		return myArgumentList;
	}

	// interface's javadoc suffices
	public String javaSource() {
		StringBuffer sb = new StringBuffer();

		// Insert the method comment here
		if (myComment != null) {
			sb.append("/**\n");
			String output = "*";
			java.util.StringTokenizer st =
				new java.util.StringTokenizer(myComment);

			while (st.hasMoreElements()) {
				String tmp = st.nextToken();
				if (output.length() + tmp.length() < 70) {
					output += " " + tmp;
				} else {
					sb.append(output + "\n");
					output = "* " + tmp;
				}
			}
			sb.append(output + "\n*/\n\n");
		}

		sb.append(getScopeString());
		sb.append(' ');
		sb.append(GlibTranslator.javaGnomeName(myName));
		sb.append(" (");
		sb.append(myArgumentList.publicJavaArguments());
		sb.append(")\n");
		sb.append("{\n");
		sb.append(myBody);
		sb.append("}\n");

		return sb.toString();
	}

	// interface's javadoc suffices
	public String cSource() {
		// there is no C source associated with this
		return "";
	}

	// interface's javadoc suffices
	public String cHeader() {
		// there is no C header associated with this
		return "";
	}
}
