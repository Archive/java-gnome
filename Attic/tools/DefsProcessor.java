import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * Class that runs the conversion of <code>.defs</code> files to
 * a set of glue files in Java and C.
 *
 * <p>The Java-Gnome bindings library is free software distributed under the
 * terms of the GNU Library General Public License version 2.</p>
 *
 * @author Dan Bornstein, danfuzz@milk.com
 * @author based on code originally by Olivier Gutknecht, gutkneco@lirmm.fr
 * @author Copyright 2000 Dan Bornstein, all rights reserved.
 */
public final class DefsProcessor {
	/** non-null; the {@link FileEmitter} to use */
	private FileEmitter myFileEmitter;

	/** non-null; the {@link TypeConverter} to use */
	private TypeConverter myTypeConverter;

	/** non-null; the {@link DefsReader} to use */
	private DefsReader myDefsReader;

	/** non-null; the {@link GlibTranslator} to use */
	private GlibTranslator myTranslator;

	/** non-null; table mapping types to {@link ClassSpec} objects that
	 * will be output for the type in question */
	private Hashtable myClassMap;

	/** count of warnings that occurred during processing */
	private int myWarningCount;

	/** count of errors that occurred during processing */
	private int myErrorCount;

	/** base directory for Java files */
	private String javaBaseDir;

	// ------------------------------------------------------------------------
	// main

	/**
	 * Do the processing, soup to nuts.
	 *
	 * @param args the commandline arguments
	 */
	static public void main(String[] args) throws Exception {
		// FIXME: should be a commandline argument
		String propsRsrc = "JNIProps.txt";

		String javaDir = null;
		String cSourceDir = null;
		String cHeaderDir = null;

		String defsFile = null;
		String outList = null;
		String suppressList = null;

		for (int i = 0; i < args.length; i++) {
			if (args[i].startsWith("--out-dir=")) {
				javaDir = args[i].substring(args[i].indexOf('=') + 1);
				cSourceDir = javaDir + File.separator + "jni";
				cHeaderDir = cSourceDir;
			} else if (args[i].startsWith("--out-list=")) {
				outList = args[i].substring(args[i].indexOf('=') + 1);
			} else if (args[i].startsWith("--suppress-list=")) {
				suppressList = args[i].substring(args[i].indexOf('=') + 1);
			} else {
				if (defsFile != null) {
					throw new RuntimeException(
						"You may only specify one " + ".defs file.");
				}
				defsFile = args[i];
			}
		}

		if (javaDir == null) {
			System.err.println("no --out-dir specified");
			System.exit(1);
		}

		if (defsFile == null) {
			System.err.println("no defs file specified");
			System.exit(1);
		}

		FileEmitter emit = new FileEmitter(javaDir, cSourceDir, cHeaderDir);
		TypeConverter tc = new TypeConverter(propsRsrc);

		DefsProcessor dp = new DefsProcessor(emit, tc);

		dp.setJavaBaseDirectory(javaDir);

		dp.readFile(defsFile);
		dp.registerTypes();
		dp.addFuncs();
		dp.addCatchalls();

		if (suppressList != null) {
			dp.suppressClasses(suppressList);
		}

		dp.emitClasses();

		if (outList != null) {
			dp.writeOutList(outList);
		}

		dp.summary();

		if (dp.gotErrors()) {
			System.exit(1);
		}
	}

	// ------------------------------------------------------------------------
	// constructor

	/**
	 * This class is not publicly instantiable.
	 *
	 * @param fileEmitter non-null; the {@link FileEmitter} to use
	 * @param typeConverter non-null; the {@link TypeConverter} to use
	 */
	private DefsProcessor(
		FileEmitter fileEmitter,
		TypeConverter typeConverter) {
		if (fileEmitter == null) {
			throw new NullPointerException("fileEmitter == null");
		}

		if (typeConverter == null) {
			throw new NullPointerException("typeConverter == null");
		}

		myFileEmitter = fileEmitter;
		myTypeConverter = typeConverter;

		myDefsReader = new DefsReader(typeConverter);
		myTranslator =
			new GlibTranslator(typeConverter, new DefsDefaultTranslator());
		myClassMap = new Hashtable();

		myErrorCount = 0;
		myWarningCount = 0;
	}

	// ------------------------------------------------------------------------
	// private instance methods

	/**
	 * Set the base directory for Java code.
	 *
	 * @param directory the name of the directory.
	 */
	public void setJavaBaseDirectory(String directory) {
		javaBaseDir = directory;
	}

	/**
	 * Read the given <code>.defs</code> file, but do no actual processing
	 * of it.
	 *
	 * @param fileName the name of the file to read.
	 */
	private void readFile(String fileName) {
		try {
			myDefsReader.readFile(fileName);
		} catch (Exception ex) {
			handleException(ex, "occurred while processing input file");
		}
	}

	/**
	 * Register the types that have been referred to in the files
	 * that have been read. This is what allows the {@link TypeConverter}
	 * to be able to translate them and also initializes a {@link
	 * ClassSpec} for each one, so that methods and fields may later be
	 * added to them.
	 */
	private void registerTypes() {
		System.err.println("registering types...");

		registerEnumTypes();
		registerStringEnumTypes();
		registerFlagsTypes();
		registerBoxedTypes();
		registerObjectTypes();
	}

	/**
	 * Do registration for boxed types.
	 */
	private void registerBoxedTypes() {
		DefineBoxed[] boxeds = myDefsReader.getDefineBoxeds();
		for (int i = 0; i < boxeds.length; i++) {
			try {
				ClassSpec spec = myTranslator.translate(boxeds[i]);
				myClassMap.put(spec.getType(), spec);
			} catch (Exception ex) {
				handleException(ex, "occurred while processing: " + boxeds[i]);
			}
		}
	}

	/**
	 * Do registration for enum types.
	 */
	private void registerEnumTypes() {
		DefineEnumOrFlags[] enums = myDefsReader.getDefineEnums();
		for (int i = 0; i < enums.length; i++) {
			try {
				ClassSpec spec = myTranslator.translate(enums[i]);
				myClassMap.put(spec.getType(), spec);
			} catch (Exception ex) {
				handleException(ex, "occurred while processing: " + enums[i]);
			}
		}
	}

	/**
	 * Do registration for string enum types.
	 */
	private void registerStringEnumTypes() {
		DefineEnumOrFlags[] enums = myDefsReader.getDefineStringEnums();
		for (int i = 0; i < enums.length; i++) {
			try {
				ClassSpec spec = myTranslator.translate(enums[i]);
				myClassMap.put(spec.getType(), spec);
			} catch (Exception ex) {
				handleException(ex, "occurred while processing: " + enums[i]);
			}
		}
	}

	/**
	 * Do registration for flags types.
	 */
	private void registerFlagsTypes() {
		DefineEnumOrFlags[] flags = myDefsReader.getDefineFlags();
		for (int i = 0; i < flags.length; i++) {
			try {
				ClassSpec spec = myTranslator.translate(flags[i]);
				myClassMap.put(spec.getType(), spec);
			} catch (Exception ex) {
				handleException(ex, "occurred while processing: " + flags[i]);
			}
		}
	}

	/**
	 * Do registration for object types.
	 */
	private void registerObjectTypes() {
		DefineObject[] objects = myDefsReader.getDefineObjects();
		for (int i = 0; i < objects.length; i++) {
			try {
				ClassSpec spec = myTranslator.translate(objects[i]);
				myClassMap.put(spec.getType(), spec);
			} catch (Exception ex) {
				handleException(ex, "occurred while processing: " + objects[i]);
			}
		}
	}

	/**
	 * Iterate over all of the function definitions, determining the
	 * most appropriate class to associate each one with, and then
	 * turning it into an appropriate {@link Sourceable} which gets
	 * added to that class.
	 */
	private void addFuncs() {
		System.err.println("associating functions with classes...");

		DefineFunc[] funcs = myDefsReader.getDefineFuncs();

		for (int i = 0; i < funcs.length; i++) {
			try {
				ClassSpec spec = myTranslator.translate(funcs[i]);
				if (spec != null) {
					// only do anything more if it wasn't handled as a
					// catch-all
					ClassSpec already = getPreexistingClass(spec.getType());
					already.addMembers(spec);
				}
			} catch (RuntimeException ex) {
				handleException(ex, "occurred while processing: " + funcs[i]);
			}
		}
	}

	/**
	 * Get the {@link ClassSpec} associated with a given {@link TypeSpecIface}.
	 * The class should already exist; if not, an exception is thrown.
	 *
	 * @param type the type to look up
	 * @return the associated {@link ClassSpec}
	 */
	private ClassSpec getPreexistingClass(TypeSpecIface type) {
		ClassSpec already = (ClassSpec) myClassMap.get(type);
		if (already == null) {
			throw new RuntimeException(
				"error: cannot find class for " + "type: " + type);
		}

		return already;
	}

	/**
	 * Add the catch-all classes to the full list of {@link ClassSpec}
	 * objects maintained by this instance.
	 */
	private void addCatchalls() {
		ClassSpec[] ca = myTranslator.getCatchalls();
		for (int i = 0; i < ca.length; i++) {
			myClassMap.put(ca[i].getType(), ca[i]);
		}
	}

	/**
	 * Suppress the classes listed in the given file. What this means is
	 * that they are removed from the list of classes to emit. The file
	 * should be a file that was written by using the
	 * <code>--out-list=</code> commandline option, or at least something
	 * in that format.
	 *
	 * @see #writeOutList
	 * 
	 * @param fileName non-null; the name of the file containing
	 * the list of classes to suppress 
	 */
	private void suppressClasses(String fileName) {
		System.err.println("suppressing classes...");

		// the names of all of the classes to suppress, as read in
		// from the file
		Vector supNames = new Vector(100);

		try {
			BufferedReader r = new BufferedReader(new FileReader(fileName));
			for (;;) {
				String one = r.readLine();
				if (one == null) {
					break;
				}
				one = one.trim();
				supNames.addElement(one);
			}
		} catch (Exception ex) {
			handleException(
				ex,
				"occurred while suppressing classes from file: " + fileName);
		}

		// a built-up list of all of the types to remove; we do it
		// this way to avoid "concurrent modification" problems; when
		// we can drop support for 1.1, this can be made much more
		// straightforward
		Vector supTypes = new Vector();

		Enumeration e = myClassMap.keys();
		while (e.hasMoreElements()) {
			TypeSpecIface one = (TypeSpecIface) e.nextElement();
			String oneClass = one.publicJavaType();
			int foundAt = supNames.indexOf(oneClass);
			if (foundAt != -1) {
				supTypes.addElement(one);
				supNames.removeElementAt(foundAt);
			}
		}

		// do the actual removals
		e = supTypes.elements();
		while (e.hasMoreElements()) {
			TypeSpecIface one = (TypeSpecIface) e.nextElement();
			myClassMap.remove(one);
		}

		// warn about classes that were named to be suppressed but
		// weren't ever constructed in the first place
		e = supNames.elements();
		while (e.hasMoreElements()) {
			String one = (String) e.nextElement();
			RuntimeException warning =
				new RuntimeException(
					"warning: class to suppress wasn't "
						+ "ever constructed: "
						+ one);
			handleException(warning, "occurred while suppressing classes");
		}
	}

	/**
	 * Emit all of the classes that have been built up.
	 */
	private void emitClasses() {
		System.err.print("emitting classes...");

		int count = 0;

		Enumeration e = myClassMap.elements();
		while (e.hasMoreElements()) {
			emitOne((ClassSpec) e.nextElement());
			count++;
		}

		System.err.println(
			"\ndone emitting classes (wrote " + count + " of them)");
	}

	/**
	 * Emit a single {@link ClassSpec}.
	 *
	 * @param spec the class to emit
	 */
	private void emitOne(ClassSpec spec) {
		TypeSpecIface type = spec.getType();

		System.out.print(".");
		try {
			// note: generate everything before emitting anything, so we
			// don't end up spitting out a partial set of files
			String js = spec.javaSource();
			String cs = spec.cSource();

			myFileEmitter.emitJavaSource(type, js);
			myFileEmitter.emitCSource(type, cs);
		} catch (Exception ex) {
			handleException(ex, "occurred while emitting code for " + type);
		}
	}

	/**
	 * Write the list of classes emitted to the given file. This
	 * is done when the <code>--out-list=</code> commandline option is
	 * specified. The format is simply one fully qualified classname per line.
	 *
	 * @param fileName non-null; the name of the file to write
	 */
	private void writeOutList(String fileName) {
		System.err.println("writing output list...");

		try {
			FileWriter w = new FileWriter(fileName);

			Enumeration e = myClassMap.keys();
			while (e.hasMoreElements()) {
				TypeSpecIface one = (TypeSpecIface) e.nextElement();
				String className = one.publicJavaType();
				int index = className.lastIndexOf(".");
				String shortName = className.substring(index + 1);
				String pkg = className.substring(0, index + 1);
				className = GlibTranslator.javaGnomeName(shortName);
				w.write(pkg + className);
				w.write('\n');
			}

			w.close();
		} catch (Exception ex) {
			handleException(
				ex,
				"occurred while writing output list file: " + fileName);
		}
	}

	/**
	 * Handle the given exception. This will interpret messages to
	 * extract out internally-generated errors and warnings, while
	 * spitting out stacktraces for the more unexpected things.
	 *
	 * @param ex the exception to handle
	 * @param desc a description of where/when the exception was caught
	 */
	private void handleException(Exception ex, String desc) {
		String msg = ex.getMessage();
		String prefix;
		if ((msg != null) && msg.startsWith("warning: ")) {
			System.err.println(msg);
			myWarningCount++;
			prefix = "warning: ";
		} else if ((msg != null) && msg.startsWith("error: ")) {
			System.err.println(msg);
			myErrorCount++;
			prefix = "error: ";
		} else {
			System.err.println("error: totally unexpected error");
			ex.printStackTrace();
			myErrorCount++;
			prefix = "error: ";
		}

		System.err.println(prefix + desc);
	}

	/**
	 * Print out a summary of the errors and/or warnings.
	 */
	private void summary() {
		myWarningCount += myTranslator.getWarningCount();

		if (myWarningCount != 0) {
			System.err.println(
				"warning: Got " + myWarningCount + " warning(s) total.");
		}

		if (myErrorCount != 0) {
			System.err.println(
				"error: Got " + myErrorCount + " error(s) total.");
		}
	}

	/**
	 * Return <code>true</code> if there were any errors during
	 * processing.
	 *
	 * @return <code>true</code> if there were any errors during
	 * processing
	 */
	private boolean gotErrors() {
		return (myErrorCount != 0);
	}

	// ------------------------------------------------------------------------
	// helper classes

	/**
	 * Default translator that knows about all of the types defined via
	 * this (outer) instance.
	 */
	private class DefsDefaultTranslator implements DefaultTranslator {
		// interface's javadoc suffices
		public String translate(String schemeType, String expr) {
			if (myTypeConverter.isEnumOrFlagType(schemeType)) {
				// it's an enum or flags type, so we'll have to
				// translate the expression
				TypeSpec type = new TypeSpec(myTypeConverter, schemeType);
				ClassSpec spec = (ClassSpec) myClassMap.get(type);

				if (spec == null) {
					throw new RuntimeException(
						"error: cannot translate default for unknown type: "
							+ schemeType);
				}

				if (expr.indexOf('|') == -1) {
					// it doesn't look fancy; just turn it into the right
					// simple static reference; this uses intern because
					// the translated value is the raw int, not the object
					return type.publicJavaType()
						+ ".intern ("
						+ spec.translateConstant(expr)
						+ ")";
				}

				// it looks like a flag being built up with | (OR);
				// massage it so it refers to the right class/constant

				StringBuffer result = new StringBuffer();
				result.append(type.publicJavaType());
				result.append(".intern (");

				StringTokenizer st = new StringTokenizer(expr, "|");
				boolean first = true;
				while (st.hasMoreTokens()) {
					if (first) {
						first = false;
					} else {
						result.append(" | ");
					}

					String tok = st.nextToken();
					result.append(spec.translateConstant(tok));
				}

				result.append(")");
				return result.toString();
			} else {
				// easy stuff; turn simple C constants into their usual
				// Java counterparts, and leave the rest alone
				if (expr.equals("TRUE")
					|| expr.equals("FALSE")
					|| expr.equals("NULL")) {
					return expr.toLowerCase();
				}

				return expr;
			}
		}
	}
}
