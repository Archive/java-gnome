import java.util.Hashtable;

/**
 * Representation of a Scheme boxed declaration, which will eventually
 * get translated into either a {@link ClassDefinition}
 * It consists of a type (that is, the type being defined) and a list
 * of fields.
 *
 * <p>The Java-Gnome bindings library is free software distributed under the
 * terms of the GNU Library General Public License version 2.</p>
 *
 * @author Dan Bornstein, danfuzz@milk.com
 * @author Copyright 2000 Dan Bornstein, all rights reserved. 
 */
final public class DefineBoxed
{
    /** non-null; the type being defined */
    private TypeSpec myType;

    /** null-ok; the superclass's type */
    private TypeSpec myExtends;

    /** non-null; the field list */
    private ArgumentSpec[] myFieldList;


    // ------------------------------------------------------------------------
    // constructor

    /**
     * Construct an instance. The only key in <code>specs</code> table 
     * that is used is <code>"fields"</code>, which should contain an
     * instance of {@link ArgumentList}.
     *
     * @param type non-null; the type being defined
     * @param extendsType null-ok; the superclass of this type
     * @param specs non-null; a map of specs
     */
    public DefineBoxed (TypeSpec type, TypeSpec extendsType, Hashtable specs)
    {
	if (type == null)
	{
	    throw new NullPointerException ("type == null");
	}

	if (specs == null)
	{
	    throw new NullPointerException ("specs == null");
	}

	myType = type;
	myExtends = extendsType;

	myFieldList = (ArgumentSpec[]) specs.get ("fields");
	if (myFieldList == null)
	{
	    myFieldList = new ArgumentSpec[0];
	}
    }



    // ------------------------------------------------------------------------
    // public instance methods

    /**
     * Return the string form for this instance.
     *
     * @return the string form
     */
    public String toString ()
    {
	return "(define-boxed " + myType + " " + myFieldList + ")";
    }

    /**
     * Get the type.
     *
     * @return non-null; the type
     */
    public TypeSpec getType ()
    {
	return myType;
    }

    /**
     * Get the superclass type.
     *
     * @return null-ok; the superclass type
     */
    public TypeSpec getExtends ()
    {
	return myExtends;
    }

    /**
     * Get the field list.
     *
     * @return non-null; the field list
     */
    public ArgumentSpec[] getFieldList ()
    {
	return myFieldList;
    }
}
