/**
 * Representation of a "structure" constructor method. Structure
 * constructor is meant only for types which have no natural C
 * constructor (_new) and they are meant to be used just as normal C
 * structures. 
 *
 * This implementation is a wrapper which will allocate the underlying
 * structure and wrap it as a full Java object.
 *
 * <p>The Java-Gnome bindings library is free software distributed under the
 * terms of the GNU Library General Public License version 2.</p>
 *
 * @author Santeri Paavolainen, santtu@iki.fi
 * @author Copyright 2001 Santeri Paavolainen, all rights reserved.
 */

final public class StructConstructorSpec
    extends JavaMemberSpec
{
    ArgumentList myArgumentList;

    public StructConstructorSpec(TypeSpecIface type, int scope) {
        super(type.simpleJavaType() + "_new", type, false, false, scope);

        myArgumentList = new ArgumentList(null);
    }

    public String javaSource() {
        return "native static private " +
            myType.publicJavaType() + " " +
            myName + "();\n" +
            getModifiers() + " " +
            myType.simpleJavaType() + "() {\n" + 
            "nativepeer = " + myName + "().nativepeer;\n}\n";
    }

    public String cSource() {
        StringBuffer result = new StringBuffer (1000);

        result.append(cFunctionDeclaration());
        result.append("\n{\n");

        // get the argument variable conversions
        String[] javaToGlib = myArgumentList.javaToGlibNativeStatements ();

        // append the initial declarations and conversions
        result.append (javaToGlib[0]);

        StringBuffer call = new StringBuffer();

        call.append("g_malloc0(sizeof(");
        call.append(myType.schemeType());
        call.append("))");

        // append the call/cleanup/return code
        result.append (myType.glibToNativeReturn (call.toString(),
                                                  javaToGlib[1]));
            
        result.append("}\n");
        return result.toString ();
    }

    public String cHeader() {
        return cFunctionDeclaration() + ";\n";
    }

    private String cFunctionDeclaration() {
        StringBuffer result = new StringBuffer();

        /* This is based on the cFunctionDeclaraion (sic) from
           NativeMethodSpec.java. Maybe it could be extended to make
           it possible to use it for struct constructors? Or maybe
           just subclassing.. */

        result.append ("/*\n");

        result.append (" * Class:     ");
        result.append (myType.publicJavaType ());
        result.append ("\n");

        result.append (" * Method:    ");
        result.append (myName);
        result.append ("\n");

        result.append (" * Signature: (");
        result.append (myArgumentList.nativeJavaSignature ());
        result.append (")");
        result.append (myType.nativeJavaSignature ());
        result.append ("\n");

        result.append (" */\n");

        result.append ("JNIEXPORT ");
        result.append (myType.nativeType ());
        result.append (" JNICALL ");
        result.append (cFunctionName (myType, myName));
        result.append (" (JNIEnv *env, jclass cls)");
        return result.toString ();
    }

    /* directly from NativeMethodSpec.java.  This is a common idiom,
       and probably should be moved into a utility class as a static
       function.*/
    private static String cFunctionName (TypeSpecIface type, String name)
    {
	StringBuffer sb = new StringBuffer (100);
	sb.append ("Java_");

	String rawName = type.publicJavaType () + '.' + name;
	int len = rawName.length ();

	for (int i = 0; i < len; i++)
            {
                char c = rawName.charAt (i);
                switch (c)
                    {
                    case '.': sb.append ("_");  break;
                    case '_': sb.append ("_1"); break;
                    case ';': sb.append ("_2"); break;
                    case '[': sb.append ("_3"); break;
                    default:
                        {
                            if ((c >= 'a' && c <= 'z')
                                || (c >= 'A' && c <= 'Z')
                                || (c >= '0' && c <= '9'))
                                {
                                    sb.append (c);
                                }
                            else
                                {
                                    // FIXME: maybe this is right, but
                                    // see the text of the exception
                                    // that's thrown anyway for the
                                    // explanation
                                    sb.append ("_0");
                                    String digits =
                                        Integer.toString (((int) c) + 0x10000,
                                                          16).
                                        toUpperCase ();
                                    sb.append (digits.substring 
                                               (digits.length () - 4));
                                    throw new RuntimeException 
                                        ("The JNI spec doesn't say if weird " +
                                         "characters get represented in " +
                                         "decimal or hex, and if in " +
                                         "hex, what case.");
                                }
                            break;
                        }
                    }
            }

	return sb.toString ();
    }

}
