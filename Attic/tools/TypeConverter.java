import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Vector;

/**
 * Class for converting between "Scheme" (<code>.defs</code> file
 * format) types and various implementation types and wrapping code. This
 * class relies on a properties file containing type names and code fragments,
 * which it stitches together in various ways.
 *
 * <p>When you boil it all down, this class is simply responsible for
 * filling in these (italicized) blanks:</p>
 *
 * <blockquote><pre> public class <i>PublicJavaType</i>
 *
 * public type method (<i>PublicJavaType</i> arg) {
 *     return method0 (<i>javaUnwrap</i> (arg));
 * }
 *
 * public <i>PublicJavaType</i> method (type arg) {
 *     return <i>javaWrap</i> (method0 (arg));
 * }
 *
 * public native type method0 (<i>NativeToJavaType</i> arg);
 *
 * public native <i>NativeToJavaType</i> method0 (type arg);
 *     
 * type nativeMethod (<i>NativeType</i> arg) {
 *     <i>Gtype</i> arg0 = <i>nativeUnwrap</i> (arg);
 *     return gFunction (arg0);
 * }
 *
 * <i>nativeType</i> nativeMethod (type arg)
 * {
 *     return <i>nativeWrap</i> (gFunction (arg));
 * }</pre></blockquote>
 *
 * <p>There are three general calling patterns that are
 * specified--enumeration types, flags types, and objects--and everything
 * else is treated as a special case which must be individually defined.
 * See the actual properties file(s) for more details. (The JNI-specific
 * one is called <code>"JNIProps.txt"</code> and should be in the same
 * directory as the code for this class.)</p>
 *
 * <p>The Java-Gnome bindings library is free software distributed under the
 * terms of the GNU Library General Public License version 2.</p>
 *
 * @author Dan Bornstein, danfuzz@milk.com
 * @author Copyright 2000 Dan Bornstein, all rights reserved. 
 */
public final class TypeConverter {
	/** whether to issue debug messages */
	private final boolean DEBUG = false;

	/** list of all the known Scheme types, as strings */
	private Vector myTypes;

	/** set of properties defining the type mappings */
	private Properties myProperties;

	/** list of keys referring to the BOXED translation */
	private String[] myBoxedKeys;

	/** list of keys referring to the ENUM translation */
	private String[] myEnumKeys;

	/** list of keys referring to the STRING_ENUM translation */
	private String[] myStringEnumKeys;

	/** list of keys referring to the FLAG translation */
	private String[] myFlagKeys;

	/** list of keys referring to the OBJECT translation */
	private String[] myObjectKeys;

	// ------------------------------------------------------------------------
	// constructors

	/**
	 * Construct an instance. It takes the name of a resource to load as a
	 * property file. Said file contains information about all the
	 * actual mappings that are done.
	 *
	 * @param rsrcName the name of the resource containing the mappings
	 */
	public TypeConverter(String rsrcName) {
		myTypes = new Vector();
		myProperties = new Properties();

		// read the properties
		try {
			InputStream is = ClassLoader.getSystemResourceAsStream(rsrcName);
			myProperties.load(is);
		} catch (IOException ex) {
			System.err.println(
				"Unable to load type conversion properties "
					+ "resource: "
					+ rsrcName);
			ex.printStackTrace();
			throw new RuntimeException("Problem constructing TypeConverter.");
		}

		// collate out the keys which refer to the schematic conversions,
		// and also note all of the explicitly-mentioned types
		Vector boxedKeys = new Vector();
		Vector enumKeys = new Vector();
		Vector stringEnumKeys = new Vector();
		Vector flagKeys = new Vector();
		Vector objectKeys = new Vector();
		Enumeration e = myProperties.keys();
		while (e.hasMoreElements()) {
			String key = (String) e.nextElement();
			if (key.startsWith("$BOXED.") || key.startsWith("$BOXED*.")) {
				boxedKeys.addElement(key);
			} else if (key.startsWith("$ENUM.") || key.startsWith("$ENUM*.")) {
				enumKeys.addElement(key);
			} else if (key.startsWith("$FLAG.") || key.startsWith("$FLAG*.")) {
				flagKeys.addElement(key);
			} else if (key.startsWith("$OBJECT.") || key.startsWith("$OBJECT*.")) {
				objectKeys.addElement(key);
			} else if (key.startsWith("$STRING_ENUM.")) {
				stringEnumKeys.addElement(key);
			} else {
				int dotAt = key.indexOf('.');
				if (dotAt == -1) {
					System.err.println("warning: ignoring weird key: " + key);
					continue;
				}
				key = key.substring(0, dotAt);
				if (!myTypes.contains(key)) {
					myTypes.addElement(key);
				}
			}
		}

		myBoxedKeys = new String[boxedKeys.size()];
		boxedKeys.copyInto(myBoxedKeys);

		myEnumKeys = new String[enumKeys.size()];
		enumKeys.copyInto(myEnumKeys);

		myStringEnumKeys = new String[stringEnumKeys.size()];
		stringEnumKeys.copyInto(myStringEnumKeys);

		myFlagKeys = new String[flagKeys.size()];
		flagKeys.copyInto(myFlagKeys);

		myObjectKeys = new String[objectKeys.size()];
		objectKeys.copyInto(myObjectKeys);
	}

	// ------------------------------------------------------------------------
	// public instance methods

	/**
	 * Register a newly-discovered boxed type. One must register
	 * these before they can be properly converted.
	 *
	 * @param schemeName the Scheme name for the type in question
	 */
	public void registerBoxedType(String schemeName) {
		bindKeys(schemeName, myBoxedKeys);
	}

	/**
	 * Register a newly-discovered enumerated type. One must register
	 * these before they can be properly converted.
	 *
	 * @param schemeName the Scheme name for the type in question
	 */
	public void registerEnumType(String schemeName) {
		bindKeys(schemeName, myEnumKeys);
	}

	/**
	 * Register a newly-discovered enumerated string type. One must register
	 * these before they can be properly converted. For these, the
	 * actual translation requirements are minimal; the type just needs
	 * to be registered for ancillary stuff (e.g., for class suppression
	 * to work).
	 *
	 * @param schemeName the Scheme name for the type in question
	 */
	public void registerStringEnumType(String schemeName) {
		bindKeys(schemeName, myStringEnumKeys);
	}

	/**
	 * Register a newly-discovered flag type. One must register
	 * these before they can be properly converted.
	 *
	 * @param schemeName the Scheme name for the type in question
	 */
	public void registerFlagsType(String schemeName) {
		bindKeys(schemeName, myFlagKeys);
	}

	/**
	 * Register a newly-discovered object type. One must register
	 * these before they can be properly converted.
	 *
	 * @param schemeName the Scheme name for the type in question
	 */
	public void registerObjectType(String schemeName) {
		bindKeys(schemeName, myObjectKeys);
	}

	/**
	 * Return <code>true</code> if the given Scheme type names
	 * a previously-registered enum or flags type, <code>false</code>
	 * otherwise.
	 *
	 * @param scheneMane the Scheme name for the type in question
	 * @return whether or not it refers to an enum or flags type
	 */
	public boolean isEnumOrFlagType(String schemeName) {
		String schema = nullOkProperty(schemeName + ".SCHEMA");

		if (schema == null) {
			return false;
		}

		return (schema.equals("ENUM") || schema.equals("FLAG"));
	}

	/**
	 * Return the type name that most closely matches the name given. In
	 * order to match, the type name must be a prefix of the given name. If
	 * there is more than one match, then the longest one wins. This is
	 * done without regard for capitalization or underscores. If there
	 * is no match at all, this returns <code>null</code>; if there are
	 * multiple "best" matches (which should never happen), then this throws
	 * an exception.
	 *
	 * @param name non-null; the name to match
	 * @return null-ok; the best matching type name, or <code>null</code> if
	 * no type name at all matched
	 */
	public String fuzzyTypeMatch(String name) {
		String multiples = null;
		String bestMatch = null;
		int bestMatchLength = 0;

		int sz = myTypes.size();
		for (int i = 0; i < sz; i++) {
			String type = (String) myTypes.elementAt(i);
			int len = fuzzyPrefixLength(name, type);
			if (len != type.length()) {
				continue;
			}

			if (len > bestMatchLength) {
				bestMatchLength = len;
				bestMatch = type;
				multiples = null;
			} else if (len == bestMatchLength) {
				// no unique match for the current best prefix length
				if (bestMatch != null) {
					multiples = bestMatch;
					bestMatch = null;
				}
				multiples += ", " + type;
			}
		}

		if (bestMatch == null) {
			if (bestMatchLength == 0) {
				return null;
			}

			throw new RuntimeException(
				"error: multiple \"best\" type "
					+ "matches found for name: "
					+ name
					+ "; "
					+ multiples);
		}

		return bestMatch;
	}

	/**
	 * Get the public Java type name corresponding to the given Scheme type
	 * name. If the result is a class, it is a fully-qualified name.
	 *
	 * @param schemeName non-null; the Scheme type name
	 * @return non-null; the Java type 
	 */
	public String publicJavaType(String schemeName) {
		return nonNullProperty(schemeName + ".javaType");
	}

	/**
	 * Get the Java type name corresponding to the Java level of a native
	 * declaration.
	 *
	 * @param schemeName non-null; the Scheme type name
	 * @return non-null; the Java type 
	 */
	public String nativeJavaType(String schemeName) {
		String result = nullOkProperty(schemeName + ".javaNative");
		if (result != null) {
			return result;
		}

		return nonNullProperty(schemeName + ".javaType");
	}

	/**
	 * Return <code>true</code> if the Scheme type of the given name
	 * requires conversion at the Java level, and <code>false</code>
	 * otherwise.
	 *
	 * @param schemeName the Scheme type name
	 * @return whether or not Java-level conversion needs to be done
	 */
	public boolean needsJavaConversion(String schemeName) {
		return nullOkProperty(schemeName + ".javaNative") != null;
	}

	/**
	 * Return <code>true</code> if code needs to be used to wrap
	 * a native return value into a public Java return value, for the
	 * given Scheme type, or <code>false</code> otherwise.
	 *
	 * @param schemeName the Scheme type name
	 * @return whether a native return value must be wrapped
	 */
	final public boolean requiresNativeToPublicJavaReturn(String schemeName) {
		// that is, you don't have to wrap if the types at the two
		// levels are identical
		String pubType = publicJavaType(schemeName);
		String natType = nativeJavaType(schemeName);
		return !pubType.equals(natType);
	}

	/**
	 * Return the Java level wrapping code to convert a function/method
	 * call of the given Scheme type from the native Java type a
	 * <code>return</code> statement of the public Java type. If there
	 * is no specific conversion specified as a property, then this
	 * returns <code>"return <i>call</i>;"</code>.
	 *
	 * @param schemeName the Scheme type name
	 * @param call the function/method call to wrap
	 * @return non-null; the formatted string 
	 */
	public String nativeToPublicJavaReturn(String schemeName, String call) {
		return wrapConversion(schemeName + ".javaWrap", call, null);
	}

	/**
	 * Return the Java level unwrapping code to convert an object of the
	 * given Scheme type from the public Java type to the native Java type.
	 * This returns <code>null</code> if there's no need to do a conversion.
	 *
	 * @param schemeName the Scheme type name
	 * @param publicName the name of the variable holding the public
	 * value to unwrap
	 * @param nativeName the name of the variable to hold the native value
	 * @return the formatted string
	 */
	public String publicToNativeJavaStatements(
		String schemeName,
		String publicName,
		String nativeName) {
		return unwrapConversion(
			schemeName + ".javaUnwrap",
			publicName,
			nativeName);
	}

	/**
	 * Get the C level type name for a native Java type corresponding to the
	 * given Scheme type name.
	 *
	 * @param schemeName the Scheme type name
	 * @return the Java type 
	 */
	public String nativeType(String schemeName) {
		return nonNullProperty(schemeName + ".nativeType");
	}

	/**
	 * Return the C level wrapping code to convert a function/method call
	 * of the given Scheme type from the underlying GLib/GTK type to a
	 * <code>return</code> statement of the native Java type. If there
	 * is no specific conversion specified as a property, then this
	 * returns the given cleanup statements followed by 
	 * <code>"return <i>call</i>;"</code>.
	 *
	 * @param schemeName the Scheme type name
	 * @param call the function/method call to wrap
	 * @param cleanup non-null; a set of cleanup statements to execute
	 * before returning
	 * @return non-null; the formatted string 
	 */
	public String glibToNativeReturn(
		String schemeName,
		String call,
		String cleanup) {
		return wrapConversion(schemeName + ".nativeWrap", call, cleanup);
	}

	/**
	 * Return the C level unwrapping code to convert an object of the given
	 * Scheme type from the native Java type to the underlying GLib/GTK
	 * type. This will throw an exception if the given type has no
	 * appropriate property defined for it.
	 * The result is an array of three elements, as follows:
	 *
	 * <ul>
	 * <li>the C declarations for the variables</li>
	 * <li>the extra initialization statements for the variables</li>
	 * <li>the statements needed to clean up after the variables are no
	 * longer needed</li>
	 * </ul>
	 *
	 * <p>If there is no need for the second or third element, then they
	 * are returned as an empty string (<code>""</code>), not as
	 * <code>null</code>.
	 *
	 * @param schemeName the Scheme type name
	 * @param javaName the name of the variable holding the native Java
	 * value to unwrap
	 * @param glibName the name of the variable to hold the GLib/GTK value
	 * @return non-null; the array of formatted strings
	 */
	public String[] javaToGlibNativeStatements(
		String schemeName,
		String javaName,
		String glibName) {
		String[] result = new String[3];
		String raw =
			unwrapConversion(schemeName + ".nativeUnwrap", javaName, glibName);

		if (raw == null) {
			throw new RuntimeException(
				"error: do not know how to unwrap "
					+ "to glib type: "
					+ schemeName);
		}

		// separate out the pieces

		int dash1 = raw.indexOf("\n-\n");
		if (dash1 == -1) {
			// only one part
			result[0] = raw;
			result[1] = "";
			result[2] = "";
		} else {
			result[0] = raw.substring(0, dash1);
			int dash2 = raw.indexOf("\n-\n", dash1 + 3);
			if (dash2 == -1) {
				// only two parts
				result[1] = raw.substring(dash1 + 3);
				result[2] = "";
			} else {
				// all three parts
				result[1] = raw.substring(dash1 + 3, dash2);
				result[2] = raw.substring(dash2 + 3);
			}
		}

		return result;
	}

	/**
	 * Get the raw GLib/GTK type corresponding to the given Scheme type, to
	 * pass as an argument or accept as a return value.
	 *
	 * @param schemeName the Scheme type name
	 * @return the raw GLib/GTK type 
	 */
	public String glibType(String schemeName) {
		return nonNullProperty(schemeName + ".rawType");
	}

	// ------------------------------------------------------------------------
	// public static methods

	/**
	 * Given a Scheme type name, return <code>true</code> if it's an array
	 * type and <code>false</code> otherwise. This is defined to be the
	 * case when the name ends with an asterisk (<code>"*"</code>),
	 * corresponding to a C-style pointer declaration.
	 *
	 * @param schemeName the Scheme type name
	 * @return <code>true</code> if it names an array type
	 */
	public static boolean isArray(String schemeName) {
		return schemeName.endsWith("*");
	}

	/**
	 * Given a Scheme type name, return the corresponding base Scheme
	 * type name. This is defined to be the same as the Scheme name,
	 * except array types have their array marker removed.
	 *
	 * @see #isArray
	 * @param schemeName the Scheme type name
	 * @return the base Scheme type name
	 */
	public static String baseName(String schemeName) {
		if (schemeName.endsWith("*")) {
			return schemeName.substring(0, schemeName.length() - 1);
		}

		return schemeName;
	}

	/**
	 * Given a Java type name--either a primitive type or a fully qualified
	 * class name--return the corresponding Java signature string.
	 *
	 * @param name the Java type name
	 * @return the corresponding signature string
	 */
	public static String javaSignature(String name) {
		if (name.equals("boolean"))
			return "Z";
		else if (name.equals("byte"))
			return "B";
		else if (name.equals("char"))
			return "C";
		else if (name.equals("double"))
			return "D";
		else if (name.equals("float"))
			return "F";
		else if (name.equals("int"))
			return "I";
		else if (name.equals("long"))
			return "J";
		else if (name.equals("short"))
			return "S";
		else if (name.equals("void"))
			return "V";
		else if (name.endsWith("[]")) {
			name = name.substring(0, name.length() - 2);
			return "[" + javaSignature(name);
		} else {
			return "L" + name.replace('.', '/') + ';';
		}
	}

	/**
	 * Return the number of significant characters to which the given two
	 * names match prefixes, ignoring case and underscores. This enables
	 * one to produce a correspondence between <code>studlyCapNames</code>
	 * and <code>underscore_style_names</code>.
	 *
	 * @param name1 a name to compare
	 * @param name2 the name to compare it to
	 * @return the number of significant (non-underscore) characters the
	 * two names have as a common prefix
	 */
	static public int fuzzyPrefixLength(String name1, String name2) {
		int l1 = name1.length();
		int l2 = name2.length();
		int i1 = 0;
		int i2 = 0;
		int sig = 0;

		while ((i1 < l1) && (i2 < l2)) {
			char c1 = name1.charAt(i1);
			if (c1 == '_') {
				i1++;
				continue;
			}

			char c2 = name2.charAt(i2);
			if (c2 == '_') {
				i2++;
				continue;
			}

			c1 = Character.toLowerCase(c1);
			c2 = Character.toLowerCase(c2);

			if (c1 != c2) {
				break;
			}

			i1++;
			i2++;
			sig++;
		}

		return sig;
	}

	/**
	 * Take an <code>underscore_style_identifier</code> and return the
	 * equivalent <code>studlyCapsIdentifier</code>.
	 *
	 * @param orig the original identifier
	 * @return the translated form
	 */
	static public String toStudlyCaps(String orig) {
		int len = orig.length();
		StringBuffer result = new StringBuffer(len);
		boolean caps = false;

		for (int i = 0; i < len; i++) {
			char c = orig.charAt(i);
			if (c == '_') {
				caps = true;
			} else {
				if (caps) {
					c = Character.toUpperCase(c);
					caps = false;
				}
				result.append(c);
			}
		}

		return result.toString();
	}

	/**
	 * Massage an identifier (name of a variable, method, etc.), so it
	 * doesn't conflict with a Java or C keyword. If it needs to do
	 * something, it generally duplicates the first letter and inserts it
	 * in capitalized form as the second letter. For example,
	 * <code>"new"</code> becomes <code>"nNew"</code>.
	
	 * only recognizes <code>"default"</code>, <code>"case"</code>,
	 * <code>"new"</code>, and the primitive Java type names.
	 *
	 * @param orig non-null; the original name
	 * @return non-null; the possibly-modified name 
	 */
	static public String makeValidJavaIdentifier(String orig) {
		char c0 = orig.charAt(0);
		char u0 = Character.toUpperCase(c0);

		if (orig.equals("default")
			|| orig.equals("case")
			|| orig.equals("new")
			|| orig.equals("void")
			|| orig.equals("boolean")
			|| orig.equals("byte")
			|| orig.equals("char")
			|| orig.equals("short")
			|| orig.equals("int")
			|| orig.equals("long")
			|| orig.equals("float")
			|| orig.equals("double")) {
			return "" + c0 + u0 + orig.substring(1);
		}

		// if this already looks like the output of the above process,
		// then add another duplicated letter, to ensure there are no
		// conflicts with other massaged names
		if ((orig.length() > 1) && (u0 == orig.charAt(1))) {
			return c0 + orig;
		}

		return orig;
	}

	// ------------------------------------------------------------------------
	// private instance methods

	/**
	 * Given a Scheme type name and a set of keys for a schematic
	 * translation, add new keys for the Scheme type name, by substituting
	 * JAVA_TYPE, JAVA_PATH, and GLIB_TYPE in the values and replacing the
	 * base part of the key with the Scheme type name.
	 *
	 * @param schemeName the Scheme type name
	 * @param keys array of keys to process 
	 */
	private void bindKeys(String schemeName, String[] keys) {
		// first make sure we haven't been asked to bind for this name
		// before
		if (myTypes.contains(schemeName)) {
			throw new RuntimeException(
				"error: attempt to redefine type: " + schemeName);
		}

		int origBaseLen = keys[0].indexOf('.');
		int ptrBaseLen = keys[0].indexOf('*');
		if (ptrBaseLen > 0)
			origBaseLen -= 1;

		Hashtable bindings = new Hashtable(2);
		String jname = javaName(schemeName);
		bindings.put("JAVA_TYPE", jname);
		bindings.put("JAVA_PATH", jname.replace('.', '/'));
		bindings.put("GLIB_TYPE", glibName(schemeName));

		for (int i = 0; i < keys.length; i++) {
			int baseLen;
			ptrBaseLen = keys[i].indexOf('*');
			if (ptrBaseLen > 0)
				baseLen = ptrBaseLen;
			else
				baseLen = keys[i].indexOf('.');
			String origKey = keys[i];
			String key = schemeName + origKey.substring(baseLen);
			String value =
				substituteString(myProperties.getProperty(origKey), bindings);
			myProperties.put(key, value);
			if (DEBUG) {
				System.err.println("DEBUG: " + key + ": " + value);
			}
		}

		// add the key identifying the schema (OBJECT, ENUM, etc.)
		String key = schemeName + ".SCHEMA";
		String value = keys[0].substring(1, origBaseLen);
		myProperties.put(key, value);

		if (DEBUG) {
			System.err.println("DEBUG: " + key + ": " + value);
		}

		// and add the type to the list of known types
		myTypes.addElement(schemeName);
	}

	/**
	 * Return the named property from {@link #myProperties}, but throw
	 * an exception rather than returning <code>null</code> if it's not
	 * found.
	 *
	 * @param name the property name to look up
	 * @return non-null; the associated value
	 */
	private String nonNullProperty(String name) {
		String result = nullOkProperty(name);
		if (result == null) {
			throw new RuntimeException(
				"error: could not find property: " + name);
		}

		return result;
	}

	/**
	 * Return the named property from {@link #myProperties}, or
	 * return <code>null</code> if it's not found.
	 *
	 * @param name the property name to look up
	 * @return null-ok; the associated value
	 */
	private String nullOkProperty(String name) {
		String result = myProperties.getProperty(name);

		if (result != null) {
			return result;
		}

		// see if the base name has an alias; if so, try to resolve it

		int dotAt = name.indexOf('.');
		if (dotAt == -1) {
			// no dot; shouldn't happen in practice
			return null;
		}

		String base = name.substring(0, dotAt);
		String aliasedTo = myProperties.getProperty(base + ".alias");
		if (aliasedTo == null) {
			// no alias property
			return null;
		}

		// bingo; recursively resolve the alias
		aliasedTo = aliasedTo.trim();
		return nullOkProperty(aliasedTo + name.substring(dotAt));
	}

	/**
	 * Return an application of the named unwrapping conversion pattern to
	 * the given "from" and "to" variable names. If there is no property
	 * corresponding to the name, then this returns <code>null</code>.
	 *
	 * @param conversionName non-null; the name of the conversion property
	 * @param fromName non-null; the "from" variable name
	 * @param toName non-null; the "to" variable name 
	 * @return the formatted string
	 */
	private String unwrapConversion(
		String conversionName,
		String fromName,
		String toName) {
		String pattern = nullOkProperty(conversionName);
		if (pattern == null) {
			return null;
		}

		Hashtable bindings = new Hashtable(2);
		bindings.put("FROM", fromName);
		bindings.put("TO", toName);
		return substituteString(pattern, bindings);
	}

	/**
	 * Return an application of the named wrapping conversion pattern to
	 * the given "call" value. If there is no specific conversion specified
	 * as a property, then this returns the cleanup statements (if
	 * any) followed by <code>"return <i>call</i>;"</code>.
	 *
	 * @param conversionName non-null; the name of the conversion property
	 * @param call non-null; the "call" value
	 * @param cleanup null-ok; a set of cleanup statements to execute
	 * before returning
	 * @return the formatted string
	 */
	private String wrapConversion(
		String conversionName,
		String call,
		String cleanup) {
		String pattern = nullOkProperty(conversionName);
		if (pattern == null) {
			String result = "return " + call + ";\n";
			if (cleanup != null) {
				result = cleanup + '\n' + result;
			}
			return result;
		}

		Hashtable bindings = new Hashtable(2);
		bindings.put("CALL", call);
		if (cleanup != null) {
			bindings.put("CLEANUP", cleanup);
		}
		return "{ " + substituteString(pattern, bindings) + " }";
	}

	// ------------------------------------------------------------------------
	// private static methods

	/**
	 * Return the GLib/GTK type name for a given non-primitive Scheme type
	 * name.
	 *
	 * As of this writing, it is an identity function.
	 *
	 * @param schemeName the Scheme type name
	 * @return the corresponding GLib type name 
	 */
	static private String glibName(String schemeName) {
		return schemeName;
	}

	/**
	 * Return the fully qualified Java class name for a given Scheme type
	 * name. FIXME: This is the method to alter to cause classes to
	 * be in the <code>gnu.</code> package hierarchy, and also to cause
	 * classes to not have a "Gtk" (etc.) prefix in the class name itself.
	 *
	 * @param schemeName the Scheme type name
	 * @return the corresponding fully-qualified class name
	 */
	static private String javaName(String schemeName) {
		if (schemeName.startsWith("Gdk")) {
			return "org.gnu.gdk." + schemeName;
		} else if (schemeName.startsWith("Gtk")) {
			return "org.gnu.gtk." + schemeName;
		} else if (schemeName.startsWith("Gnome")) {
			return "org.gnu.gnome." + schemeName;
		} else if (schemeName.startsWith("Atk")) {
			return "org.gnu.atk." + schemeName;
		} else if (schemeName.startsWith("Bonobo")) {
			return "org.gnu.bonobo." + schemeName;
		} else if (schemeName.startsWith("Pan")) {
			return "org.gnu.pango." + schemeName;
		} else if (
			schemeName.startsWith("GType")
				|| schemeName.startsWith("GValue")
				|| schemeName.startsWith("GObject")
				|| schemeName.startsWith("GError")
				|| schemeName.startsWith("GSpawnError")
				|| schemeName.startsWith("GQuark")
				|| schemeName.startsWith("GClosure")
				|| schemeName.startsWith("GBoxed")
				|| schemeName.startsWith("GEnum")
				|| schemeName.startsWith("GFlags")
				|| schemeName.startsWith("GTypeInterface")
				|| schemeName.startsWith("GList")) {
			return "org.gnu.glib." + schemeName;
		} else {
			System.err.println(
				"WARNING: Unrecognized prefix for Scheme "
					+ "name: "
					+ schemeName
					+ "; assuming package "
					+ "\"org.gnu.gnome.\"");
			return "org.gnu.gnome." + schemeName;
		}
	}

	/**
	 * Extremely simple string substitution engine. It takes a source
	 * string and substitutes the values in the given map for the
	 * corresponding keys with a dollar-sign (<code>"$"</code>) prefix
	 * found in the source. For example, if there is a mapping from
	 * <code>"MILK"</code> to <code>"yummy white stuff"</code>, and the
	 * source string is <code>"I like $MILK!"</code>, then the result
	 * is <code>"I like yummy white stuff!"</code>.
	 *
	 * @param source the source string to manipulate
	 * @param bindings the map of bindings
	 * @return the result of substituting the bindings in the source 
	 */
	static private String substituteString(String source, Hashtable bindings) {
		Enumeration e = bindings.keys();
		while (e.hasMoreElements()) {
			String key = (String) e.nextElement();
			String value = (String) bindings.get(key);
			key = "$" + key;
			int keyLen = key.length();

			for (;;) {
				int foundAt = source.indexOf(key);
				if (foundAt == -1) {
					break;
				}
				source =
					source.substring(0, foundAt)
						+ value
						+ source.substring(foundAt + keyLen);
			}
		}

		return source;
	}
}
