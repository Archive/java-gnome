/**
 * Representation of a C function which is associated with a class. In
 * addition to the standard Java member properties, it has a
 * an argument list and a piece of C code.
 *
 * <p>The Java-Gnome bindings library is free software distributed under the
 * terms of the GNU Library General Public License version 2.</p>
 *
 * @author Dan Bornstein, danfuzz@milk.com
 * @author Copyright 2000 Dan Bornstein, all rights reserved. 
 */
final public class CFunctionSpec
    extends JavaMemberSpec
{
    /** non-null; the list of arguments to the function */
    private ArgumentList myArgumentList;

    /** non-null; the C code for the function body */
    private String myBody;

    

    // ------------------------------------------------------------------------
    // constructor

    /**
     * Construct an instance.
     *
     * @param name non-null; the name of the function
     * @param returnType non-null; the return type of the function
     * @param argumentList non-null; list of arguments to the function
     * @param body non-null; the C code for the function body; it should not
     * have braces around its entirety (those are provided for you at
     * no additional cost)
     */
    public CFunctionSpec (String name, TypeSpecIface returnType,
			  ArgumentList argumentList, String body)
    {
	super (name, returnType, true, true, SCOPE_PRIVATE);

	if (argumentList == null)
	{
	    throw new NullPointerException ("argumentList == null");
	}

	if (body == null)
	{
	    throw new NullPointerException ("body == null");
	}

	myArgumentList = argumentList;
	myBody = body;
    }



    // ------------------------------------------------------------------------
    // public instance methods

    /**
     * Return the Java source for this instance. There is no source
     * in this case, so it returns an empty string.
     *
     * @return <code>""</code>, always
     */
    public String javaSource ()
    {
	return "";
    }

    // interface's comment suffices
    public String cSource ()
    {
	StringBuffer sb = new StringBuffer ();

	sb.append (cFunctionDeclaraion ());
	sb.append ("\n");
	sb.append ("{\n");
	sb.append (myBody);
	sb.append ("}\n");

	return sb.toString ();
    }

    /**
     * Return the C header for this instance.
     *
     * @return the C header
     */
    public String cHeader ()
    {
	return cFunctionDeclaraion () + ";\n";
	// wow, that was easy!
    }

    // ------------------------------------------------------------------------
    // private instance methods

    /**
     * Return the function declaration for the method.
     *
     * @return the function declaration for the method
     */
    private String cFunctionDeclaraion ()
    {
	StringBuffer result = new StringBuffer ();

	//result.append (myType.nativeType ());
	result.append(myType.glibType());
	result.append (" ");
	result.append (myName);
	result.append (" (");
	result.append (myArgumentList.glibArguments ());
	result.append (")");

	return result.toString ();
    }
}
