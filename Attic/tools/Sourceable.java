/**
 * Interface for things which can generate a source code representation.
 * The source may be in one of three places: a Java source file, a C source
 * file, or a C header file. If a given instance has no code to output
 * for a given place, it should return the empty string (<code>""</code>),
 * not <code>null</code> from the appropriate method(s).
 *
 * <p>The Java-Gnome bindings library is free software distributed under the
 * terms of the GNU Library General Public License version 2.</p>
 *
 * @author Dan Bornstein, danfuzz@milk.com
 * @author Copyright 2000 Dan Bornstein, all rights reserved. 
 */
public interface Sourceable
{
    /**
     * Return the Java source for this instance.
     *
     * @return non-null; the Java source
     */
    public String javaSource ();

    /**
     * Return the C source for this instance.
     *
     * @return non-null; the C source
     */
    public String cSource ();

    /**
     * Return the C header for this instance.
     *
     * @return non-null; the C header
     */
    public String cHeader ();
}
