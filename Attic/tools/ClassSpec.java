import java.util.Hashtable;
import java.util.Vector;

/**
 * Representation of a class. It consists of a target type (that is, what
 * class it is associated with), and a list of fields and methods (where
 * methods includes constructors). This object is mutable; it is constructed
 * basically empty, and one adds elements to it.
 *
 * <p>The Java-Gnome bindings library is free software distributed under the
 * terms of the GNU Library General Public License version 2.</p>
 *
 * @author Dan Bornstein, danfuzz@milk.com
 * @author based on code originally by Olivier Gutknecht, gutkneco@lirmm.fr
 * @author Copyright 2000 Dan Bornstein, all rights reserved. 
 */
final public class ClassSpec {
	/** non-null; the type representing this instance's class */
	private TypeSpecIface myType;

	/** null-ok; the type representing this instance's superclass */
	private TypeSpecIface myExtends;

	/** non-null; list of members */
	private Vector myMemberList;

	/** non-null; list of constant translations
	 * @see #translateConstant 
	 * @see #addConstantTranslation */
	private Hashtable myConstantTranslations;

	/** null-ok; non-generated code to add to the C header file */
	private String manualHeaderCode = null;

	/** null-ok; non-generated code to add the the C glue file */
	private String manualGlueCode = null;

	/** null-ok; comment to add to the output */
	private String myComment = null;

	// ------------------------------------------------------------------------
	// constructor

	/**
	 * Construct an instance. It initially contains no members.
	 *
	 * @param type non-null; the type representing this instance's class
	 * @param extendsType null-ok; the type representing this instance's
	 * superclass
	 */
	public ClassSpec(TypeSpecIface type, TypeSpecIface extendsType) {
		this(type, extendsType, null);
	}

	/**
	 * Construct an instance. It initially contains no members.
	 *
	 * @param type non-null; the type representing this instance's class
	 * @param extendsType null-ok; the type representing this instance's
	 * @param comment null-ok; a comment describing this type
	 * superclass
	 */
	public ClassSpec(
		TypeSpecIface type,
		TypeSpecIface extendsType,
		String comment) {
		if (type == null) {
			throw new NullPointerException("type == null");
		}

		myType = type;
		myExtends = extendsType;
		myComment = comment;

		myMemberList = new Vector();
		myConstantTranslations = new Hashtable();
	}

	// ------------------------------------------------------------------------
	// public instance methods

	/**
	 * Get the type of this class.
	 *
	 * @return the type
	 */
	public TypeSpecIface getType() {
		return myType;
	}

	/**
	 * Get the count of the members of this instance.
	 *
	 * @return the count of the members
	 */
	public int getMemberCount() {
		return myMemberList.size();
	}

	/**
	 * Get an array of all of the members of this instance.
	 *
	 * @return an array of all of the members
	 */
	public JavaMemberSpec[] getMembers() {
		JavaMemberSpec[] result = new JavaMemberSpec[myMemberList.size()];
		myMemberList.copyInto(result);
		return result;
	}

	/**
	 * Add a member to this instance.
	 *
	 * @param member non-null; the new member to add
	 */
	public void add(JavaMemberSpec member) {
		if (member == null) {
			throw new NullPointerException("member == null");
		}

		myMemberList.addElement(member);
	}

	/**
	 * Add all the members from the given instance to this one.
	 *
	 * @param other the instance to take members from
	 */
	public void addMembers(ClassSpec other) {
		Vector otherMem = other.myMemberList;
		int sz = otherMem.size();
		for (int i = 0; i < sz; i++) {
			JavaMemberSpec jms = (JavaMemberSpec) otherMem.elementAt(i);
			add(jms);
		}
	}

	/**
	 * Add a new constant translation to this instance. A constant translation
	 * is a mapping from a constant name in one representational form
	 * (presumably, but not necessarily, the Scheme default value
	 * form) to the name of a public static field of this instance.
	 *
	 * @param fromName non-null; the original form of the constant
	 * @param toName non-null; the name of the field on this instance that
	 * it maps to 
	 */
	public void addConstantTranslation(String fromName, String toName) {
		if (fromName == null) {
			throw new NullPointerException("fromName == null");
		}

		if (toName == null) {
			throw new NullPointerException("toName == null");
		}

		myConstantTranslations.put(fromName, toName);
	}

	public void addManualHeaderCode(String code) {
		if (manualHeaderCode == null) {
			manualHeaderCode = "";
		}
		manualHeaderCode += code;
	}

	public void addManualGlueCode(String code) {
		if (manualGlueCode == null) {
			manualGlueCode = "";
		}
		manualGlueCode += code;
	}

	/**
	 * Return a string expression which refers to the appropriate public
	 * static field of this instance, using a fully-qualified class name.
	 * This throws an exception if the given name was never defined as a
	 * constant.
	 *
	 * @param fromName non-null; the original form of the constant
	 * @return non-null; the Java expression to refer to that constant 
	 */
	public String translateConstant(String fromName) {
		if (fromName == null) {
			throw new NullPointerException("fromName == null");
		}

		String xlat = (String) myConstantTranslations.get(fromName);
		if (xlat == null) {
			throw new RuntimeException(
				"error: don't know how to translate constant: " + fromName);
		}

		return myType.publicJavaType() + '.' + xlat;
	}

	/**
	 * Return the Java source for this instance.
	 *
	 * @return the Java source
	 */
	public String javaSource() {
		String thisPackage = myType.publicJavaPackage();

		StringBuffer sb = new StringBuffer(10000);

		sb.append("package ");
		sb.append(thisPackage);
		sb.append(";\n");

		if (myExtends != null) {
			String extendsPackage = myExtends.publicJavaPackage();
			if (!(extendsPackage.equals("")
				|| thisPackage.equals(extendsPackage))) {
				sb.append("import ");
				String className = myExtends.publicJavaType();
				int index = className.lastIndexOf(".");
				String shortName = className.substring(index+1);
				String pkg = className.substring(0, index+1);
				className = GlibTranslator.javaGnomeName(shortName);

				sb.append(pkg + className);
				sb.append(";\n");
			}
		}

		// FIXME: shouldn't have to do this like this
		if (thisPackage.indexOf("gnome") != -1) {
			sb.append("import org.gnu.gtk.*;\n");
		}

		// Insert the class comment here
		if (myComment != null) {
			sb.append("/**\n");
			String output = "*";
			java.util.StringTokenizer st =
				new java.util.StringTokenizer(myComment);

			while (st.hasMoreElements()) {
				String tmp = st.nextToken();
				if (output.length() + tmp.length() < 70) {
					output += " " + tmp;
				} else {
					sb.append(output + "\n");
					output = "* " + tmp;
				}
			}
			sb.append(output + "\n*/\n\n");
		}

		sb.append("public class ");
		//sb.append (myType.simpleJavaType ());
		sb.append(GlibTranslator.javaGnomeName(myType.simpleJavaType()));
				
		if (myExtends != null) {
			sb.append(" extends ");
			sb.append(GlibTranslator.javaGnomeName(myExtends.simpleJavaType()));
		}

		sb.append("\n{\n\n\n\n\n");
		sb.append("\n/****************************************");
		sb.append("\n * BEGINNING OF GENERATED CODE");
		sb.append("\n ****************************************/\n\n");

		int sz = myMemberList.size();
		for (int i = 0; i < sz; i++) {
			JavaMemberSpec src = (JavaMemberSpec) myMemberList.elementAt(i);
			sb.append(src.javaSource());
		}
		sb.append("\n/****************************************");
		sb.append("\n * END OF GENERATED CODE");
		sb.append("\n ****************************************/\n\n");

		sb.append("}\n");
		return CodeCleanser.indent(sb.toString(), 0);
	}

	/**
	 * Return the C source for this instance.
	 *
	 * @return the C source
	 */
	public String cSource() {
		StringBuffer sb = new StringBuffer(10000);

		sb.append("#include <jni.h>\n" + "#include <sys/types.h>\n");

		// FIXME: shouldn't have to do this
		String pkg = myType.publicJavaPackage();
		if (pkg.indexOf("gdk") != -1) {
			sb.append("#include <gdk/gdk.h>\n");
		} else if (pkg.indexOf("gtk") != -1) {
			sb.append("#include <gtk/gtk.h>\n");
		} else if (pkg.indexOf("gnome") != -1) {
			sb.append("#include <gnome.h>\n");
		} else if (pkg.indexOf("bonobo") != -1) {
			sb.append("#include <bonobo.h>\n");
		} else if (pkg.indexOf("glib") != -1) {
			sb.append("#include <glib.h>\n");
			sb.append("#include <glib-object.h>\n");
		} else if (pkg.indexOf("atk") != -1) {
			sb.append("#include <atk/atk.h>\n");
			sb.append("#include <gtk/gtk.h>\n");
		} else if (pkg.indexOf("pango") != -1) {
			sb.append("#include <pango/pango.h>\n");
			sb.append("#include <gtk/gtk.h>\n");
		}

		sb.append("#ifdef __cplusplus\n" + "extern \"C\" {\n" + "#endif\n");

		sb.append("\n\n\n\n\n");
		sb.append("\n/****************************************");
		sb.append("\n * BEGINNING OF GENERATED CODE");
		sb.append("\n ****************************************/\n\n");

		int sz = myMemberList.size();
		for (int i = 0; i < sz; i++) {
			JavaMemberSpec src = (JavaMemberSpec) myMemberList.elementAt(i);
			sb.append(src.cSource());
		}

		sb.append("\n/****************************************");
		sb.append("\n * END OF GENERATED CODE");
		sb.append("\n ****************************************/\n\n");

		sb.append("#ifdef __cplusplus\n" + "}\n" + "#endif\n");

		return CodeCleanser.indent(sb.toString(), 0);
	}

}
