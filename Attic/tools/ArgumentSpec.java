/**
 * Specification of a single argument. An argument consists of a Scheme
 * type, a formal name, an optional default value, and a {@link TypeConverter}
 * instance which knows how to generate code.
 *
 * <p>The Java-Gnome bindings library is free software distributed under the
 * terms of the GNU Library General Public License version 2.</p>
 *
 * @author Dan Bornstein, danfuzz@milk.com
 * @author based on code originally by Olivier Gutknecht, gutkneco@lirmm.fr
 * @author Copyright 2000 Dan Bornstein, all rights reserved. 
 */
final public class ArgumentSpec extends TypeSpec {
	/** non-null; the formal argument name */
	private String myFormalName;

	/** null-ok; the default value for the argument */
	private String myDefaultValue;

	/** <code>true</code> if it is okay to pass a <code>null</code>
	 * value for this argument */
	private boolean myNullOk;

	/** <code>true</code> if the argument is read-only (mostly useful when
	 * this is representing an object field) */
	private boolean myReadOnly;

	/** <code>true</code> if the argument is the object reference
	 * (mostly useful in cases where the object argument is not the
	 * first argument for a function). An argument list may have a
	 * maximum of one element with myIsObject true. */
	private boolean myIsObject;

	// ------------------------------------------------------------------------
	// constructor

	/**
	 * Construct an instance.
	 *
	 * @param typeConverter non-null; the {@link TypeConverter} to use
	 * @param schemeType non-null; the Scheme type name for the argument
	 * @param formalName non-null; the formal parameter name of the argument
	 * @param defaultValue null-ok; the default value of the argument
	 * @param nullOk <core>true</code> if it is okay to pass a
	 * <code>null</code> value for this argument, <code>false</code> if
	 * not
	 * @param readOnly <code>true</code> if the argument is read-only
	 * (mostly useful when this is representing an object field)
	 */
	public ArgumentSpec(
		TypeConverter typeConverter,
		String schemeType,
		String formalName,
		String defaultValue,
		boolean nullOk,
		boolean readOnly,
		boolean isObject) {
		super(typeConverter, schemeType);

		if (formalName == null) {
			throw new NullPointerException("formalName == null");
		}

		myNullOk = nullOk;
		myReadOnly = readOnly;
		myFormalName = TypeConverter.makeValidJavaIdentifier(formalName);
		myDefaultValue = defaultValue;
		myIsObject = isObject;
	}

	// ------------------------------------------------------------------------
	// public instance methods

	/**
	 * Return the string form for this instance.
	 *
	 * @return the string form
	 */
	public String toString() {
		StringBuffer result = new StringBuffer();

		result.append("(");
		result.append(super.toString());
		result.append(" ");
		result.append(myFormalName);

		if (myNullOk) {
			result.append(" (null-ok)");
		}

		if (myIsObject) {
			result.append(" (object)");
		} else if (myReadOnly) {
			result.append(" (read-only)");
		} else {
			result.append(" (read-write)");
		}

		if (myDefaultValue != null) {
			result.append(" (= \"" + myDefaultValue + "\")");
		}

		result.append(")");
		return result.toString();
	}

	/**
	 * Return a new instance, just like this one, except its default
	 * value is nulled out.
	 *
	 * @return the new instance 
	 */
	public ArgumentSpec withoutDefault() {
		return new ArgumentSpec(
			myTypeConverter,
			mySchemeType,
			myFormalName,
			null,
			myNullOk,
			myReadOnly,
			myIsObject);
	}

	/**
	 * Return the type of this argument.
	 *
	 * @return the type
	 */
	public TypeSpec getType() {
		// note: this class inherits from TypeSpec, so theoretically we
		// could return "this"; however, a call to this method signals
		// the intent to *just* use a type, so to reduce confusion, we
		// simply construct a new direct instance of TypeSpec
		return new TypeSpec(myTypeConverter, mySchemeType);
	}

	/**
	 * Return <code>true</code> if this instance was specified
	 * as <code>null-ok</code>.
	 *
	 * @return the <code>null-ok</code> value
	 */
	public boolean getNullOk() {
		return myNullOk;
	}

	/**
	 * Return <code>true</code> if this instance was specified
	 * as <code>read-only</code>.
	 *
	 * @return the <code>read-only</code> value
	 */
	public boolean getReadOnly() {
		return myReadOnly;
	}

	/**
	 * Return <code>true</code> if this instance was specified as
	 * <code>object</code>.
	 */
	public boolean getIsObject() {
		return myIsObject;
	}

	// XXX
	public void setIsObject(boolean s) {
		myIsObject = s;
	}

	/**
	 * Return the default value of the argument, or <code>null</code> if
	 * no default is defined for this instance.
	 *
	 * @param translator null-ok; the translator to use to translate the
	 * default; if passed as <code>null</code>, then the default won't
	 * get translated at all
	 * @return null-ok; the default value or <code>null</code> 
	 */
	public String getDefaultValue(DefaultTranslator translator) {
		if (translator == null) {
			return myDefaultValue;
		}

		if (myDefaultValue == null) {
			return null;
		}

		return translator.translate(mySchemeType, myDefaultValue);
	}

	/**
	 * Return the name of the argument, as a <i>formal</i> argument to
	 * a method or function.
	 *
	 * @return the formal argument name
	 */
	public String getFormalName() {
		return myFormalName;
	}

	/**
	 * Return the declaration for this instance as an argument to a public
	 * Java method.
	 *
	 * @return the argument declaration 
	 */
	public String publicJavaArgument() {
		return myTypeConverter.publicJavaType(mySchemeType)
			+ " "
			+ myFormalName;
	}

	/**
	 * Return the statements needed at the public Java level to convert
	 * this argument into a variable holding a value suitable to pass to a
	 * native Java method. If no conversion needs to be done, then this
	 * returns the empty string (<code>""</code>), not <code>null</code>.
	 *
	 * @return the conversion statements
	 */
	public String publicToNativeJavaStatements() {
		if (!myTypeConverter.needsJavaConversion(mySchemeType)) {
			return "";
		}

		return myTypeConverter.publicToNativeJavaStatements(
			mySchemeType,
			myFormalName,
			nativeJavaVariableName());
	}

	/**
	 * Return the name of the argument, as a variable containing a
	 * value appropriate to pass to a native Java method. In the
	 * case where no conversion needs to be done for the public
	 * argument, this is the same as the result of calling {@link
	 * #getFormalName}.
	 *
	 * @return the native Java variable name 
	 */
	public String nativeJavaVariableName() {
		if (myTypeConverter.needsJavaConversion(mySchemeType)) {
			return myFormalName + "_n";
		}

		return myFormalName;
	}

	/**
	 * Return the declaration for this instance as an argument to a native
	 * Java method.
	 *
	 * @return the argument declaration 
	 */
	public String nativeJavaArgument() {
		return myTypeConverter.nativeJavaType(mySchemeType)
			+ " "
			+ myFormalName;
	}

	/**
	 * Return the declaration for this instance as an argument at the
	 * C level to a native Java method.
	 *
	 * @return the argument declaration 
	 */
	public String nativeArgument() {
		return myTypeConverter.nativeType(mySchemeType) + " " + myFormalName;
	}

	/**
	 * Return the three sets of statements needed to convert a C level
	 * native Java method argument to a GLib/GTK variable.
	 *
	 * @see TypeConverter#javaToGlibNativeStatements
	 * @return the conversion statements
	 */
	public String[] javaToGlibNativeStatements() {
		return myTypeConverter.javaToGlibNativeStatements(
			mySchemeType,
			myFormalName,
			glibVariableName());
	}

	/**
	 * Return the declaration for this instance as an argument to a
	 * function at the raw GLib level.
	 *
	 * @return the argument declaration 
	 */
	public String glibArgument() {
		return myTypeConverter.glibType(mySchemeType) + " " + myFormalName;
	}

	/**
	 * Return the name of the argument, as a variable containing a
	 * value appropriate to pass to a GLib/GTK function.
	 *
	 * @return the GLib/GTK variable name 
	 */
	public String glibVariableName() {
		return myFormalName + "_g";
	}
}
